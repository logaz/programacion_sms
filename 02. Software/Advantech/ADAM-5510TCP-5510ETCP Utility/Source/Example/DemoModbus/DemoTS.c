//-------------------------------------------------------------------------------------
//	Program: Mod_TCP_Server.c
//
//	Author: Arthur Hsu, Advantech Co., Ltd.
//
//	Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//				 This is a simple TCP based server example. This program is single-
//	             threaded but multi-connections capability program.
//	
//	Note: This program is for demostration only and is not guaranteed to be worked
//		  in every application. Programmers who use this sample code should modify
//		  it depends on their applications.
//
//	History:
//			Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------

#include "mod.h"
#include "5510drv.h"

#define DATASIZE 250
#define sizeofShareMem	4000

int count=0;
unsigned int LocalDIO(void);

int main(void)
{
	SOCKET Sock_5510;
	int err_code;
	unsigned int Share_Mem[sizeofShareMem];
	unsigned int tmpcnt=0;
	int tmpidx;

	
	memset(Share_Mem, 0, sizeof(Share_Mem));
	if((err_code=ADAMTCP_ModServer_Create(502, 5000, 7,
										  (unsigned char *)Share_Mem, sizeof(Share_Mem)))!=0)	//first step
	{
		printf("error code is %d\n", err_code);
	}
	
	Timer_Init();
	tmpidx = Timer_Set(1000);
	printf("Server started, wait for connect...\n");
	while(1)
	{
		ADAMTCP_ModServer_Update();	//second step: return 0 NO packet, return 1 has packet

		if(tmArriveCnt[tmpidx])
		{
			Timer_Reset(tmpidx);
			disable();
			Share_Mem[0] = LocalDIO();	//write 5051 status to address 40001
			enable(); 
		}
	}
	
	ADAMTCP_ModServer_Release();

	return 0;
}

unsigned int LocalDIO(void)     //set Adam-5056&5068 and return Adam-5051 Status
{
    unsigned div, dov;
    char dov1;

    if(count%2==0)
    {
        dov = 0xffff;
        dov1 = 0x0;
    }
    else
    {
        dov = 0x0000;
        dov1 = 0xff;
    }
        
    count++;
    if(count>100)
        count = 1;
    Set5068(&dov1,2,0,AByte);       //slot 2
    Set5056(&dov,1,0,AWord);        //slot 1

    Get5051(0,0,AWord,&div);        //slot 0
    return (unsigned int)~div;
}