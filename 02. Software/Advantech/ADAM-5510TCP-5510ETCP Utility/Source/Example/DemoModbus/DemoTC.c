//-------------------------------------------------------------------------------------
//  Program: Mod_TCP_Client.c
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple TCP based Modbus client example.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include "mod.h"

#define Server_Port 502 
#define MAXDATASIZE 100


int main(int argc, char *argv[])
{
	char * ServerIP;
    SOCKET SO_5510;
    unsigned char HostData[MAXDATASIZE];
    int DataByteCount = 0;
    int tmp;
    unsigned int tmpcnt=0, tmpcnt1=0;
    int errcode;
 
    memset(HostData, MAXDATASIZE, 0);
    
    if(argc==2)
    {
    	ServerIP = argv[1];
    }
    else
    {
    	printf("Please input Server IP.\n");
    	return 0;
    }

    if(ADAMTCP_Connect(&SO_5510, ServerIP, Server_Port)<=0)
    {
        perror("ADAMTCP_Connect()\n");
        ADAMTCP_Disconnect(&SO_5510);
        return 0;
    }

	printf("Starting to send..\n");
	while(1)
	{
   		//Query Adam-6051 Server
    	if((errcode=ADAMTCP_ReadCoilStatus(&SO_5510, 50, 0x01, 0x01, 0x01, &DataByteCount, HostData))<=0)
    	{
    		if(errcode==TCPTimeOut_Err)
    			perror("Time Out.\n");
    		else
    	    	printf("Error: Error Code is %d\n", errcode);
    	    ADAMTCP_Disconnect(&SO_5510);
    	    return 0;
    	}
    	else
    	{
       		printf("Adam-6051 Channel 0 Status: ");
       		for(tmp=0; tmp<DataByteCount; tmp++)
    		{
    		    printf("%2X", HostData[tmp]&0x01);   
   		 	}
    		printf("\n");
		}

		for(tmpcnt=0; tmpcnt<50000; tmpcnt++)	//delay
		{for(tmpcnt1=0; tmpcnt1<4; tmpcnt1++){}}

	}
                            

    return 1;
}
