#include <stdio.h>
#include <dos.h>
#include <time.h>
#include <conio.h>
#include "5510drv.h"
#include "RTU.h"

#define MAXDATASIZE 100
#define sizeofShareMem	10

int count;
unsigned int LocalDIO(void);

void main()
{
	unsigned int Share_Mem[sizeofShareMem];
	char cCh;
	char LSR_State;
	unsigned int tmpcnt, tmpcnt1;

	if(Modbus_COM_Init(COM1, Slave, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	} 

	printf("init success!!\n");
	
	if(!ADAMRTU_ModServer_Create(3, (unsigned char *)Share_Mem, sizeof(Share_Mem)))
	{
		printf("err code is %d\n", Error_Code());
		return;
	}

	printf("server started..\n");

	while(1) 
	{
		disable();
		Share_Mem[0] = LocalDIO();	//write 5051 status to address 40001
		enable();

		for(tmpcnt=0; tmpcnt<50000; tmpcnt++)	//delay
		{for(tmpcnt1=0; tmpcnt1<8; tmpcnt1++){}}
		 
	}
	
}

unsigned int LocalDIO(void)     //set Adam-5056&5068 and return Adam-5051 Status
{
    unsigned div, dov;
    char dov1;

    if(count%2==0)
    {
        dov = 0xffff;
        dov1 = 0x0;
    }
    else
    {
        dov = 0x0000;
        dov1 = 0xff;
    }
        
    count++;
    if(count>100)
        count = 1;
    Set5068(&dov1,2,0,AByte);       //slot 2
    Set5056(&dov,1,0,AWord);        //slot 1

    Get5051(0,0,AWord,&div);        //slot 0
    return (unsigned int)~div;
}