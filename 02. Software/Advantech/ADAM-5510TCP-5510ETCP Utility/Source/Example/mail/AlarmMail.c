//-------------------------------------------------------------------------------------
//  Program: AlarmMail.c
//
//  The following source codes are copyrighted, 2004, by Advantech Co., Ltd.
//  All rights are reserved. Advantech Co., Ltd. You may freely use or incorporate
//  the source codes into your own programs without royalty to Advantech. However,
//  if you distribute the source code, you would include this header in the source
//  file and not remove it.
//
//  Example provided in the source codes is intended to demonstrate the functions
//  only. Advantech Co., Ltd. assumes no responsibility for its use, nor for any
//  infringements upon the rights of third parties, which may result from its use.
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple Adam-5510 Web Control example. This program is CGI
//               based and is compatible with Adam-5510 library that provides
//               programmers implement versatile control programs based on this
//               architecture.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------
#include <stdio.h>
#include <process.h>
#include <errno.h>
#include "5510drv.h"


int SendAlarmMail(void);
int MakeAlarmMail(void);

int count = 1;

void main(void)
{
    unsigned div, dov;
    char dov1;


	if(!MakeAlarmMail())
	{
		printf("make mail fail..");
		return;
	}
	
	while(1)
	{
		printf("Please input Adam-5056 output values: ");
		scanf("%X", &dov);
	    if(count%2==0)
	    {
	        dov1 = 0x0;
	    }
	    else
	    {
	        dov1 = 0xff;
	    }

		if(dov == 0x33)
			return;

	    count++;
    	if(count>100)
	        count = 1;

	    Set5068(&dov1,2,0,AByte);
	    Set5056(&dov,1,0,AWord);
    
	    Get5051(0,0,AWord,&div);
	
		if(div == 0x00ff)
		{
			if(!SendAlarmMail())
			{
				printf("send mail error..");
				return;
			}
		}
	}
		

}

int MakeAlarmMail(void)
{
	char * arg_To = "-t567@123.com";
	char * arg_From = "-f345@hotmail.com";
	char * arg_subject = "-s5510TCP";
	char * arg_MailContent = "-bmail.txt";
	char * arg_O_mail = "-omail.dat";

	printf("Making Mail..\n");
	if(spawnlp(P_WAIT,
			   "d:\\mail\\makemail.exe",
			   "d:\\mail\\makemail.exe",
			   arg_To,
			   arg_From,
			   arg_subject,
			   arg_MailContent,
			   arg_O_mail,
			   NULL)==-1)
	{
		return 0;
	}

	return 1;
		
}
int SendAlarmMail(void)
{
	char * arg1 = "smtp.123.com";
	char * arg2 = "mail.dat";

	printf("send Alarm mail prepare..\n");
	if(spawnlp(P_WAIT,"d:\\mail\\sendmail.exe","d:\\mail\\sendmail.exe",arg1,arg2,NULL)==-1)
	{
		return 0;
	}

	return 1;
}