//-------------------------------------------------------------------------------------
//	Program: Mod_TCP_Server.c
//
//	Author: Arthur Hsu, Advantech Co., Ltd.
//
//	Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//				 This is a simple TCP based server example. This program is single-
//	             threaded but multi-connections capability program.
//	
//	Note: This program is for demostration only and is not guaranteed to be worked
//		  in every application. Programmers who use this sample code should modify
//		  it depends on their applications.
//
//	History:
//			Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------

#include "mod.h"
#include "stdio.h"
#define DATASIZE 250
#define sizeofShareMem	4000


int main(void)
{
	int err_code;
	unsigned int Share_Mem[sizeofShareMem];
	unsigned int pre_data=0;
	int iState = 0;

	
	if((err_code=ADAMTCP_ModServer_Create(502, 5000, 20,
										  (unsigned char *)Share_Mem, sizeof(Share_Mem)))!=0)	//first step
	{
		printf("error code is %d/n", err_code);
	}
	

	printf("Server started, wait for connect...\n");
	while(1)
	{
		iState = ADAMTCP_ModServer_Update();	//second step

		if(iState)//if has message, show the data at address 40001
		{
			if(pre_data != Share_Mem[0])
			{
				adv_printf("40001 is %X\n", Share_Mem[0]);	//notice: print message will decrease
				pre_data = Share_Mem[0];					//server performance
			}
		}
	}

	ADAMTCP_ModServer_Release();

	return 0;
}







