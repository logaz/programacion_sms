//-------------------------------------------------------------------------------------
//  Program: Mod_TCP_Client.c
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple TCP based Modbus client example.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include "mod.h"

#define Server_Port 502 
#define MAXDATASIZE 100
#define ServerIP "xxx.xxx.xxx.xxx" 

int main(void)
{
    SOCKET SO_5510;
    unsigned char HostData[MAXDATASIZE];
    int DataByteCount = 0;
    int tmp;
 
    memset(HostData, MAXDATASIZE, 0);

    if(ADAMTCP_Connect(&SO_5510, ServerIP, Server_Port)<=0)
    {
        perror("ADAMTCP_Connect()\n");
        ADAMTCP_Disconnect(&SO_5510);
        return 0;
    }

    //Query Adam-5000/TCP Server, Adam5051 in Slot 1
    if(ADAMTCP_ReadCoilStatus(&SO_5510, 3000, 0x01, 0x11, 0x10, &DataByteCount, HostData)<=0)
    {
        perror("ADAMTCP_ReadCoilStatus()\n");
        ADAMTCP_Disconnect(&SO_5510);
        return 0;
    }
    else
    {
        printf("Adam-5051 Status: ");
        for(tmp=0; tmp<DataByteCount; tmp++)
        {
            printf("%2X", HostData[tmp]);   
        }
        printf("\n");
    }
                            

    return 1;
}
