//-------------------------------------------------------------------------------------
//  Program: Mod_TCP_Client.c
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple TCP based Modbus client example.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include "mod.h"

#define Server_Port 502 
#define MAXDATASIZE 100
#define ServerIP "172.18.3.199" 

int main(void)
{
    SOCKET SO_5510;

    if(ADAMTCP_Connect(&SO_5510, ServerIP, Server_Port)<=0)
    {
        perror("ADAMTCP_Connect()\n");
        ADAMTCP_Disconnect(&SO_5510);
        return 0;
    }
    
    //Query Adam-5000/TCP, Adam-5024 in slot 4, force channel 1(type 0~10V) to 5V
    if(ADAMTCP_PresetSingleReg(&SO_5510, 3000, 0x01, 0x19, 0x07ff)<=0)
    {
        perror("ADAMTCP_PresetSingleReg()\n");
        ADAMTCP_Disconnect(&SO_5510);
        return 0;
    }


    return 1;
}
