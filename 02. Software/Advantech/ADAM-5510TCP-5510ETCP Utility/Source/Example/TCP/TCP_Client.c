//-------------------------------------------------------------------------------------
//  Program: TCP_Client.c
//
//  The following source codes are copyrighted, 2004, by Advantech Co., Ltd.
//  All rights are reserved. Advantech Co., Ltd. You may freely use or incorporate
//  the source codes into your own programs without royalty to Advantech. However,
//  if you distribute the source code, you would include this header in the source
//  file and not remove it.
//
//  Example provided in the source codes is intended to demonstrate the functions
//  only. Advantech Co., Ltd. assumes no responsibility for its use, nor for any
//  infringements upon the rights of third parties, which may result from its use.
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple TCP based client example. This program is single-
//               threaded but multi-connections capability program.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <malloc.h>
#else
#include <mem.h>
#endif
#include <string.h>
#include <conio.h>
#include <errno.h>
#include "socket.h"
#define Errno errno

#define Server_Port 5510 
#define MAXDATASIZE 100 

int main(int argc, char *argv[])
{
    SOCKET SO_5510;
    int numbytes=0;
    char buf[MAXDATASIZE];
    struct hostent *he;
    struct sockaddr_in Server_addr; 
    char *str1, *str2, *str;
    int tmpcount=1;
    
    str1 = "TCP\n";
    str2 = "Client\n";  
    
    if (argc != 2)
    {
        fprintf(stderr,"usage: server hostname\n");
        exit(1);
    }
    
    if ((he=gethostbyname(argv[1])) == NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    if ((SO_5510 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
    {
        perror("socket");
        exit(1);
    }

    
    Server_addr.sin_family = AF_INET;
    Server_addr.sin_port = htons(Server_Port);
    Server_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(&(Server_addr.sin_zero), 0, 8);


    if (connect(SO_5510, (struct sockaddr *)&Server_addr, 
        sizeof(struct sockaddr)) == SOCKET_ERROR)
    {
        perror("connect");
        exit(1);
    }

    while(1)
    {
        if ((numbytes=recv(SO_5510, buf, MAXDATASIZE-1, 0)) == SOCKET_ERROR)
        {
            perror("recv");
            exit(1);
        }

        if(numbytes>0)
        {
            printf("Received: %s",buf);
            
            memset(buf, 0, sizeof(buf));
            if(tmpcount%2==0)
                str = str1;
            else
                str = str2;
            
            sleep(1);   
            if (send(SO_5510, str, strlen(str), 0) == SOCKET_ERROR)
            {
                perror("send");
                exit(1);
            }
            tmpcount++;
            if(tmpcount>100)
                tmpcount=1;
        }
        else
        {
            closesocket(SO_5510);
            break;
        }
    }
    return 0;
}