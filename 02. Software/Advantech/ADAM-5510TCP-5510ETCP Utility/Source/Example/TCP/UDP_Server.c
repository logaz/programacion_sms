//-------------------------------------------------------------------------------------
//  Program: UDP_Server.c
//
//  The following source codes are copyrighted, 2004, by Advantech Co., Ltd.
//  All rights are reserved. Advantech Co., Ltd. You may freely use or incorporate
//  the source codes into your own programs without royalty to Advantech. However,
//  if you distribute the source code, you would include this header in the source
//  file and not remove it.
//
//  Example provided in the source codes is intended to demonstrate the functions
//  only. Advantech Co., Ltd. assumes no responsibility for its use, nor for any
//  infringements upon the rights of third parties, which may result from its use.
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple UDP based server example.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <malloc.h>
#else
#include <mem.h>
#endif
#include <string.h>
#include <conio.h>
#include <errno.h>
#include "socket.h"

#define Errno errno

#define FALSE 0
#define TRUE 1
#define Host_Port 5510
#define MAXBUFLEN 100




int main(void)
{
    SOCKET Host_Sock;
    struct sockaddr_in Host_addr;
    struct sockaddr_in Client_addr;
    int hasMessage = FALSE;
    unsigned long pulArgp;
    char buf[MAXBUFLEN];
    int addr_len, numbytes;
    char* ackmsg = "ACK";

    if ((Host_Sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
    {
        perror("socket");
        exit(1);
    }


    Host_addr.sin_family = AF_INET; 
    Host_addr.sin_port = htons(Host_Port); 
    Host_addr.sin_addr.s_addr = INADDR_ANY; 
    memset(&(Host_addr.sin_zero), 0, 8); 


    if (bind(Host_Sock, (struct sockaddr *)&Host_addr, sizeof(struct sockaddr)) == SOCKET_ERROR)
    {
        perror("bind");
        exit(1);
    }

    pulArgp = 1;
    if(ioctlsocket(Host_Sock, FIONBIO, &pulArgp))
    {
        perror("ioctlsocket");
        exit(1);
    }


    printf("wait for client send message...\n");
    
    while(1)
    {
        
        hasMessage = Host_WaitForMessage(Host_Sock, 0);
    
        if(hasMessage)
        {       
            addr_len = sizeof(struct sockaddr);
            if ((numbytes = recvfrom( Host_Sock, buf, sizeof(buf), 0,
                (struct sockaddr *)&Client_addr, &addr_len)) == SOCKET_ERROR)
            {
                perror("recvfrom");
                if (errno == EWOULDBLOCK)
                    printf("EWOULDBLOCK");
                break;
            }
            buf[numbytes] = 0;
            printf("got packet \"%s\" from %s\n", buf, inet_ntoa(Client_addr.sin_addr));

            if ((numbytes=sendto(Host_Sock, ackmsg, strlen(ackmsg), 0,
                (struct sockaddr *)&Client_addr, sizeof(struct sockaddr))) == SOCKET_ERROR)
            {
                perror("sendto");
                break;
            }
        }
    }

    closesocket(Host_Sock);
    return 0;
}

int Host_WaitForMessage(int serverSocket, int i_iWaitMilliSec)
{
    fd_set FdSet;
    struct timeval  waitTime;
    
    FD_ZERO(&FdSet);
    FD_SET(serverSocket, &FdSet);
    waitTime.tv_sec  = i_iWaitMilliSec / 1000;
    waitTime.tv_usec = (i_iWaitMilliSec % 1000)*1000L;

    if (select(0, &FdSet, NULL, NULL, &waitTime) > 0)
        return TRUE;
    return FALSE;
}
