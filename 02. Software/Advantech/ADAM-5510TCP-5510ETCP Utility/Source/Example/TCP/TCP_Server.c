//-------------------------------------------------------------------------------------
//  Program: TCP_Server.c
//
//  The following source codes are copyrighted, 2004, by Advantech Co., Ltd.
//  All rights are reserved. Advantech Co., Ltd. You may freely use or incorporate
//  the source codes into your own programs without royalty to Advantech. However,
//  if you distribute the source code, you would include this header in the source
//  file and not remove it.
//
//  Example provided in the source codes is intended to demonstrate the functions
//  only. Advantech Co., Ltd. assumes no responsibility for its use, nor for any
//  infringements upon the rights of third parties, which may result from its use.
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple TCP based server example. This program is single-
//               threaded but multi-connections capability program.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <malloc.h>
#else
#include <mem.h>
#endif
#include <string.h>
#include <conio.h>
#include <errno.h>
#include "socket.h"
#define Errno errno

#define FALSE 0
#define TRUE 1
#define Host_Port 5510 
#define Max_Conn 40 
#define MAXDATASIZE 100

SOCKET remoteSocket[Max_Conn];
int WaitSocketCount[Max_Conn];
int socketTotal = 0;
int timeoutRelease = FALSE;

void ReleaseClient(int idx_so);

int main(void)
{
    SOCKET Sock_5510, New_Conn;
    struct sockaddr_in Host_addr; 
    struct sockaddr_in Client_addr; 
    int sin_size;
    int hasConnect, hasMessage;
    int maxSocket, sidx, New_Sidx, numbytes, sidx2;
    char buf[MAXDATASIZE];
    unsigned long pulArgp;
    char *str;
    int tmpcount=1;


    if ((Sock_5510 = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        perror("socket");
        exit(1);
    }


    Host_addr.sin_family = AF_INET;
    Host_addr.sin_port = htons(Host_Port);
    Host_addr.sin_addr.s_addr = INADDR_ANY; 
    memset(&(Host_addr.sin_zero), 0, 8); 

    if (bind(Sock_5510, (struct sockaddr *)&Host_addr, sizeof(struct sockaddr)) == SOCKET_ERROR)
    {
        perror("bind");
        exit(1);
    }

    pulArgp = 1;
    if(ioctlsocket(Sock_5510, FIONBIO, &pulArgp))
    {
        perror("ioctlsocket");
        exit(1);
    }


    if (listen(Sock_5510, 5) == SOCKET_ERROR)
    {
        perror("listen");
        exit(1);
    }

    hasMessage = FALSE;
    memset(WaitSocketCount, 0, sizeof(WaitSocketCount));
    printf("Server started, wait for connect...\n");
    while(1)
    { 
        if (socketTotal > 0)
            hasConnect = Host_WaitForClient(Sock_5510, 0);
        else
            hasConnect = Host_WaitForClient(Sock_5510, 5);
        
        if(hasConnect)
        {
            printf("Receive client connect request...\n");
            sin_size = sizeof(struct sockaddr_in);
            if ((New_Conn = accept(Sock_5510, (struct sockaddr *)&Client_addr,
                &sin_size)) == INVALID_SOCKET)
            {
                perror("accept");
                continue;
            }

            if (New_Conn != INVALID_SOCKET)
            {
                if (socketTotal < Max_Conn)
                {
                    remoteSocket[socketTotal] = New_Conn;
                    New_Sidx = socketTotal;
                    socketTotal++;
                }
                else
                {
                    if (send(New_Conn, "Connetion full, you are going to be disconnected!\n", 50, 0) == SOCKET_ERROR)
                        perror("send");
                    closesocket(New_Conn);
                    printf("Connetion full, disconnect client!\n");
                }
            }
            else
                printf("(TCP) Invalid incoming socket!\n");

                
            str = "Hello, world!\n";
            if (send(remoteSocket[New_Sidx], str, strlen(str), 0) == SOCKET_ERROR)
                perror("send"); 


        }
        
        if(socketTotal>0)
        {
            for(sidx=0; sidx<socketTotal; sidx++)
            {
                hasMessage = Host_WaitForClient(remoteSocket[sidx], 0);
                if(hasMessage)
                {
                    if((numbytes=recv(remoteSocket[sidx], buf, sizeof(buf), 0)) == SOCKET_ERROR)
                    {
                        ReleaseClient(sidx);
                    }
                    else
                    {
                    
                    	if(numbytes>0)
                    	    printf("Server receive: %s", buf);
                    
                    	if(tmpcount%2==0)
                    	    str = "ACK\n";
                    	else
                    	    str = "A C K\n";
                        
                    	if(numbytes==0)
                    	{
                    		ReleaseClient(sidx);
                    	}
                    	else if(send(remoteSocket[sidx], str, strlen(str), 0) == SOCKET_ERROR)
                    	{
                        	ReleaseClient(sidx);
                    	}
                	

                    	memset(buf, 0, sizeof(buf));
                    	tmpcount++;
                    	if(tmpcount>100)
                        	tmpcount = 1;
                        
                    	WaitSocketCount[sidx] = 0;
                    }
                }
                else
                    WaitSocketCount[sidx]++;
                    
                if(WaitSocketCount[sidx]>10000)
                {
                    timeoutRelease = TRUE;
                    ReleaseClient(sidx);
                }

            }   
        }
    }
    
    return 0;
}

int Host_WaitForClient(int WaitSocket, int i_iWaitMilliSec)
{
    fd_set FdSet;
    struct timeval  waitTime;
    
    FD_ZERO(&FdSet);
    FD_SET(WaitSocket, &FdSet);
    waitTime.tv_sec  = i_iWaitMilliSec / 1000;
    waitTime.tv_usec = (i_iWaitMilliSec % 1000)*1000L;

    if (select(0, &FdSet, NULL, NULL, &waitTime) > 0)
        return TRUE;
    return FALSE;
}


void ReleaseClient(int idx_so)
{
    int sidx, sidx2;
    
    sidx = idx_so;
    
    if(timeoutRelease)
    {
        if (send(remoteSocket[sidx], "Connetion timeout, you are going to be disconnected!\n", 53, 0) == -1)
            perror("send");
    }
    
    if(remoteSocket[sidx]!=INVALID_SOCKET)
    {
    	if(closesocket(remoteSocket[sidx])!=0)
    		printf("Release client resource fail!");
    }

    
    for(sidx2 = sidx; sidx2<= socketTotal-1; sidx2++)
    {
        if(sidx2<socketTotal-1)
        {
            WaitSocketCount[sidx2] = WaitSocketCount[sidx2+1];
            remoteSocket[sidx2] = remoteSocket[sidx2+1];    
        }
        else if(sidx2==socketTotal-1)
        {
            WaitSocketCount[sidx2] = 0;
            remoteSocket[sidx2] = NULL; 
        }
                        
    }
                    
    socketTotal--;
    

    if(timeoutRelease)
        printf("Connetion timeout, disconnect client %d!\n", sidx);
    else
        printf("Socket error, disconnect client %d!\n", sidx);
        
    if(socketTotal==0)
        printf("Wait for client connect...\n");

    timeoutRelease = FALSE;

}