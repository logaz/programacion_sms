//-------------------------------------------------------------------------------------
//  Program: UDP_Client.c
//
//  The following source codes are copyrighted, 2004, by Advantech Co., Ltd.
//  All rights are reserved. Advantech Co., Ltd. You may freely use or incorporate
//  the source codes into your own programs without royalty to Advantech. However,
//  if you distribute the source code, you would include this header in the source
//  file and not remove it.
//
//  Example provided in the source codes is intended to demonstrate the functions
//  only. Advantech Co., Ltd. assumes no responsibility for its use, nor for any
//  infringements upon the rights of third parties, which may result from its use.
//
//  Author: Arthur Hsu, Advantech Co., Ltd.
//
//  Description: This program is designed for Adam-5510/TCP and Adam-5510E/TCP.
//               This is a simple UDP based client example.
//  
//  Note: This program is for demostration only and is not guaranteed to be worked
//        in every application. Programmers who use this sample code should modify
//        it depends on their applications.
//
//  History:
//          Version A1.0  05/18/2004 created by Arthur
//------------------------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <malloc.h>
#else
#include <mem.h>
#endif
#include <string.h>
#include <conio.h>
#include <errno.h>
#include "socket.h"
#define Errno errno
#define BufferSize  100
#define Host_Port 5510


int main(int argc, char *argv[])
{
    SOCKET SO_5510;
    struct sockaddr_in Server_addr; 
    struct sockaddr_in From_Addr; 
    struct hostent *he;
    char buf[BufferSize];
    int numbytes;
    unsigned int From_Size; 
    char* msg = "UDP Client Conneted!";
    
    if (argc != 2)
    {
        fprintf(stderr,"usage: uclient xxx.xxx.xxx.xxx\n");
        exit(1);
    }

    if ((he=gethostbyname(argv[1])) == NULL)
    { 
        perror("gethostbyname");
        exit(1);
    }

    if ((SO_5510 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
    {
        perror("socket");
        exit(1);
    }


    Server_addr.sin_family = AF_INET;
    Server_addr.sin_port = htons(Host_Port); 
    Server_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(&(Server_addr.sin_zero), 0, 8);
    
    if ((numbytes=sendto(SO_5510, msg, strlen(msg), 0,
        (struct sockaddr *)&Server_addr, sizeof(struct sockaddr))) == SOCKET_ERROR)
    {
        perror("sendto");
        exit(1);
    }
    
    printf("sent %d bytes to %s\n", numbytes, inet_ntoa(Server_addr.sin_addr));

    From_Size = sizeof(From_Addr);
    if ((numbytes = recvfrom( SO_5510, buf, sizeof(buf), 0,
        (struct sockaddr *)&From_Addr, &From_Size)) == -1)
    {
        perror("recvfrom");
        exit(1);
    }
    buf[numbytes] = 0;
    printf("got Ack packet \"%s\" from %s\n", buf, inet_ntoa(From_Addr.sin_addr));

    closesocket(SO_5510);
    
    return 0;
}