#include "5510drv.h"

struct IOmodule	{
  unsigned char	id;
  char *str;
} Module;

struct IOmodule	ModuleID[14] = {
	0x04, "ADAM5017",
	0x05, "ADAM5018",
	0x08, "ADAM5013A",
	0x09, "ADAM5013B",
	0x0C, "ADAM5017H",
	0x0D, "ADAM5018H",
	0x0F, "ADAM5052",
	0x10, "ADAM5050",
	0x11, "ADAM5051",
	0x12, "ADAM5056",
	0x13, "ADAM5068",
	0x14, "ADAM5060",
	0x18, "ADAM5024",
	0x1E, "ADAM5080",
};

void main(void)
{
  int i, j;
  unsigned char	mID, found;
  unsigned char	sec, min, hour;

    adv_printf("\n Welcome to ADAM5510 PC-Based Controller");
    adv_printf("\n Scan I/O module ...");
    adv_printf("\n ADAM5510 NodeID = %02Xh", Get_NodeID() );

    /* Scan ADAM5510 Slot IO Module */
    for(i=0;i <	4;i++) {
      mID = Get_BoardID(i);
      found=0;
      for(j=0;j	< 13;j++)
	if(ModuleID[j].id == mID) {
	  adv_printf("\n Slot%d = %s", i, ModuleID[j].str);
	  found=1;
	}
      if(found == 0) adv_printf("\n Slot%d = None installed", i);
    }

    /* Demo LED	Light */
    LED_init();
    adv_printf("\n flash PWR led light in panel ...");
    for(i=0;i <	5;i++) {
      LED_OFF(PWR); ADAMdelay(500);
      LED_ON(PWR); ADAMdelay(500);
    }
    adv_printf("\n flash RUN led light in panel ...");
    for(i=0;i <	5;i++) {
      LED_OFF(RUN); ADAMdelay(500);
      LED_ON(RUN); ADAMdelay(500);
    }
    adv_printf("\n flash COMM led light in panel ...");
    for(i=0;i <	5;i++) {
      LED_OFF(COMM); ADAMdelay(500);
      LED_ON(COMM); ADAMdelay(500);
    }

    /* Get RTC chips Timer Register */
    sec=GetRTCtime(RTC_sec);
    min=GetRTCtime(RTC_min);
    hour=GetRTCtime(RTC_hour);
    adv_printf("\n The RTC chips DS12887 timer ...");
    adv_printf("\n Hr:Mn:Sc = %d:%d:%d",hour,min,sec);

    adv_printf("\n Program End!");
}
