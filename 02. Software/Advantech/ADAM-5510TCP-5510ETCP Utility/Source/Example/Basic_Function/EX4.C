

/****************************************************/
/*  Filename	 : EX4.C			    */
/*  I/O	Hardware : None				    */
/*						    */
/*  Description	 : Modem test example, include dial */
/*		   ,hang-up, auto-answer, set break.*/
/*						    */
/*						    */
/*						    */
/****************************************************/

#include "5510drv.h"


int get_modem_response(char *buf)
{
long i;
int index;
unsigned char c;

		  index=0;
		  for(i=0;i<10000; i++)
		  {
		    /*--- Get the receiving string from	the COM1 port ---*/
		    if((c=com_rx())!=0)
		     buf[index++]=c;
		    if(	index >0  && c == '\r')	/* end of command */
		    {
		      buf[index] =0;
		      adv_printf("Response : %s ",buf);
		      return(1);

		    }
		  }
		  return(0);
}


void main()
{
   char	c;
   int result_code;
   char	buf[131];
   int index;
   long	i;
   long	retry;


  modem_initial();
  while(1)
  {
    adv_printf("---------- Main Menu ---------\n");
    adv_printf("0: Exit. \n");
    adv_printf("1: COM port setting. \n");
    adv_printf("2: Dial. \n");
    adv_printf("3: Set to auto-answer. \n");
    adv_printf("4: Set BREAK. \n");
    adv_printf("5: Hand up. \n");

    adv_printf("\n Please select a item to implement...\n");
    c=getch();
    switch(c)
    {
	case '0':
		return;
	case '1':
		/*--- Install the interrupt service routine for	COM 1--*/
		com_install(1);

		/* Set data format of the COM1 port */
		com_set_format(8, 0, 1);
		/*-- set baudrate -----*/
		com_set_speed(9600L);

		/*--- Show the data format on the screen ---*/
		adv_printf("COM port is COM1, baud rate is 9600 bps, data format is N,8,1\r\n");
		break;
	case '2':
		/*--- Send prefix ---*/
		modem_command("AT");

		/*--- Wait about 1 second--*/
		retry=100000;
		for(i=0;i<retry;i++)
		{
		  i++;i--;
		}

		/*--- Clear the	buffers	of the transmitter and receiver	--*/
		com_flush_rx();
		com_flush_tx();

		/**--- Start to	dial ---*/

		  /*---	Set DTR	is ON(1). --*/
		  com_raise_dtr();

		  /*---	Send the dialing command and phone number --*/
		  sprintf(buf,"886222184867");
		  modem_dial(buf);
		  adv_printf("Command  : %s \n",buf);

		  /*---	Wait for the response from the other end --*/
		  if( get_modem_response(buf)==	1)
		      adv_printf("Response : %s \n",buf);
		  else
		      adv_printf("Response : %s \n","No response");
		break;
	case '3':
		/*--- After one	ring-bell, the phone is	answered automatically.	*/
		com_raise_dtr();
		modem_autoanswer();
		adv_printf("Now is ready to get data...\n");
		break;
	case '4':  
		/* Set Break */
		com_set_break(0x3F8);

		adv_printf("Now set a break to modem...\n\n\n");
		/*--- Wait about 0.3 second--*/
		retry=30000;
		for(i=0;i<retry;i++)
		{
		  i++;i--;
		}
		com_clear_break(0x3f8);
		adv_printf("Now clear the break ...\n\n\n");
		break;
	case '5':
		/*--- Set DTR line OFF --*/
		com_lower_dtr();

		/*--- Wait about 0.3 second--*/
		retry=30000;
		for(i=0;i<retry;i++)
		{
		  i++;i--;
		}
		  /*---	Check whether DCD is off ---*/
		  /*
		  if(!(com_get_modem_status(0x3F8)&0x80))
		    break;
		   */
		/*--- Go to modem command state	---*/
		modem_command_state();
		retry=3;
		do{
		    modem_handup();
		    if(	get_modem_response(buf)== 1)
		    {
		      if( buf[0] ==0)
			break;
		    }
		    adv_printf("retry %ld \n",4-retry);
		}while(--retry);
		adv_printf("Now is hand up...\n");

		break;
    }
  }
}
