/****************************************************/
/*  Filename	 : EX10.C			    */
/*  I/O	Hardware : slot1 is 5080		    */
/*						    */
/*  Description	 : Read	the Counter value from	    */
/*		   ADAM-5080, then show	the data    */
/*		   on the screen of ADAM-5510	    */
/*		   utility.			    */
/*						    */
/****************************************************/

#include "5510drv.h"

  char *s_type[0x1f]={
    "",
    "",
    "",
    "",
    "ADAM5017_ID ",  /*0x4*/
    "ADAM5018_ID ",
    "ADAM5080_ID ",  /*0x06*/
    "",
    "ADAM5013A_ID ",  /*0x8*/
    "ADAM5013B_ID ",  /*0x9*/
    "",
    "",
    "ADAM5017H_ID",  /*0xc*/
    "ADAM5018H_ID",
    "",
    "ADAM5052_ID ",
    "ADAM5050_ID ",   /*0x10*/
    "ADAM5051_ID ",   
    "ADAM5056_ID ",
    "ADAM5068_ID ",   /*0x13*/
    "ADAM5060_ID ",   /*0x14*/
    "",
    "",
    "",
    "ADAM5024_ID "    /*0x18*/
    "",
    "",
    "",
    "",
    "",
    "",
  };



void main()

{

	unsigned char range;
	unsigned char type[4];
	unsigned long counter_value;
	char overflag_value[4];
	char c;
	int ch,i;


	/* ----	first scan IO module -------*/
	for(i=0;i<4;i++)
	{
		type[i]=Get_BoardID(i);
		if( type[i] > 0x18)
			type[i]=0;
	}

	/*----show on the screen ---*/
	for(i=0;i<4;i++)
	{
		adv_printf("IO slot %d is %s \n",i+1,s_type[type[i]]);
	}
	
	

	/*--- Initialize counter module	----- */
	adv_printf("Initialize ADAM-5080\n");
	Init5080(0);

	/* Get ADAM-5080 range */
	GetRange5080(0,&range);
	if (range==1)
		adv_printf("Range is counter\n");
	if (range==2)
		adv_printf("Range is frequency\n");

	/* Start all of	counter	*/
	for (i=0;i<4;i++)
	{
		if (Start_Stop_Counter(0,i,1)==0)
			adv_printf("Board %d Channel %d Start failure!!\n",0,i);
	}

	/*--- Set initial counter value	---*/
	for (i=0;i<4;i++)
	{
		if (SetInitCounterVal(0,i,4294967290)==0)
			adv_printf("Board %d Channel %d Setting failure!!\n",0,i);
	}

	/*--- Clear all	of counter ---*/
	for (i=0;i<4;i++)
	{
		if (Clear_Counter(0,i)==0)
			adv_printf("Board %d Channel %d Clear failure!!\n",0,i);
	}


	/*---- Forever loop until user press the "Q" key */
	while(1)
	{
		ReadOverflowFlag(0,&(overflag_value[0]));
		for (i=0;i<4;i++)
			adv_printf("Channel %d over_flag_value=%d\n",i,overflag_value[i]);

		for (i=0;i<4;i++)
		{
			Get5080(0,i,&(counter_value));
			adv_printf("Channel %d counter_value=%lu \n",i,counter_value);
		}
		adv_printf("press 'Q' to quit, the other key to continue..\n");
		c=getch();
		if( c == 'q' ||	c == 'Q')  /* Quit from	this program */
			break;
	}

	/*--- Release all allocated timers to reload the control programs  */
	Release_All();
}




