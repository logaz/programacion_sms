/****************************************************************/
/*  Filename	 : EX11.C			    						*/
/*  I/O	Hardware : Slot0: 5017UH		    					*/
/*						    									*/
/*  Description	 : Set Adam-5017UH as following settings:		*/
/*								Non-Cyclic Mode					*/
/*								Engineering Units				*/
/*								Enable Channel 0~3				*/
/*								Acquistion Number 1				*/
/*								Trigger Off						*/
/*								Filter Off						*/
/*								Input Range:-10~10V				*/
/*					then read channel 0 current data			*/
/*						    									*/
/****************************************************************/
#include "5510drv.h"

void main(void)
{
	int iSlot=0;
	int iChannel=0;
	unsigned char ChannelMask;
	unsigned long Conf_Setting;
	unsigned long Freq=100000;
	unsigned int AcquisitionNum=1;
	float Data;
	
	ChannelMask=0x0F;	/*enable ch0~ch3, disable ch4~ch7*/
	if(!Init5017UH(iSlot, Conf_NonCyclic, Conf_EngineeringUnit, ChannelMask))
	{
		adv_printf("Init Error!\n");
	}

	if(!Set5017UH_Advanced_Setting(iSlot, AcquisitionNum, Freq, Conf_Trigger_OFF, Conf_Filter_OFF))
	{
		adv_printf("Advanced Setting Error!\n");
	}
	
	if(Get5017UH(iSlot, iChannel, &Data))
	{
		adv_printf("Data: %f\n", Data);
	}
	else
	{
		adv_printf("Get Adam-5017UH value Error!\n");
		adv_printf("Error code %d\n", ErrCode);
	}
	
}