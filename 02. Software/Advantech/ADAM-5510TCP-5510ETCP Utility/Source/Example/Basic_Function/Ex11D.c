/****************************************************************/
/*  Filename	 : EX11.C			    						*/
/*  I/O	Hardware : Slot0: 5017UH		    					*/
/*						    									*/
/*  Description	 : Set Adam-5017UH as following settings:		*/
/*								Non-Cyclic Mode					*/
/*								Engineering Units				*/
/*								Enable Channel 0~3				*/
/*								Acquistion Number 32			*/
/*								Sampling rate 50000Hz			*/
/*								Trigger ON: Trigger Mode		*/
/*								Trigger Value: 4.556			*/
/*								Filter Off						*/
/*								Input Range:-10~10V				*/
/*					then read channel 0 current data			*/
/*						    									*/
/****************************************************************/
#include "5510drv.h"

#define Ch0			0x1
#define Ch1			0x2
#define Ch2			0x4
#define Ch3			0x8
#define Ch4			0x10
#define Ch5			0x20
#define Ch6			0x40
#define Ch7			0x80
#define MaxAcqNum	32
#define TriData_Num	64

void main(void)
{
	int iSlot=0;
	int iChannel=0;
	unsigned char ChannelMask;
	unsigned long Conf_Setting;
	unsigned long Freq=50000;
	unsigned int AcquisitionNum=MaxAcqNum;
	int tmpcnt;
	int TriData[TriData_Num];
	
	ChannelMask=0x0F;	/*enable ch0~ch3, disable ch4~ch7*/
	if(!Init5017UH(iSlot, Conf_NonCyclic, Conf_EngineeringUnit, ChannelMask))
	{
		adv_printf("Init Error!\n");
	}

	if(!Set5017UH_Advanced_Setting(iSlot, AcquisitionNum, Freq, Conf_Trigger, Conf_Filter_OFF))
	{
		adv_printf("Advanced Setting Error!\n");
	}

	if(Get5017UHSetting(iSlot, &ChannelMask, &Conf_Setting, &Freq, &AcquisitionNum))
	{
		adv_printf("Channel Mask: %02X\n", ChannelMask);
		if(ChannelMask&Ch5)
			adv_printf("Channel 5 is enabled\n");
		else
			adv_printf("Channel 5 is disabled\n");
			
		adv_printf("System Settings:\n");
		if(Conf_Setting&Conf_Cyclic)
		{	adv_printf("	Cyclic Mode\n");}
		else
		{
			adv_printf("	Non-Cyclic Mode\n");
			adv_printf("	Acquisition Number %d\n", AcquisitionNum);
		}
			
		if(Conf_Setting&Conf_EngineeringUnit)
			adv_printf("	Engineering Units\n");
		else
			adv_printf("	Raw Data\n");

		if(!(Conf_Setting&Conf_Trigger_OFF))
		{
			if(Conf_Setting&Conf_Trigger)
				adv_printf("	Trigger\n");
			else if(Conf_Setting&Conf_PreTrigger)
				adv_printf("	Pre-Trigger\n");
			else if(Conf_Setting&Conf_PostTrigger)
				adv_printf("	Post-Trigger\n");
			else
				adv_printf("	Trigger Setting Read Error\n");
		}
		else
			adv_printf("	Trigger OFF\n");
			
		if(Conf_Setting&Conf_Filter_ON)
			adv_printf("	Digital Filter ON\n");
		else
			adv_printf("	Digital Filter OFF\n");
			
		adv_printf("Acquisition Freq. %ldHz\n", Freq);
	}
	else
	{
		adv_printf("Read Adam-5017UH Setting Error!\n");
		adv_printf("Error code %d\n", ErrCode);
	}
	
	if(SetRange5017UH(iSlot, iChannel, In_Range_mi10V_10V))
	{
		unsigned char InRange;
		if(GetRange5017UH(iSlot, iChannel, &InRange))
		{
			if(InRange==In_Range_0_500mV)
				adv_printf("Input Range: 0~500mV\n");
			else if(InRange==In_Range_0_10V)
				adv_printf("Input Range: 0~10V\n");
			else if(InRange==In_Range_mi10V_10V)
				adv_printf("Input Range: -10~10V\n");
			else if(InRange==In_Range_4_20mA)
				adv_printf("Input Range: 4~20mA\n");
			else if(InRange==In_Range_0_20mA)
				adv_printf("Input Range: 0~20mA\n");
		}
		else
		{
			adv_printf("Read Adam-5017UH range code Error!\n");
			adv_printf("Error code %d\n", ErrCode);
		}
	}
	else
	{
		adv_printf("Set Adam-5017UH range code Error!\n");
		adv_printf("Error code %d\n", ErrCode);
	}
	
	if(Set5017UHTriVal(iSlot, iChannel, 4.556))
	{
		float TriVal;
		if(Get5017UHTriVal(iSlot, iChannel, &TriVal))
			adv_printf("Ch%d trigger value: %f\n", iChannel, TriVal);
		else
		{
			adv_printf("Read Adam-5017UH trigger value Error!\n");
			adv_printf("Error code %d\n", ErrCode);
		}
	}
	else
	{
		adv_printf("Set Adam-5017UH trigger value Error!\n");
		adv_printf("Error code %d\n", ErrCode);
	}
	
	if(Get5017UH_TriData(iSlot, iChannel, TriData))
	{
		int tmpcnt;
		for(tmpcnt=0; tmpcnt<TriData_Num; tmpcnt++)
			adv_printf("Data %02d: %04X\n", tmpcnt, TriData[tmpcnt]);
	}
	else
	{
		if(ErrCode==Trig_Event_Not_Ocur_Err)
			adv_printf("The trigger event has not occurred yet!\n");
		else
		{
			adv_printf("Get Adam-5017UH value Error!\n");
			adv_printf("Error code %d\n", ErrCode);
		}
	}

	
}