#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "RTU9.h"
#include "Adam5090.h"

#define MAXDATASIZE 100

void main()
{
	int iport;
	unsigned char HostData[MAXDATASIZE];

	iport = 74;		//slot7, port 4
	if(Modbus_5090_Init(iport, 9600L, NO_P, D8, S1)!=0)
	{
		printf("error\n");
		return;
	}

	printf("init success!!\n");

	HostData[0]=0xf0;

	while(1)
	{
	if(!A5090_RTU_ForceMultiCoils(iport, 0x02, 0x64, 0x08, 0x01, HostData))
	{
		printf("err code is %d\n", Error_Code());
		printf("fail send..");
	}
	else
		printf("Success!!");
	}

}
