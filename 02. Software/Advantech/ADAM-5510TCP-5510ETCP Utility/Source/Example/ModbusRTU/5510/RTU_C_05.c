#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "RTU.h"

#define MAXDATASIZE 100

void main()
{
	int tmpcnt;
	int tmpi;

	if(Modbus_COM_Init(COM1, Master, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	}

	printf("init success!!\n");

	if(!ADAMRTU_ForceSingleCoil(COM1, 0x02, 0x65, 0))
	{
		printf("err code is %d\n", Error_Code());
		printf("fail send..");
	}
	else
		printf("Success!!");

}
