#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "RTU.h"

#define MAXDATASIZE 100

void main()
{
	int tmpcnt;
	unsigned char HostData[MAXDATASIZE];
	int DataByteCount;

	if(Modbus_COM_Init(COM1, Master, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	}

	printf("init success!!\n");

	memset(HostData, MAXDATASIZE, 0);

	if(!ADAMRTU_ReadInputRegs(COM1, 0x02, 0x65, 0x01, &DataByteCount, HostData))
	{
		printf("err code is %d\n", Error_Code());
		printf("fail send..");
	}
	else
	{
		printf("Status: ");
		for(tmpcnt=0; tmpcnt<DataByteCount; tmpcnt++)
		{
			printf("%02X", HostData[tmpcnt]);	
		}
		printf("\n");
	}

}
