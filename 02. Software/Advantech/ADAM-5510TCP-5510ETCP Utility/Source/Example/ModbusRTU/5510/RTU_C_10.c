#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "RTU.h"

#define MAXDATASIZE 100

void main()
{
	int tmpcnt;
	unsigned char HostData[MAXDATASIZE];

	if(Modbus_COM_Init(COM1, Master, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	}

	printf("init success!!\n");


	HostData[0]=0x12;
	HostData[1]=0x56;
	HostData[2]=0x38;
	HostData[3]=0x09;

	if(!ADAMRTU_PresetMultiRegs(COM1, 0x02, 0x64, 2, 4, HostData))
	{
		printf("err code is %d\n", Error_Code());
		printf("fail send..");
		return;
	}
	else
		printf("Success!!");
			

}
