#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "RTU.h"

#define MAXDATASIZE 100

void main()
{
	unsigned char HostData[MAXDATASIZE];

	if(Modbus_COM_Init(COM1, Master, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	}

	printf("init success!!\n");

	HostData[0]=0xf0;

	if(!ADAMRTU_ForceMultiCoils(COM1, 0x02, 0x64, 0x08, 0x01, HostData))
	{
		printf("err code is %d\n", Error_Code());
		printf("fail send..");
	}
	else
		printf("Success!!");

}
