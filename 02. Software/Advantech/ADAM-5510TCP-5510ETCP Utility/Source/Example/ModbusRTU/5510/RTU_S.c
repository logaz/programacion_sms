#include <stdio.h>
#include <dos.h>
#include <time.h>
#include <conio.h>
#include "5510drv.h"
#include "RTU.h"

#define MAXDATASIZE 100
#define sizeofShareMem	10

void main()
{
	unsigned int Share_Mem[sizeofShareMem];
	unsigned int predate;
	char cCh;
	char LSR_State;

	if(Modbus_COM_Init(COM1, Slave, (unsigned long)9600, NO_PARITY, DATA8, STOP1)!=0)
	{
		printf("error\n");
		return;
	}
	 
	printf("init success!!\n");
	
	ADAMRTU_ModServer_Create(3, (unsigned char *)Share_Mem, sizeof(Share_Mem));

	printf("server started..\n");

	while(1) 
	{
		
		if(predate != Share_Mem[0])
		{
			adv_printf("40001 is %X\n", Share_Mem[0]);	//strongly recommend use adv_printf() instead of printf()
			predate = Share_Mem[0];
		}
	
	}
	
}
