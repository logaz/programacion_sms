//----------------------------------------------------------------
//Revised History:
//		A1.20	revise ADAMRTU_ModServer_Create() return type		07-27-2004 Arthur
//		A1.21	Slave&Master Co-exist problem fix					08-30-2004 Arthur
//Current Ver: A1.21
//----------------------------------------------------------------
#include "dos.h"

typedef int bool;
#define FALSE   0
#define TRUE    !FALSE

#define false   0
#define true    !false

//-----COM Port
#define COM1	1
#define COM2	2
#define COM3	3
#define COM4	4
#define Master	7
#define Slave	8
//--------Error Code
#define InvalidResponse			91
#define Com_InitOrMode_Err		92
#define COMTimeOut				93
/*------------------------------------------------------*/
/*  Line control register bits                          */
/*------------------------------------------------------*/
#define DATA5           0x00    /* 5 Data bits */
#define DATA6           0x01    /* 6 Data bits */
#define DATA7           0x02    /* 7 Data bits */
#define DATA8           0x03    /* 8 Data bits */

#define STOP1           0x00    /* 1 Stop bit */
#define STOP2           0x04    /* 2 Stop bits */

#define NO_PARITY       0x00    /* No parity */
#define ODD_PARITY      0x08    /* Odd parity */
#define EVEN_PARITY     0x18    /* Even parity */
#define ONE_PARITY      0x28    /* Parity bit = 1 */
#define ZERO_PARITY     0x38    /* Parity bit = 0 */


int Modbus_COM_Init(int Port, int iMode, unsigned long iBaud, int iParity, int iFormat, int iStopBits);
void Modbus_COM_Release(int Port);

int Error_Code();
bool ADAMRTU_ForceMultiCoils(int iPort,
							int Slave_Addr,
							int CoilIndex,
							int TotalPoint,
							int TotalByte,
							unsigned char szData[]);
bool ADAMRTU_ForceSingleCoil(int iPort,
							 int i_iAddr,
							 int i_iCoilIndex,
							 int i_iData);
bool ADAMRTU_PresetMultiRegs(int iPort,
						 	 int i_iAddr, 
						 	 int i_iStartReg,
						 	 int i_iTotalReg,
						 	 int i_iTotalByte,
						 	 unsigned char i_szData[]);
bool ADAMRTU_PresetSingleReg(int iPort,
						 	 int i_iAddr,
						 	 int i_iRegIndex,
						 	 int i_iData);
bool ADAMRTU_ReadCoilStatus(int iPort,
						   int i_iAddr, 
						   int i_iStartIndex,
						   int i_iTotalPoint,
						   int *o_iTotalByte, 
						   unsigned char o_szData[]);
bool ADAMRTU_ReadHoldingRegs(int iPort,
							 int i_iAddr, 
							 int i_iStartIndex,
							 int i_iTotalPoint,
							 int *o_iTotalByte,
							 unsigned char o_szData[]);
bool ADAMRTU_ReadInputRegs(int iPort,
						   int i_iAddr, 
						   int i_iStartIndex,
						   int i_iTotalPoint,
						   int *o_iTotalByte,
						   unsigned char o_szData[]);
bool ADAMRTU_ReadInputStatus(int iPort,
							 int i_iAddr, 
							 int i_iStartIndex,
							 int i_iTotalPoint,
						   	 int *o_iTotalByte,
						   	 unsigned char o_szData[]);
void ADAMRTU_ModServer_Create(int slave_addr,
							  unsigned char * ptr_mem,
							  unsigned int size_of_mem);

void Ver_RTU_Mod(char *vstr);   /*library version*/