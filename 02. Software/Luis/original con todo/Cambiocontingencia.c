//-----------------------------------------------------------------------------------

#include "mod.h"
#include "5510drv.h"

#define DATASIZE 250
#define sizeofShareMem	1000

//Variabes de Tramos (asigandas a registros)
///////////////////////////////////////////

#define modo 				100
#define automatico 			0x1
#define seguro 	 			0x2
#define manual 				0x3
#define manualH 			0x4
#define emergencia 			0x5
#define cambiocontingencia  0xa
#define bloqueo 			0xb  	//11
#define test 				0xc  	//12
#define off 				0xd		//13

#define en_1_in  	101  //nombre SEM 1
#define en_1_out 	111
#define tra_1_in	121
#define tra_1_out	131
#define mod_tra_1 	141

#define en_2_in  	102  //nombre SEM 2
#define en_2_out 	112
#define tra_2_in	122
#define tra_2_out	132
#define mod_tra_2 	142

#define en_3_in  	103  //nombre SEM 3
#define en_3_out 	113
#define tra_3_in	123
#define tra_3_out	133
#define mod_tra_3 	143

#define en_4_in  	104  //nombre SEM 4
#define en_4_out 	114
#define tra_4_in	124
#define tra_4_out	134
#define mod_tra_4 	144

#define s1m			190
#define s2m 		191
#define s3m 		192
#define s4m 		193

#define BLOQH		160
#define REVH   		161
#define C1H	 		162


#define timebloq 	194
#define waitnp 		195
#define waitzero	196
#define tnp      	197
#define vacio      	198


#define tra_12_in	151
#define tra_12_out	152
#define mod_tra_12 	155

#define tra_34_in	153
#define tra_34_out	154
#define mod_tra_34 	156

#define luces		165
//#define puert		166


//VARIABLES //

unsigned int Share_Mem[sizeofShareMem];

//variables Modo seguro //
int count2=0;

int sem_1_ver 	= 5;
int sem_1_roj 	= 5;
int sem_2_ver 	= 5;
int sem_2_roj 	= 5;
int sem_3_ver 	= 5;
int sem_3_roj 	= 5;
int sem_4_ver 	= 5;
int sem_4_roj 	= 5;
int sem_default = 5;

//Variables Modo Test //
int count3=0;


int estado1=1; 	//Bibase
int estado2=1;	//BibaseBFF

//Variables servidor //
int desconexion;

//Variables Modo seguro
int segtrojo;
int segestado=0;
int segtdefault;
int segtverde;

//Variables Semmaster y Semaforo
int sem1;
int s1;
int s2;
int s3;
int s4;


//Variables modo ModbusReset
int modbusreset=1;

// Variables modo Automatico
int a;
int b;
int c;
int d;

// Variables Modo BiBase
int est=0;
int cbloq=0;
int dpri=0;
int cesp=0;
int vernp=0;

// Variables Modo AutomaticoXXX
int en1in;
int en1out;
int tra1in;
int tra1out;
int en2in;
int en2out;
int tra2in;
int tra2out;
int en3in;
int en3out;
int tra3in;
int tra3out;
int en4in;
int en4out;
int tra4in;
int tra4out;
int multi;
int suma;

// Variables Modo WDT_Control
int wdt 		= 10;
int wdt_off		= 2;
int wdt_rst		= 9;

//Variables Modo BibaseBFF
int dt2V;
int dtV;
int dtrojo;

int contador=0;
int tverde=0;
int tverde2=0;
int tverde3=0;
int cuenta=0;
int E1=0;
int E2=0;
int E3=0;
int E4=0;
int cont=0;
int conta=0;
int cp1=0;
int cp2=0;
int cp3=0;
int cu=0;
int contad=0;
int c1=0;
int c2=0;
int c3=0;
int c4=0;
int cn1=0;
int cn2=0;
int cn3=0;
int cn4=0;
int ESTA;


//Funciones
void BloqueoBFF(int r_Bidir, int r_Fin, int r_Fout);
void BiBaseBFF(int r_Bidir, int entrando, int r_Fin, int r_Fout);
void seminterBFF(int r_Bidir, int entrando, int r_Fin, int r_Fout);
void BiBaseFs(int r_e1, int r_e2, int r_e3);
void sesta6(int r_e1, int r_e2, int r_e3);
void sesta7(int r_e1, int r_e2, int r_e3);
void sesta8(int r_e1, int r_e2, int r_e3);
void sesta9(int r_e1, int r_e2, int r_e3);
void sesta10(int r_e1, int r_e2, int r_e3);
void sesta11(int r_e1, int r_e2, int r_e3);
void sesta12(int r_e1, int r_e2, int r_e3);
void sesta13(int r_e1, int r_e2, int r_e3);
void sesta14(int r_e1, int r_e2, int r_e3);
void sesta15(int r_e1, int r_e2, int r_e3);
void sesta16(int r_e1, int r_e2, int r_e3);
void sesta17(int r_e1, int r_e2, int r_e3);
void sesta18(int r_e1, int r_e2, int r_e3);
void sesta19(int r_e1, int r_e2, int r_e3);
void sesta20(int r_e1, int r_e2, int r_e3);
void sesta21(int r_e1, int r_e2, int r_e3);
unsigned int LocalDI(void);  //retorna un int sin argumento
void Emergencia(void);
void Cambio(void);
void Off(void);
void Test(void);
void PinOut(void);
void Modo(void);
void III(void);
void FeFsI(void);
void BeFeI(void);
void BsFeI(void);
void BeFsI(void);
void FeFeFs(void);
void BsFsI(void);
void BeFeFe(void);
void FeFsFs(void);
void BsFeFe(void);
void BeBsI(void);
void BeFsFs(void);
void BsFsFs(void);
void Manual(void); 				//**** crear modo manual
void ManualH(void); 
void SecureMode(void);
void WDT_control(void);
void Bloqueo (void);
void AutomaticoXXX(void);
void AutomIII(void);
void AutomIFF(void);
void AutomBFF(void);
void AutomBBI(void);
void AutomBBB(void);
void AutomBBBB(void);
void BloqueoBFF(int r_Bidir, int r_Fin, int r_Fout);
void Seguro(void);
void ModbusReset(void);
void SemMaster(int S1, int S2, int S3, int s4);
void SemMasterH(int S1, int S2, int S3, int s4);
void Semaforo(int sem1,int sem2,int sem3, int sem4);
void BiBase(int r_e1, int r_e2);
void seminter(int reg1, int reg2);
void BiBaseBFF(int r_Bidir, int entrando, int r_Fin, int r_Fout);
void seminterBFF(int r_Bidir, int entrando, int r_Fin, int r_Fout);
void BFeFs(void);
void AutomBBFs(void);
void AutomBBFe(void);
void BiBaseFe(int r_e1,int r_e2, int r_e3);
void sesta1(int r_e1, int r_e2,int r_e3);
void sesta2(int r_e1,int r_e2,int r_e3);
void AutomBeBsBs(void);
void sesta3(int r_e1, int r_e2,int r_e3);
void sesta4(int r_e1,int r_e2,int r_e3);
void sesta5(int r_e1, int r_e2, int r_e3);
void AutomBeBB(int r_e1,int r_e2,int r_e3);
void AutomFeBsBs(void);
void AutomFeBB(int r_e1,int r_e2,int r_e3);
void AutomBeFeFs(void);
void AutomBeFF(int r_e1,int r_e2,int r_e3);
void AutomBeBeBs(void);
void AutomBeBeB(int r_e1,int r_e2,int r_e3);
void AutomFsBeBe(void);
void AutomFsBB(int r_e1,int r_e2,int r_e3);
void AutomBsFeFs(void);
void AutomBsFF(int r_e1,int r_e2,int r_e3);
void Hr(void);
void H(int r_e1,int r_e2,int r_e3, int r_4);
//void puerta(void);


int main(void){

	SOCKET Sock_5510;
	int err_code;
	int serv_code;
	int tmpidx;
	int pinout;

	int CC;

	//Load the value of the registers, saved in the Flash MEmory
	//////////////////////////////////////////
	//int dt2V;
	//int tV=read_backup_ram();
	//////////////////////////////////////////

	//void puerta();


	

	memset(Share_Mem, 0, sizeof(Share_Mem));
	if((err_code=ADAMTCP_ModServer_Create(502, 5000, 7,
										  (unsigned char *)Share_Mem, sizeof(Share_Mem)))!=0)	//first step
	{
		adv_printf("error code is %d\n", err_code);
	}
	
	Share_Mem[modo]=seguro;   //ADAM start in secure mode
	Share_Mem[wdt]=0x0000;

	//SetDebugMessageState(1);

	Timer_Init();
	tmpidx = Timer_Set(1000);
	adv_printf("Server started, wait for connect...\n");

	WDT_enable();

	while(1)
	{
		serv_code=ADAMTCP_ModServer_Update();	//second step: return 0 NO packet, return 1 has packet
		if(serv_code!=0){
			adv_printf("packet N is %d\n", serv_code);
			if(serv_code==1)
				desconexion=0;
		}
		if(tmArriveCnt[tmpidx])
		{

			Timer_Reset(tmpidx);
			disable();


			WDT_control();
			SecureMode();
			PinOut();

			Modo();

			enable();
		}
	}
	ADAMTCP_ModServer_Release();

	WDT_disable();
	return 0;
}
///////////////////////////////////////////////
void WDT_control(void){

	if (Share_Mem[wdt]==wdt_off){
		WDT_disable();
		adv_printf("WDT disable \n");
	}

	else if (Share_Mem[wdt]==wdt_rst){
		WDT_clear();
		ADAMdelay(3000);
	}

	else
		WDT_clear();
}
///////////////////////////////////////////////
void SecureMode(void){

	if(desconexion>=15 && desconexion<=29){			////// se debe poder configurar estos tiempos de desconexi? ***
		Share_Mem[modo]=seguro;
			ModbusReset();
	}

	else if(desconexion>=30){
		desconexion=0;
		modbusreset=1;

	}

	desconexion++;
}
///////////////////////////////////////////////////
void ModbusReset(void){

	switch(modbusreset){

		case 1:
		ADAMTCP_ModServer_Release();
		modbusreset=2;
		adv_printf("MDB release \n");
		break;

		case 2:
		ADAMTCP_ModServer_Create(502, 5000, 7,(unsigned char *)Share_Mem, sizeof(Share_Mem));
		modbusreset=0;
		adv_printf("MDB create\n");
		break;
	}
}
////////////////////////////////////////////////////
unsigned int LocalDI(void){
	unsigned DI;
	Get5050(0,0,AWord,&DI);
	return (unsigned int) DI;
}
/////////////////////////////////////////////////////
void PinOut(void){
	Share_Mem[1] = LocalDI();
	Share_Mem[8] = modbusreset;
	Share_Mem[9] = desconexion;

	adv_printf("modbus reset %d\n",modbusreset);
	adv_printf("desconexion %d\n",desconexion);

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
void Modo(void){ 					// este es el menu principal de funcionamiento del PAC

	int *p;


	switch (Share_Mem[modo]){

		case automatico:
		AutomaticoXXX();					// entra a la funcion intersecci?, definida segun MOD de cada subtramo
		adv_printf("AutomaticoXXX\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case seguro:
		Seguro();		// Set Secure mode on ADAM 5510
		adv_printf("Seguro\n");
		break;

		case manual:
		Manual();		// Set Manual mode on ADAM 5510 				/// se debe configurar el modo manual u override ***
		adv_printf("Manual\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case manualH:
		ManualH();		// Set Manual mode on ADAM 5510 				/// se debe configurar el modo manual u override ***
		adv_printf("Manual\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case emergencia:
		Emergencia();		// Set Emergency mode on ADAM 5510
		adv_printf("Emergencia\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case cambiocontingencia: //10
		adv_printf("Cambio de contingencia, restaurando valores\n");
		Cambio();
		break;

	
		case bloqueo:	// 11
		Bloqueo();
		segestado=0;					//reinicia el modo seguro
		break;

		case test:	// 12
		Test();		// Set Off mode on ADAM 5510
		adv_printf("Test\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case off:	// 13
		Off();		// Set Off mode on ADAM 5510
		adv_printf("Off\n");
		segestado=0;					//reinicia el modo seguro
		break;


}	
}
/////////////////////////////////////////////////////////////////////////////////////
void Seguro(void){


	//primero semaforo 1 verde, demas en rojo por 5 segundos(ahora dtV)
		//luego todos en rojo x delta tiempo segdt1 (ahora dtRt1)
	//segundo S2 verde demas en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt2		(dtRt2)
	//tercero S3 verde demas en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt3		(dtRt3)
	//cuarto S4 verde dem? en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt3		(dtRt4)

	switch(segestado){



		case 1:							//verde sem 1
			SemMaster(4,2,2,2);
			segtverde++;
			adv_printf("SEM 1 VERDE\n");
			if(segtverde>=sem_1_ver){
				segestado=2;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 2:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO\n");
			if(segtrojo>=sem_1_roj){			// x 10s
				segestado=3;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 3:							//verde sem 2
			SemMaster(2,4,2,2);
			segtverde++;
			adv_printf("SEM 2 VERDE\n");
			if(segtverde>=sem_2_ver){
				segestado=4;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 4:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO\n");
			if(segtrojo>=sem_2_roj){			// x 10s
				segestado=5;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 5:							//verde sem 3
			SemMaster(2,2,4,2);
			segtverde++;
			adv_printf("SEM 3 VERDE\n");
			if(segtverde>=sem_3_ver){
				segestado=6;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 6:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO\n");
			if(segtrojo>=sem_3_roj){			// x 10s
				segestado=7;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 7:							//verde sem 3
			SemMaster(2,2,2,4);
			segtverde++;
			adv_printf("SEM 4 VERDE\n");
			if(segtverde>=sem_4_ver){
				segestado=8;
				segtverde=0;
				segtrojo=0;
			}
		break;		

		case 8:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO\n");
			if(segtrojo>=sem_4_roj){			// x 10s
				segestado=1;
				segtverde=0;
				segtrojo=0;
			}
		break;


		default:
			SemMaster(2,2,2,2);
			segtdefault++;
			adv_printf("SEMAFOROS EN ROJO DEFAULT\n");
			if(segtdefault>=sem_default){
				segestado=1;
				segtdefault=0;
			}

	}
}
//////////////////////////////////////////////
void Off(void){
	unsigned doff = 0x0000;
	Set5050(&doff,0,0,AWord);
	return 0;
	Share_Mem[luces]=0;
}
///////////////////////////////////////////////
void Manual(void){

	s1=Share_Mem[s1m];

	s2=Share_Mem[s2m];

	s3=Share_Mem[s3m];





SemMaster(s1,s2,s3,s4);
}

///////////////////////////////////////////////
void ManualH(void){

	s1=Share_Mem[s1m];

	s2=Share_Mem[s2m];

	s3=Share_Mem[s3m];

	s4=Share_Mem[s4m];



SemMasterH(s1,s2,s3,s4);
}

///////////////////////////////////////////////
void Emergencia(void){     //set Adam-5056&5068 and return Adam-5051 Status

    unsigned doe;
    if(count2%2==0){
        doe = 0x1492;
         Share_Mem[luces]=1362;   // en binario es: ?1010010010010?   (V12,V10,V7,V4,V1)
    }
    else{
        doe = 0x0000;  
         Share_Mem[luces]=0;  
    }
    count2++;
    if(count2>100)
        count2 = 1;
    Set5050(&doe,0,0,AWord);       	//slot 0, pin V0-V16
    return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
void Cambio(void){

adv_printf("Volviendo a 0\n");
est=0;
cbloq=0;
dpri=0;
cesp=0;
vernp=0;
contador=0;
tverde=0;
tverde2=0;
tverde3=0;
cuenta=0;
E1=0;
E2=0;
E3=0;
E4=0;
cont=0;
conta=0;
cp1=0;
cp2=0;
cp3=0;
cu=0;
contad=0;
c1=0;
c2=0;
c3=0;
c4=0;
cn1=0;
cn2=0;
cn3=0;
cn4=0;
Bloqueo();
adv_printf("TODOS LOS VALORES EN ESTADO INICIAL\n");
}

/////////////////////////////////////////////////////////////////////////////////////
void Test(void){     //set Adam-5056&5068 and return Adam-5051 Status
    unsigned dtest;

    switch(count3){
    	case 0:
    	dtest = 0x249;
    	Share_Mem[luces]=585;
    	break;

    	case 1:
    	dtest = 5266;
    	Share_Mem[luces]= 5266;
    	break;

    	case 2:
    	dtest = 0x924;
    	Share_Mem[luces]=2340;
    	break;

    	default:
    	dtest = 0x0000;
    	Share_Mem[luces]=0;
    }
      
    count3++;
    if(count3>3)
        count3 = 0;
    Set5050(&dtest,0,0,AWord);       	//slot 0, pin V0-V16
    return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
void Bloqueo (void){
	unsigned Bq=0x1492;
		Set5050(&Bq,0,0,AWord);
		Share_Mem[luces]= 5266;
		adv_printf("Bloqueo\n");
}
//////////////////////////////////////////////////////////////////////////////
void AutomaticoXXX(void){

	a=Share_Mem[mod_tra_1]; // ACA YA SE INGRESA LA PRIORIDAD
	b=Share_Mem[mod_tra_2];
	c=Share_Mem[mod_tra_3];
	d=Share_Mem[mod_tra_4];

en1in=Share_Mem[en_1_in];
en1out=Share_Mem[en_1_out];
tra1in=Share_Mem[tra_1_in];
tra1out=Share_Mem[tra_1_out];
en2in=Share_Mem[en_2_in];
en2out=Share_Mem[en_2_out];
tra2in=Share_Mem[tra_2_in];
tra2out=Share_Mem[tra_2_out];
en3in=Share_Mem[en_3_in];
en3out=Share_Mem[en_3_out];
tra3in=Share_Mem[tra_3_in];
tra3out=Share_Mem[tra_3_out];
en4in=Share_Mem[en_4_in];
en4out=Share_Mem[en_4_out];
tra4in=Share_Mem[tra_4_in];
tra4out=Share_Mem[tra_4_out];

multi=a*b*c*d;
suma=a+b+c+d;

if(multi==0){
	adv_printf("Ingrese el modo de cada tramo\n");
	Share_Mem[mod_tra_1]=1;
	Share_Mem[mod_tra_2]=1;
	Share_Mem[mod_tra_3]=1;
	Share_Mem[mod_tra_4]=1;
	Bloqueo();

}

if (multi==1){
	adv_printf("Los tramos estan inhabilitados\n");
	III();
}

if(multi==6){
	adv_printf("Modo Fe-Fs-I\n");
	FeFsI();
}


if(multi==12 && a!=b && a!=c && b!=c){
adv_printf("El modo Be-Fs-I no esta permitido\n");
Bloqueo();
}

if((a==b || a==c || b==c) && multi==12){
FeFeFs();
}


if(multi==18){
FeFsFs();
}

if(multi==20 && suma==10){
adv_printf("El modo BS-Fe-Fe no esta permitido\n");
Bloqueo();
}

if(multi==20 && a!=b && a!=c && b!=c){
BeBsI();
}

if(multi==24){
adv_printf("Modo Be-Fe-Fs\n");	
AutomBeFeFs();
}

if(multi==30){
AutomBsFeFs();
}

if(multi==40){
adv_printf("Modo Be-Bs-Fe\n");
AutomBBFe();
}

if(multi==48){
adv_printf("Modo Fs Be Be\n");
AutomFsBeBe();
}

if(multi==50){
adv_printf("Modo Fe Bs Bs\n");
AutomFeBsBs();
}

if(multi==60){
AutomBBFs();
}

if(multi==80){
adv_printf("Modo Be Be Bs\n");
AutomBeBeBs();
}

if(multi==100){
AutomBeBsBs();
adv_printf("Modo Be-Bs-Bs\n");
}

if(multi==256){
adv_printf("Modo H\n");
Hr();	
}

if (multi==2 || multi==3 || multi==4 || multi==5 || multi==8 || 
	multi==9 || multi==10 || multi==15 || multi==16 || multi==25 || 
	multi==27 || multi==32 || multi==36 || multi==45 || 
	multi==75 || multi==125){
	adv_printf("Ingrese un modo v?ido\n");
	Bloqueo();
} 

}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//
//Inhabilitado-Inhabilitado-Inhabilitado (III) (1)
//
void III(void){
if(multi==1){
	Bloqueo();
	adv_printf("Todos los tramos estan inhabilitados\n");
}
}
//
//Fijo entrando-Fijo Saliendo-Ihnabilitado(FeFsI) (6)
//
void FeFsI(void){
if(multi==6){
	s4=2;
switch(a){
	case 1:
	s1=2; //inhabilitado semaforo en rojo
	break;
	case 2:
	if(b!=1 && tra2in==0 && tra1out==0){
	s1=3;
	break;	
	}
	
	if(b!=1 && tra2in!=0 && tra1out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}


    if(c!=1 && tra3in==0 && tra1out==0){
	s1=1;
	break;
	}
	if(c!=1 && tra3in!=0 && tra1out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}

	case 3:
	s1=2; //semaforo en rojo es fijo saliendo
	break;
	}

switch(b){
	case 1:
	s2=2; //inhabilitado semaforo en rojo
	break;
	case 2:

	if(a!=1 && tra1in==0 && tra2out==0){
	s2=1;
	break;
	}

	if(a!=1 && tra1in!=0 && tra2out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	if(c!=1 && tra3in==0 && tra2out==0){
	s2=3;
	break;
	}

	if(c!=1 && tra3in!=0 && tra2out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	case 3:
	s2=2; //semaforo en rojo es fijo saliendo
	break;
	}

switch(c){
	case 1:
	s3=2; //inhabilitado semaforo en rojo
	break;

	case 2:
	if(a!=1 && tra1in==0 && tra3out==0){
	s3=3;
	break;
	}

	if(a!=1 && tra1in!=0 && tra3out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}

	if(b!=1 && tra2in==0 && tra3out==0){
	s3=1;
	break;
	}

	if(b!=1 && tra2in!=0 && tra3out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	case 3:
	s3=2; //semaforo en rojo es fijo saliendo
	break;
	}


SemMaster(s1,s2,s3,s4);

}
}
//


//
//Fijo entrando - Fijo entrando - Fijo saliendo (FeFeFs) (12)
//
void FeFeFs(void){
	if((a==b || a==c || b==c) && multi==12){

switch (a){
	case 2:
	if (a==b && tra1out==0 && tra3in==0){
		s1=1;
		break;
	}
	if (a==c && tra1out==0 && tra2in==0){
		s1=3;
		break;
	}
	case 3:
		s1=2; //apagado fijo saliendo
		break;
		}

switch (b){
	case 2:
	if(b==a && tra2out==0 && tra3in==0){
		s2=3;
		break;
	}
	if(b==c && tra2out==0 && tra1in==0){
		s2=1;
		break;
}
	case 3:
		s2=2;
		break;

}

switch(c){
	case 2:
	if(c==a && tra3out==0 && tra2in==0){
		s3=1;
		break;
	}
	if(c==b && tra3out==0 && tra1in==0){
		s3=3;
		break;
	}	
	case 3:
		s3=2;
		break;
	}

SemMaster(s1,s2,s3,s4);
}
}


//
//Fijo entrando - Fijo saliendo - Fijo saliendo (FeFsFs) (18)
//
void FeFsFs(void){
	if(multi==18){
	s4=2;
	switch (a){
		case 2:
		if (tra1out==0 && tra2in==0 && tra3in==0){
		s1=4;
		break;}

		if(tra1out!=0){
			Bloqueo();
		}

		case 3:
		s1=2;
		break;
	}

	switch (b){
		case 2:
		if (tra2out==0 && tra1in==0 && tra3in==0){
		s2=4;
		break;}

		if(tra2out!=0){
			Bloqueo();
		}
		
		case 3:
		s2=2;
		break;
	}

	switch (c){
		case 2:
		if (tra3out==0 && tra1in==0 && tra2in==0){
		s3=4;
		break;
		}

		if(tra3out!=0){
			Bloqueo();
		}
		case 3:
		s3=2;
		break;
		}
	SemMaster(s1,s2,s3,s4);
}
}

//
//Bidireccional entrando - Bidireccional saliendo - Inhabilitado (BeBsI) (20)
//
void BeBsI(void){
	if(multi==20 && a!=b && a!=c && b!=c){
	AutomBBI();
}
}

//////
//Bidireccional entrando o saliendo - 
////

void BFeFs(void){
	if(multi==24 || multi==30){
		AutomBFF();
	}
}




////////////////////////////////////////////////////////////////////////////////////
// sirve para el multi=20 a=!b,a=!c
void AutomBBI(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2; // registro ensanche 1 y registro ensache 2, de este modo se le llamara al tramo comprometido.
	// siempre r_e1 es el tramo bidireccional que tiene prioridad entrando y r_e2 es el 2do bidireccional con prioridad saliendo.

	//En esta parte del codigo, se detecta que tramo es bidireccional y en que sentido.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		


	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}		
			
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}		



		
	BiBase(r_e1,r_e2);  //prioridad de ensanche 1 al ensanche 2
	return 0;
}

//////////////////////////////////////////////////////////////

void BiBase(r_e1,r_e2){
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e1+30]==0){
				est=1;
				adv_printf("ESTADO ES 1\n");
			}
			}
		}

	if (est==1){
		if(Share_Mem[r_e2]!=0){
			adv_printf("CAMION EN ENSANCHE 2 ESPERANDO\n");
		dpri++;
		}
		
		//if(Share_Mem[r_e2+10]>=2){
		//	adv_printf("ENSANCHE DE SALIDA CON 2 AUTOS\n");
		//	Bloqueo();
		//}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e1+20]==0){
			cont++;
		}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && cont>Share_Mem[vacio]){
			adv_printf("No hay camiones en el ensanche 1, dar paso a camiones del ensanche 2\n");
			est=2;
			cont=0;
		}

		if (dpri>Share_Mem[waitnp] && Share_Mem[r_e2]!=0){
			dpri=0;
			adv_printf("AUTOS DEL ENS2 YA ESPERARON CAMBIA A ESTADO 2\n");
			if(Share_Mem[r_e1+10]==0){
			est=2;
			}
	
		}
		if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0){
			adv_printf("VERDE DE ENSANCHE 1 A ENSANCHE 2\n");

			seminter(r_e1,r_e2);
			}	
		if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0){
			adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
			Bloqueo();
		}
	}

	if(est==2){
		adv_printf("ESPERANDO TRAMO VACIO PARA DAR VERDE A LOS NP\n");
		Bloqueo();
		cesp++;
		if(cesp>Share_Mem[waitzero]){
			adv_printf("TIEMPO CUMPLIDO, REVISAR TRAMO\n");
			cesp=0;
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0){
				adv_printf("TRAMO LIBRE PASAR A ESTADO 3\n");
				est=3;
			}
		}
	}	

	if(est==3){
		vernp++;
		if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0){
		adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
		Bloqueo();
		}
	//	if(Share_Mem[r_e2]==0){
	//		adv_printf("NO HAY AUTOS EN EL ENSANCHE 2, SE VUELVE A ESTADO 0\n");
	//		est=0;
	//	}
		if(Share_Mem[r_e1+10]!=0){
			adv_printf("NO HAY ESPACIO EN EL ENSANCHE 1 VUELVE A 0\n");
			est=0;
		}

		if(vernp>Share_Mem[tnp]){
			adv_printf("TIEMPO EN VERDE LISTO, VUELVE A 0\n");
			vernp=0;
			est=0;
		}
		if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e1+10]==0){
		seminter(r_e2,r_e1);
		adv_printf("ENCENDER NP\n");
	}
	}
}
//////////////////////////////////////
void seminter(int reg1, int reg2){  /////  *** falta actualizar para 4 semaforos

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (reg2){		//como la direcci? es de reg1 a reg2, el semaforo asociado a reg2 tiene que estar en rojo.
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}

	switch (reg1){		//como existen 2 verdes, se debe indical cual de los 2 debe encender(para el caso B-B)
		case en_1_in:
			if(reg2==en_2_in)  //si la salida es por el semaforo de la derecha (en_2_in>en_1_in), entonces el S1 es verde a la derecha ( 3 = rojo derecha)
				S1=3;
			else
				S1=1;
			break;

		case en_2_in:
			if(reg2==en_3_in)
				S2=3;
			else
				S2=1;
			break;

		case en_3_in:
			if(reg2==en_1_in) // aca en_1_in es el que esta a la derecha del en_3_in, porque da la vuelta. para la versi? de 4 semaforos, hay que resolver ***
				S3=3;
			else
				S3=1;
			break;
	}

	SemMaster(S1,S2,S3,S4);

	return 0;
}

//////////////////////////////////////////

void SemMaster (int s1, int s2, int s3, int s4){

	unsigned SB, sem1, sem2, sem3, sem4;

	if(s1==3 && s2==1||s2==3 && s3==1 || s3==3 && s4==1 || s4==3 && s1==1 ||  //flechas opuestas

			s1==1 && s2==1 || s1==3 && s2==3 ||      //felchas semi opuestas // esto se puede optimizar cuando se sepa la posici? del cami? cuando dobla ***
			s2==1 && s3==1 || s2==3 && s3==3 ||
			s3==1 && s4==1 || s3==3 && s4==3 ||
			s4==1 && s1==1 || s4==3 && s1==3// ||

		//	(s1==1 || s1==3) && (s3==1 || s3==3) ||		//felchas cruzadas (solo para 4 piernas) // revisar, creo que no es as?***. 
		//	(s2==1 || s2==3) && (s4==1 || s4==3)		// una opci? de soluci? es contar con 3 flechas para indicar el paso de cada tramo.
		)

	{		//esta serie de if, aseguran que no existan errores en la declaraci? de los sem?oros.
		adv_printf("Error-flechas verdes inconsecuentes\n");		
		Bloqueo();
		return 0;
	}

	else if (s1!=2 && s2!=2 && s3!=2 && s4!=2) {
		adv_printf("Error-todos con verde hacia nodo\n");
		Bloqueo();
		return 0;
	}
	else if(s1==4 && (s2!=2 || s3!=2 || s4!=2) || s2==4 && (s3!=2 || s1!=2 || s4!=2) ||  // esto se puede optimizar cuando se sepa la posici? del cami? cuando dobla **
		    s3==4 && (s1!=2 || s2!=2 || s4!=2) || s4==4 && (s1!=2 || s2!=2 || s3!=2)) {

		adv_printf("Error-semaforo con doble flecha y demas con verde\n");
		Bloqueo();
		return 0;
	}

	switch (s1){
		case 1:			//semaforo 1 verde izquierda
			sem1=1;			
			break;
		case 2:			//semaforo 1 verde rojo
			sem1=2;			
			break;
		case 3:			//semaforo 1 verde derecha
			sem1=4;			
			break;
		case 4:			//semaforo 1 verde izquierda y verde derecha
			sem1=5;			
			break;
		default:
			sem1=2;
			break;
	}

	switch (s2){
		case 1:
			sem2=8;
			break;
		case 2:
			sem2=16;
			break;
		case 3:
			sem2=32;
			break;
		case 4:
			sem2=40;
			break;
		default:
			sem2=16;
			break;
	}

	switch (s3){
		case 1:
			sem3=64;
			break;
		case 2:
			sem3=128;
			break;
		case 3:
			sem3=256;
			break;
		case 4:
			sem3=320;
			break;
		default:
			sem3=128;
			break;
	}

	switch (s4){
		case 1:
			sem4=512;
			break;
		case 2:
			sem4=1024;
			break;
		case 3:
			sem4=2048;
			break;
		case 4:
			sem4=2560;
			break;
		default:
			sem4=1024;
			break;
	}

	Semaforo(sem1,sem2,sem3,sem4);

	return 0;
}
////////////////////////////////////////////////////////////////////

//////////////////////////////////////////

void SemMasterH (int s1, int s2, int s3, int s4){

	unsigned SB, sem1, sem2, sem3, sem4;

	switch (s1){
		case 1:			//semaforo 1 verde izquierda
			sem1=1;			
			break;
		case 2:			//semaforo 1 verde rojo
			sem1=2;			
			break;
		case 3:			//semaforo 1 verde derecha
			sem1=4;			
			break;
		case 4:			//semaforo 1 verde izquierda y verde derecha
			sem1=5;			
			break;
		default:
			sem1=2;
			break;
	}

	switch (s2){
		case 1:
			sem2=8;
			break;
		case 2:
			sem2=16;
			break;
		case 3:
			sem2=32;
			break;
		case 4:
			sem2=40;
			break;
		default:
			sem2=16;
			break;
	}

	switch (s3){
		case 1:
			sem3=64;
			break;
		case 2:
			sem3=128;
			break;
		case 3:
			sem3=256;
			break;
		case 4:
			sem3=320;
			break;
		default:
			sem3=128;
			break;
	}

	switch (s4){
		case 1:
			sem4=512;
			break;
		case 2:
			sem4=1024;
			break;
		case 3:
			sem4=2048;
			break;
		case 4:
			sem4=2560;
			break;
		default:
			sem4=1024;
			break;
	}

	Semaforo(sem1,sem2,sem3,sem4);

	return 0;
}

void Semaforo(int sem1,int sem2,int sem3, int sem4){

	unsigned SB;

	SB=sem1+sem2+sem3+sem4;
	Share_Mem[luces]=SB;
	adv_printf("SB1 %u \n", SB);
	Set5050(&SB,0,0,AWord);
	return 0;
}

//////////////////////////////////////////////////////////////////
void AutomBFF(void){

	//en este codigo se determina que tramo es Bidireccional entrando o saliendo, cual es fijo entrando y cual es el fijo saliendo
	//luego se pasa a la funci? bibase modificada para BFF y luego a seminterBFF, el cual direcionar?los 
	//semaforos correspondientes.

	int  r_Fin, r_Fout, r_Bidir, entrando;

	switch (Share_Mem[mod_tra_1]){		//modo de tramo 1 -> Semaforo 1


		case 2:		//fijo entrando
			r_Fin= en_1_in;
		break;

		case 3:		//fijo saliendo
			r_Fout= en_1_in;	
		break;

		case 4:		//bidireccional  entrando
			r_Bidir= en_1_in;
			entrando=1;
		break;

		case 5:		//bidireccional saliendo
			r_Bidir= en_1_in;
			entrando=0;
		break;
	}

	switch (Share_Mem[mod_tra_2]){		//modo de tramo 2 -> Semaforo 2

		case 2:		//fijo entrando
			r_Fin= en_2_in;
		break;

		case 3:		//fijo saliendo
			r_Fout= en_2_in;	
		break;

		case 4:		//bidireccional  entrando
			r_Bidir= en_2_in;
			entrando=1;
		break;

		case 5:		//bidireccional saliendo
			r_Bidir= en_2_in;
			entrando=0;
		break;
	}

	switch (Share_Mem[mod_tra_3]){		//modo de tramo 3 -> Semaforo 3

		case 2:		//fijo entrando
			r_Fin= en_3_in;
		break;

		case 3:		//fijo saliendo
			r_Fout= en_3_in;	
		break;

		case 4:		//bidireccional entrando
			r_Bidir= en_3_in;
			entrando=1;
		break;

		case 5:		//bidireccional saliendo
			r_Bidir= en_3_in;
			entrando=0;
		break;
	}
		BiBaseBFF(r_Bidir, entrando, r_Fin, r_Fout);
}
/////////////////////////////////////////////////////////////////////////////////////
void BiBaseBFF(r_Bidir, entrando, r_Fin, r_Fout){		//definir los registros de ensanche1, tramo1, tramo2 y ensanche2//el primer registro es el que tiene prioridad entrando.

	estado1=1; // previene que cuando cambie de modo a BiBase ?te parta en estado inicial.

	switch (estado2){		

		case 1:					//en este estado el semaforo esta verde a la prioridad.
			adv_printf("case 1 BiBaseBFF\n");

			if (entrando==1 && Share_Mem[(r_Bidir+30)]!=0x0) 	// x si hay violacion de rojo, o alguien esta en el tramo en contra de sentido de prioridad.
			{
				Bloqueo();										//**** poner mas casos, ya que si van en contra de sentidos fijos, bloqueo general, pero si solo van en contra
																//del sentido prioridad, basta que el semaforo en contra de prioridad de rojo o la flecha que corresponde, no todo el grupo en rojo.
				break;
			}
			else
				seminterBFF(r_Bidir, entrando, r_Fin, r_Fout);	// verde de ensanche e1 al e2


			if ((entrando==1 && Share_Mem[r_Bidir]!=0x0) || (entrando==0 && Share_Mem[r_Fin]!=0)){	// se debe agregar aca el tiempo de espera, o cambiar prioridad.
				adv_printf("if 1\n");
				break;
			}					//significa que hay autos circulando en sentido de prioridad  IF 1

			else if(entrando==1 && Share_Mem[r_Fin]==0x0 || entrando==0 && Share_Mem[r_Bidir]==0x0 ){  //pregunta si hay alguien esperando en sentido contrario a la prioridad
				adv_printf("if 2\n");
				break;
			}

			else if(entrando==1 && Share_Mem[(r_Bidir+10)]!=0x0){ //pregunta si el ensanche siguiente para los vehiculos en contra d eprioridad esta desocupado
				adv_printf("if 3\n");
				break;		// no es necesario revisar en el otro sentido, ya que es fijo saliendo y no tiene problema que se llene.
			}

			else if (entrando==1 && Share_Mem[(r_Bidir+20)]!=0x0 || entrando==0 && Share_Mem[(r_Bidir+30)]!=0x0 && Share_Mem[(r_Fin+20)]!=0x0 ){		// revisa que el tramo est?desocupado
				adv_printf("if 4\n");
				break;
			}



			else{			// si se dan las condiciones para permitir el paso de un vehiculo en sentido contrario, se pasa al estado 2.
				estado2 = 2;
				adv_printf("else 5\n");
				break;
			}

		case 2:			//en este caso se ponen ambos semaforos en rojo hasta que no existan vehiculos en el sentido de prioridad en el tramo

			adv_printf("case 2 BiBaseBFF\n");

			BloqueoBFF(r_Bidir, r_Fin, r_Fout);	//semaforo bidir en rojo en rojo y SFin solo hacia SFout *******************************

			if((entrando==1 && Share_Mem[(r_Bidir+20)]!=0x0)||(entrando==0 && Share_Mem[(r_Bidir+30)]!=0x0))	//mantiene el rojo hasta que salgan los intrusos
				break;

			else if (dtrojo>10){		//delta tiempo en rojo
				dtrojo=0;
				estado2 = 3;
				break;
			}
			else
				dtrojo++;  //delta tiempo en ROJO
				break;

		case 3:

			adv_printf("case 3 BiBaseBFF\n");

			if(entrando==1)
				seminterBFF(r_Bidir, 0, r_Fin, r_Fout);	// verde de ensanche e2 al e1  

			else
				seminterBFF(r_Bidir, 1, r_Fin, r_Fout);	// verde de ensanche e2 al e1  
   
										//semaforo da verde a vehiculo en contra de prioridad

			if(dt2V> 10){				//espera 10 segundos en verde para que solo pasen 2 vehiculos, estos 5 segundos deben
				estado2 = 4;			// especificarse en registro 40120
				dt2V=0;
				break;
			}
			else{
				dt2V++;			//falta declarar dt2v en el main.
				break;
			}

		case 4:

			adv_printf("case 4 BiBaseBFF\n");

			BloqueoBFF(r_Bidir, r_Fin, r_Fout); //semaforo bidir en rojo en rojo y SFin solo hacia SFout

			if ((entrando==0 && Share_Mem[(r_Bidir+20)]!=0x0)||(entrando==1 && Share_Mem[(r_Bidir+30)]!=0x0))
				break;
			
			else if(dtrojo > 10) {	//espera 10 segundos por seguridad a detectar los vehiculos en el tramo antes de pasar al siguiente estado
				estado2=1;
				dtrojo=0;
				break;
			}
			else{
				dtrojo++;
				break;
			}

		default:
			adv_printf("default BiBaseBFF\n");
				break;
	}
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////
void seminterBFF(r_Bidir, entrando, r_Fin, r_Fout){

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_Fout){
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:		//Fout
			S3=2;
			break;
	}

	switch (r_Bidir){
		case en_1_in:
			if(entrando==1 && r_Fout==en_2_in){
				S1=3;
				S3=1;
			}
			else if(entrando==1 && r_Fout==en_3_in){
				S1=1;
				S2=3;
			}
			else if(entrando==0 && r_Fout==en_2_in){
				S1=2;
				S3=4;
			}
			else if(entrando==0 && r_Fout==en_3_in){
				S1=2;
				S2=4;
			}
			break;

		case en_2_in:								//Bin
			if(entrando==1 && r_Fout==en_1_in){
				S2=1;
				S3=3;
			}
			else if(entrando==1 && r_Fout==en_3_in){
				S2=3;								// S2 verde derecha
				S1=1;								// S1 verde izquierda
			}
			else if(entrando==0 && r_Fout==en_1_in){
				S2=2;
				S3=4;
			}
			else if(entrando==0 && r_Fout==en_3_in){
				S2=2;
				S1=4;
			}
			break;

		case en_3_in:
			if(entrando==1 && r_Fout==en_1_in){
				S3=3;
				S2=1;
			}
			else if(entrando==1 && r_Fout==en_2_in){
				S3=1;
				S1=3;
			}
			else if(entrando==0 && r_Fout==en_1_in){
				S3=2;
				S2=4;
			}
			else if(entrando==0 && r_Fout==en_2_in){
				S3=2;
				S1=4;
			}
			break;
	}

	SemMaster(S1,S2,S3,S4);

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
void BloqueoBFF(r_Bidir, r_Fin, r_Fout){  //semaforo bidir en rojo y Sem?oro Fin solo hacia Semaforo Fout

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_Fout){
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;			//S3=2
			break;
	}

	switch (r_Bidir){
		case en_1_in:
				S1=2;		//S1=2
			break;

		case en_2_in:
				S2=2;
			break;

		case en_3_in:
				S3=2;
			break;
	}

	switch (r_Fin){
		case en_1_in:
			if(r_Fout==en_2_in)
				S1=3;
			else
				S1=1;
			break;

		case en_2_in:
			if(r_Fout==en_3_in)
				S2=3;
			else
				S2=1;
			break;

		case en_3_in:
			if(r_Fout==en_1_in)
				S3=3;
			else
				S3=1;
			break;
	}

	SemMaster(S1,S2,S3,S4);

	return 0;	
}

////////////////////////////////////////////////////////////////////////////////////
void AutomBBFs(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2, r_e3; // registro ensanche 1 y registro ensache 2, de este modo se le llamara al tramo comprometido.
	// siempre r_e1 es el tramo bidireccional que tiene prioridad entrando y r_e2 es el 2do bidireccional con prioridad saliendo.

	//En esta parte del codigo, se detecta que tramo es bidireccional y en que sentido.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		
	if(Share_Mem[mod_tra_1]==3){ 	
		r_e3= en_1_in;}	

	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}		
	if(Share_Mem[mod_tra_2]==3){ 	
		r_e3= en_2_in;}

	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}		
	if(Share_Mem[mod_tra_3]==3){ 	
		r_e3= en_3_in;}	

adv_printf("el valor r_e1 %i \n", r_e1);
adv_printf("el valor r_e2 %i \n", r_e2);
adv_printf("el valor r_e3 %i \n", r_e3);
		
		
	BiBaseFs(r_e1,r_e2,r_e3);  //prioridad de ensanche 1 al ensanche 2
	return 0;
}

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

void BiBaseFs(r_e1,r_e2,r_e3){
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 ){
				est=1;
				adv_printf("ESTADO ES 1\n");
			}
			}
		}

	if (est==1){
		
		if(Share_Mem[r_e2]!=0){
		adv_printf("CAMION EN ENSANCHE 2 ESPERANDO\n");
		dpri++;
		}
		if (dpri>Share_Mem[waitnp] && Share_Mem[r_e2]!=0){
		adv_printf("AUTOS DEL ENS2 YA ESPERARON CAMBIA A ESTADO 2\n");
		est=2;
		dpri=0;
		}



		if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
		adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
		Bloqueo();
		}	

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+30]==0){
			cont++;
		}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1+10]==0 && cont>Share_Mem[vacio]){
			adv_printf("No hay camiones en el ensanche 1, dar paso a camiones del ensanche 2\n");
			est=2;
			cont=0;
		}


		if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
			adv_printf("VERDE DE ENSANCHE 1 A ENSANCHE 2\n");
			sesta6(r_e1,r_e2, r_e3);
			}	


	}

	if(est==2){
		adv_printf("ESPERANDO TRAMO VACIO PARA DAR VERDE A LOS NP\n");
		Bloqueo();
		cesp++;
		if(cesp>Share_Mem[waitzero]){
			adv_printf("TIEMPO CUMPLIDO, REVISAR TRAMO\n");
			cesp=0;
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 &&Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
				adv_printf("TRAMO LIBRE PASAR A ESTADO 3\n");
				est=3;
			}
		}
	}	

	if(est==3){
		vernp++;

		if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
			adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
			Bloqueo();
		}

		//if(Share_Mem[r_e2]==0){
		//	adv_printf("NO HAY AUTOS EN EL ENSANCHE 2, SE VUELVE A ESTADO 0\n");
		//	est=0;
		//}
		//if(Share_Mem[r_e1+10]!=0){
		//	adv_printf("NO HAY ESPACIO EN EL ENSANCHE 1 VUELVE A 0\n");
		//	est=0;
		//}

		if(vernp>Share_Mem[tnp]){
			adv_printf("TIEMPO EN VERDE LISTO, VUELVE A 0\n");
			vernp=0;
			est=0;
		}
		if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
		sesta7(r_e1,r_e2,r_e3);
		adv_printf("ENCENDER NP\n");
	}
	}
}
//////////////////////////////////////
void sesta6(int r_e1, int r_e2, int r_e3){  

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){		

	case en_1_in:
		if(Share_Mem[r_e2+10]<=10){
		S1=4;
		break;
	}
		if(r_e2==en_2_in && Share_Mem[r_e2+10]>10){
		S1=1;
		break;
	}
		if(r_e2==en_3_in && Share_Mem[r_e2+10]>10){
		S1=3;
		break;
	}

		case en_2_in:
		if(Share_Mem[r_e2+10]<=10 ){
		S2=4;
		break;
	}
		if(r_e2==en_1_in && Share_Mem[r_e2+10]>10){
		S2=3;
		break;
	}
		if(r_e2==en_3_in && Share_Mem[r_e2+10]>10){
		S2=1;
		break;
	}

		case en_3_in:
		if(Share_Mem[r_e2+10]<=10 ){
		S3=4;
		break;
	}
		if(r_e2==en_1_in && Share_Mem[r_e2+10]>10){
		S3=1;
		break;
	}
		if(r_e2==en_2_in && Share_Mem[r_e2+10]>10){
		S3=3;
		break;
	}
}

	switch (r_e2){		//como la direcci? es de reg1 a reg2, el semaforo asociado a reg2 tiene que estar en rojo.
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}



	SemMaster(S1,S2,S3,S4);

	return 0;
}


////////////////////////////


void sesta7(int r_e1, int r_e2, int r_e3){  /////  *** falta actualizar para 4 semaforos

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){		
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}

	switch (r_e2){		
	case en_1_in:
		if(Share_Mem[r_e1+10]==0){
		S1=4;
		break;
	}
		if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0){
		S1=1;
		break;
	}
		if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0){
		S1=3;
		break;
	}

		case en_2_in:
		if(Share_Mem[r_e1+10]==0){
		S2=4;
		break;
	}
		if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0){
		S2=3;
		break;
	}
		if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0){
		S2=1;
		break;
	}
		case en_3_in:
		if(Share_Mem[r_e1+10]==0){
		S3=4;
		break;
	}
		if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0){
		S3=1;
		break;
	}
		if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0){
		S3=3;
		break;
	}
}
	SemMaster(S1,S2,S3,S4);

	return 0;
}



////////////////////////////////////////////////////////////////////////////////////
void AutomBBFe(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2 , r_e3; // r_e1 es el bidireccional entrando, r_e2 es el bidireccional saliendo y r_e3 es el fijo entrando.

	//En esta parte del codigo, se detecta que tramo es bidireccional y fijo.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		
	if(Share_Mem[mod_tra_1]==2){ 	
		r_e3= en_1_in;}	


	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}	
	if(Share_Mem[mod_tra_2]==2){ 	
		r_e3= en_2_in;}		
			
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}	
	if(Share_Mem[mod_tra_3]==2){ 	
		r_e3= en_3_in;}		



		
	BiBaseFe(r_e1,r_e2,r_e3);  //prioridad de ensanche 1 al ensanche 2
	return 0;
}



void BiBaseFe(r_e1,r_e2,r_e3){

if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta1( r_e1, r_e2, r_e3);

	if(Share_Mem[r_e2]!=0 || Share_Mem[r_e3]!=0){
	contador++;
	adv_printf("Camion esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[waitnp]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	if(Share_Mem[r_e1+10]==0){
	est=2;
	}
	contador=0;

	}

	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && (Share_Mem[r_e2]!=0 || Share_Mem[r_e3]!=0)){
		cu++;
	}

	if(cu>Share_Mem[vacio] && Share_Mem[r_e1]==0 && Share_Mem[r_e1+10]==0){
		cu=0;
		est=2;

	}


	if(Share_Mem[r_e2+10]>=10){
	adv_printf(" Ensanche de salida lleno, pasar a estado 0\n");
	Bloqueo();
	}

	if(Share_Mem[r_e2+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}
	}

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(Share_Mem[r_e1+10]!=0){
		est=0;
		adv_printf("Ensanche sin espacio\n");
	}
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e1+10]==0 ){
		est=3;
		adv_printf("Tramo libre, verde contra la prioridad \n");
	}
}
}


if(est==3){
	adv_printf(" Verde contra la prioridad\n");
	sesta2(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

}

////////////////////////////////

void sesta1(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){
	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S1=1;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
		S1=2;
		break;
	}


	case en_2_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S2=3;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
	S2=2;
	break;
	}

	case en_3_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S3=1;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
	S3=2;
	break;
	}
}

switch (r_e2){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch(r_e3){
	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S1=3;
	break;
    }
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S1=1;
	break;
	}
	if(Share_Mem[r_e3+30]!=0){
	S1=2;
	break;	
	}

	case en_2_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S2=1;
	break;
    }
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S2=3;
	break;
	}
	if(Share_Mem[r_e3+30]!=0){
	S2=2;
	break;	
	}

	case en_3_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S3=1;
	break;
    }
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S3=3;
	break;
	}

	if(Share_Mem[r_e3+30]!=0){
	S3=2;
	break;	
	}
}
SemMaster(S1,S2,S3,S4);

}

////////////////////////////////////
void sesta2(r_e1,r_e2,r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;
switch (r_e1){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){
	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S1=1;
		break;
	}
	if(Share_Mem[r_e2+30]!=0){
		S1=2;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S2=3;
		break;
	}

	if(Share_Mem[r_e2+30]!=0){
		S2=2;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S3=3;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S3=1;
		break;
	}
	if(Share_Mem[r_e2+30]!=0){
		S3=2;
	}
}

switch (r_e3){
	case en_1_in:
	//if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S1=2;
		break;
	//}
	//if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S1=1;
		break;
	//}
	
	//if(Share_Mem[r_e3+30]!=0){
	//	S1=2;
	//}

	case en_2_in:
	//if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S2=2;
		break;
	//}
	//if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S2=3;
	//	break;
	//}
	//if(Share_Mem[r_e3+30]!=0){
	//	S2=2;
	//}

	case en_3_in:
	//if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S3=2;
		break;
	//}
	//if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S3=1;
	//	break;
	//}
	//if(Share_Mem[r_e3+30]!=0){
	//	S3=2;
	//}
}
SemMaster(S1,S2,S3,S4);
}
////////////////

//Modo BeBsBs

void AutomBeBsBs(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2 , r_e3; // r_e1 es el bidireccional entrando, r_e2 es el bidireccional saliendo y r_e3 es el fijo entrando.

	//En esta parte del codigo, se detecta que tramo es bidireccional y fijo.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;			
		r_e2= en_2_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}



adv_printf("el valor r_e1 %i \n", r_e1);
adv_printf("el valor r_e2 %i \n", r_e2);
adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeBB(r_e1,r_e2,r_e3);

	return 0;
}

////////////////////////////////////////////////

void AutomBeBB(r_e1,r_e2,r_e3){

//----------------------------ESTADO 0-------------------------//
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//----------------------------ESTADO 1-------------------------//
if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta3( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0 && Share_Mem[r_e2+30]==0){
	cp1++;
	adv_printf("ENSANCHE Y TRAMO CON PRIORIDAD EN 0\n");
	}

	if(cp1>Share_Mem[vacio] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e3+10]<=10)){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3]!=0 && Share_Mem[r_e3+30]==0){
	cp2++;
	adv_printf("ENSANCHE Y TRAMO CON PRIORIDAD EN 0\n");
	}

	if(cp2>(Share_Mem[vacio]+2) && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]<=10)){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo\n");
	    est=2;
	    E2=1;
	    E1=0;
	}


	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contador++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[waitnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e3+10]<=10)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	contador=0;
	conta=2;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]<=10)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contador=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0){
		est=3;
		adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e1 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0){
		est=4;
		adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e1 \n");
	}

}
}

//------------------------ESTADO 3----------------------------------//

if(est==3){
	adv_printf(" Verde a r_e2\n");
	sesta4(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}


if(est==4){
	adv_printf(" Verde a r_e3\n");
	sesta5(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

}

///////////////////////////////////////////////////

void sesta3(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){
	case en_1_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}


	case en_2_in:
	
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}

	case en_3_in:
	
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}	
}

switch (r_e2){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch (r_e3){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

SemMaster(S1,S2,S3,S4);



}

///////////////////////////////

void sesta4(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}


switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10 ){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10 ){
	S1=1;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10 ){
	S1=3;
	break;
	}

	/////////////////

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10 ){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10 ){
	S1=3;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}



	case en_2_in:
	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}	

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}	

	case en_3_in:
	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}


}
switch (r_e3){

	case en_1_in:
	S1=2;

	case en_2_in:
	S2=2;

	case en_3_in:
	S3=2;

}



SemMaster(S1,S2,S3,S4);

}

////////

void sesta5(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

}

switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S1=3;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S2=3;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S2=1;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S2=1;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S2=3;
	break;
	}


	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S3=1;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S3=3;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S3=1;
	break;
	}

}
SemMaster(S1,S2,S3,S4);
}

//--------------------------------------

void Hr(void){

	int r_e1, r_e2 , r_e3, r_e4;

		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;
		r_e4= en_4_in;			

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
	adv_printf("el valor r_e4 %i \n", r_e4);	
		
	H(r_e1,r_e2,r_e3,r_e4);

}

void H(r_e1,r_e2,r_e3,r_e4){
unsigned S1, S2, S3, S4;
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[BLOQH]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e4+20]==0 && Share_Mem[r_e4+30]==0){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

if (est==1){

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0 && Share_Mem[mod_tra_1]!=1){
	c1++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(c1>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=1;
	E2=0;
	E3=0;
	E4=0;
	c1=0;
	c2=3;
	c3=2;
	c4=1;
	}

	if(Share_Mem[r_e2]!=0 && Share_Mem[mod_tra_2]!=1 ){
	c2++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(c2>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=1;
	E3=0;
	E4=0;
	c1=1;
	c2=0;
	c3=3;
	c4=2;
	}

	if(Share_Mem[r_e3]!=0 && Share_Mem[mod_tra_3]!=1){
	c3++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(c3>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=0;
	E3=1;
	E4=0;
	c1=2;
	c2=1;
	c3=0;
	c4=3;
	}


	if(Share_Mem[r_e4]!=0 && Share_Mem[mod_tra_4]!=1){
	c4++;
	adv_printf("Camion de r_e4 esperando contra la prioridad \n");
	}

	if(c4>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=0;
	E3=0;
	E4=1;
	c1=3;
	c2=2;
	c3=1;
	c4=0;
	}

if(Share_Mem[r_e1+20]!=0 && Share_Mem[r_e1+30]!=0 && Share_Mem[r_e2+20]!=0 && Share_Mem[r_e2+30]!=0 && Share_Mem[r_e3+20]!=0 && Share_Mem[r_e3+30]!=0 && Share_Mem[r_e4+20]!=0 && Share_Mem[r_e4+30]!=0){
est=0;
adv_printf(" Camiones en el tramo\n");			
}
}

if (est==2){
	S1=2;
	S2=2;
	S3=2;
	S4=2;
	if(E1==1){
	cn1++;
	if(cn1>Share_Mem[REVH] &&(Share_Mem[r_e2]!=0 ||Share_Mem[r_e3]!=0 ||Share_Mem[r_e4]!=0)){
	cn1=0;
	est=0;}	
	if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+20]==0){
	S1=4;
	}
	}

	if(E2==1){	
	cn2++;
	if(cn2>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e3]!=0 ||Share_Mem[r_e4]!=0)){
	cn2=0;
	est=0;}	
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+20]==0){
	S2=4;
	}
	}
	if(E3==1){
	cn3++;	
	if(cn3>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e2]!=0 ||Share_Mem[r_e4]!=0)){
	cn3=0;
	est=0;}	
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e4+20]==0){
	S3=4;
	}
	}

	if(E4==1){	
	cn4++;
	if(cn4>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e2]!=0 ||Share_Mem[r_e3]!=0)){
	cn4=0;
	est=0;}		
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+30]==0){
	S4=4;
	}
	}
SemMaster(S1,S2,S3,S4);
}
}

///////////////////////

void AutomBeBeBs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==5){ 	
		r_e1= en_2_in;
		r_e2= en_3_in;		
		r_e3= en_1_in;			

	}
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e1= en_1_in;		
		r_e2= en_3_in;
		r_e3= en_2_in;
	}
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e1= en_1_in;		
		r_e2= en_2_in;
		r_e3= en_3_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeBeB(r_e1,r_e2,r_e3);

}

/////////////////////

void AutomBeBeB(r_e1,r_e2,r_e3){

	//----------------------------ESTADO 0-------------------------//
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//----------------------------ESTADO 1-------------------------//
if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta8( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e2]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e1]!=0){
	cp1++;
	adv_printf("ENSANCHE 1 ESPERANDO POR R_E2 Y ENSANCHE 2 Y TRAMO EN 0\n");
	}

	if(cp1>(Share_Mem[vacio]+2) && Share_Mem[r_e2+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	    E2=0;
	    E3=0;
	}
	
	////////////////
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0){
	cp2++;
	adv_printf("ENSANCHE 2 ESPERANDO POR R_E1 Y ENSANCHE 1 Y TRAMO EN 0\n");
	}

	if(cp2>Share_Mem[vacio] && Share_Mem[r_e1+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	    E3=0;
	}

	////////////////

	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3]!=0 && (Share_Mem[r_e1]==0 || Share_Mem[r_e2]==0)){
	cp3++;
	adv_printf("ENSANCHE 3 ESPERANDO POR R_E1 Y ENSANCHE 1 Y TRAMO EN 0\n");
	}

	if(cp3>(Share_Mem[vacio]+4) && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]==0)){
		cp3=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e3\n");
	    est=2;
	    E2=0;
	    E1=0;
	    E3=1;
	}


	//Si en el ensanche de r_e1 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0){
	contador++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[tnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	E3=0;
	est=2;
	contador=0;
	conta=2;
	contad=4;
	}


	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[tnp] && Share_Mem[r_e1+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	E3=0;
	est=2;
	contador=2;
	conta=4;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[tnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]==1)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=0;
	E3=1;
	est=2;
	conta=0;
	contador=4;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e1 a r_e2 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e1 \n");
	}
	if(E3==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=5;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a (r_e1 y r_e2) \n");
	}

}
}

//----------------------------ESTADO 3-------------------------//

if(est==3){
	adv_printf(" Verde hacia r_e2\n");
	sesta9(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

//----------------------------ESTADO 4-------------------------//

if(est==4){
	adv_printf(" Verde hacia r_e1\n");
	sesta10(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}	

//----------------------------ESTADO 5-------------------------//

if(est==5){
	adv_printf(" Verde hacia r_e1 y r_e2\n");
	sesta11(r_e1, r_e2, r_e3);
	tverde3++;
	if(tverde3>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde3=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
}


///////////////////////////////////////////////////

void sesta8(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:

	if(r_e2==en_2_in && Share_Mem[r_e3+10]<=10){
	
		S1=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]<=10){

		S1=3;
		break;
	}

	case en_2_in:

	if(r_e2==en_1_in && Share_Mem[r_e3+10]<=10){

		S2=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]<=10){

		S2=1;
		break;
	}	

	case en_3_in:
	
	if(r_e2==en_1_in && Share_Mem[r_e3+10]<=10){
		S3=1;

		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]<=10){

		S3=3;
		break;
	}
}

switch (r_e2){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){

		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){

		S1=3;
		break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
	
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){

		S2=1;
		break;
	}

	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){

		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){

		S3=3;
		break;
	}
}

switch (r_e3){

	case en_1_in:

	S1=2;
	break;


	case en_2_in:

	S2=2;
	break;

	case en_3_in:

	S3=2;
	break;
}
SemMaster(S1,S2,S3,S4);
}
	
///////////////////////////////////////////////////

void sesta9(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=1;
	break;
	}	
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=3;
	break;
	}


	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=3;
	break;
	}	
	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}


	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}	

	case en_3_in:

	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}	
	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}


	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}	
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}	
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

}

switch (r_e3){
		case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

}
SemMaster(S1,S2,S3,S4);
}

///////////////////////////////////////////////
void sesta10(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=3;
	break;
	}


	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}


	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}


	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}


	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}
}

switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}
SemMaster(S1,S2,S3,S4);
}


////////////////////////////////////////////////

void sesta11(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
}


switch (r_e3){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S1=3;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S2=1;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S2=3;
	break;
	}


	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S3=1;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S3=3;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S3=1;
	break;
	}
}
SemMaster(S1,S2,S3,S4);	
}

//------------------------------------------------------------

void AutomFeBsBs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 	
		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;			

	}
	if(Share_Mem[mod_tra_2]==2){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==2){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomFeBB(r_e1,r_e2,r_e3);

}


void AutomFeBB(r_e1,r_e2,r_e3){


//---------------------------------------------E0--------------------
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//---------------------------------------------E1--------------------

if (est==1){

adv_printf(" verde a la Prioridad\n");
	sesta12( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0 && Share_Mem[r_e2+30]==0){
	cp1++;
	adv_printf("ENSANCHE 2 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	////////////////
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3]!=0 && Share_Mem[r_e3+30]==0){
	cp2++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp2>(Share_Mem[vacio]+2) && Share_Mem[r_e2+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	}

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	conta=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e3 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}

}
}

//----------------------------ESTADO 3-------------------------//

if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta13(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	//----------------------------ESTADO 4-------------------------//

if(est==4){
	adv_printf(" Verde hacia r_e2\n");
	sesta14(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
}	

//---------------------------------------------------------------


void sesta12(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S3=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S3=3;
		break;
	}
}


switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}

switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}
SemMaster(S1,S2,S3,S4);	
}
	
//---------------------------------------------------------------


void sesta13(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e3+10]<=10){
		S3=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
}


switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S3=2;
		break;
	}

}

switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}
SemMaster(S1,S2,S3,S4);	
}

//---------------------------------------------------------------


void sesta14(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e2+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S3=1;
		break;
	}
}



switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}


switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e2+10]<=10){
		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e2+10]<=10){
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e2+10]<=10){
		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S3=2;
		break;
	}

}
SemMaster(S1,S2,S3,S4);	

}
//----------------------------------------------

void AutomFsBeBe(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==3){ 	
		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;			

	}
	if(Share_Mem[mod_tra_2]==3){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomFsBB(r_e1,r_e2,r_e3);

}


//-------------------------------------------------------------------------------

void AutomFsBB(r_e1,r_e2,r_e3){


//---------------------------------------------E0--------------------
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//---------------------------------------------E1--------------------

if (est==1){

adv_printf(" verde a la Prioridad\n");
	sesta15( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e3]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e2]!=0){
	cp1++;
	adv_printf("ENSANCHE 2 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	////////////////
	if(Share_Mem[r_e2]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3]!=0){
	cp2++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp2>Share_Mem[vacio] && Share_Mem[r_e2+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	}

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	conta=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e3 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}

}
}

//----------------------------ESTADO 3-------------------------//

if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta16(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	//----------------------------ESTADO 4-------------------------//

if(est==4){
	adv_printf(" Verde hacia r_e2\n");
	sesta17(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
}	

//---------------------------------------------------------------


void sesta15(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}


switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in){
		S1=3;
		break;
	}
	if(r_e1==en_3_in){
		S1=1;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in){
		S2=1;
		break;
	}
	if(r_e1==en_3_in){
		S2=3;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in){
		S3=3;
		break;
	}
	if(r_e1==en_2_in){
		S3=1;
		break;
	}
}


switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in){
		S1=3;
		break;
	}
	if(r_e1==en_3_in){
		S1=1;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in){
		S2=1;
		break;
	}
	if(r_e1==en_3_in){
		S2=3;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in){
		S3=3;
		break;
	}
	if(r_e1==en_2_in){
		S3=1;
		break;
	}


}
SemMaster(S1,S2,S3,S4);	
}
	
//---------------------------------------------------------------


void sesta16(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){

	case en_1_in:
	if(Share_Mem[r_e3+10]==0){
		S1=4;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]!=0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e3+10]==0){
		S2=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e3+10]!=0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]!=0){
		S2=3;
		break;
	}
	case en_3_in:
	if(Share_Mem[r_e3+10]==0){
		S3=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e3+10]!=0){
		S3=3;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]!=0){
		S3=1;
		break;
	}
}



switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}
SemMaster(S1,S2,S3,S4);	
}

//---------------------------------------------------------------


void sesta17(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
}


switch (r_e3){

	case en_1_in:
	if(Share_Mem[r_e2+10]==0){
		S1=4;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]==0){
		S2=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S2=3;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]==0){
		S3=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0){
		S3=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S3=1;
		break;
	}
}

SemMaster(S1,S2,S3,S4);	

}
//----------------------------------------------

//--------------------------------------

void AutomBsFeFs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 			
		r_e1=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==3){ 			
		r_e2=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==5){ 			
		r_e3=en_1_in;
	}


	if(Share_Mem[mod_tra_2]==2){ 			
		r_e1=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==3){ 			
		r_e2=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==5){ 			
		r_e3=en_2_in;
	}

	if(Share_Mem[mod_tra_3]==2){ 			
		r_e1=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 			
		r_e2=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==5){ 			
		r_e3=en_3_in;
	}


	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBsFF(r_e1,r_e2,r_e3);

}


//-------------------------------------------------------------------------------

void AutomBsFF(r_e1,r_e2,r_e3){


//---------------------------------------------E0--------------------
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//---------------------------------------------E1--------------------

if (est==1){

adv_printf(" verde a la Prioridad\n");
	sesta18( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e3]!=0){
	cp1++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]<=10){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e3\n");
	    est=2;
	    E1=1;
	}
	

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e3]!=0){
	contad++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	est=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>5){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}
}
}

//----------------------------ESTADO 3-------------------------//

if(est==3){
	adv_printf(" Verde hacia r_e2\n");
	sesta19(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
}


//---------------------------------------------------------------


void sesta18(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;



switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]>10){
		S1=1;
		break;
	}

	case en_2_in:

	if(Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]>10){
		S2=3;
		break;
	}


	case en_3_in:
	
	if(Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]>10){
		S3=1;
		break;
	}
}



switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}


switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;


}
SemMaster(S1,S2,S3,S4);	
}
	
//---------------------------------------------------------------


void sesta19(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
}
	if(r_e2==en_3_in){
	S1=1;
	break;
}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
}
	if(r_e2==en_3_in){
	S2=3;
	break;
}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
}
	if(r_e2==en_2_in){
	S3=1;
	break;
}
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}



switch (r_e3){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
}
	if(r_e2==en_3_in){
	S1=1;
	break;
}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
}
	if(r_e2==en_3_in){
	S2=3;
	break;
}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
}
	if(r_e2==en_2_in){
	S3=1;
	break;
}
}

SemMaster(S1,S2,S3,S4);	
}

//---------------------------------------------------------------
void AutomBeFeFs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 			
		r_e1=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==3){ 			
		r_e2=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==4){ 			
		r_e3=en_1_in;
	}


	if(Share_Mem[mod_tra_2]==2){ 			
		r_e1=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==3){ 			
		r_e2=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==4){ 			
		r_e3=en_2_in;
	}

	if(Share_Mem[mod_tra_3]==2){ 			
		r_e1=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 			
		r_e2=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==4){ 			
		r_e3=en_3_in;
	}


	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeFF(r_e1,r_e2,r_e3);

}


//-------------------------------------------------------------------------------

void AutomBeFF(r_e1,r_e2,r_e3){


//---------------------------------------------E0--------------------
if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

//---------------------------------------------E1--------------------

if (est==1){

adv_printf(" verde a la Prioridad\n");
	sesta20( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e3]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e1]!=0){
	cp1++;
	adv_printf("reg 1 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	}
	

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0){
	contad++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	est=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e1 a r_e3 \n");
	}
}
}

//----------------------------ESTADO 3-------------------------//

if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta21(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
}



//---------------------------------------------------------------


void sesta20(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;

switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
}
	if(r_e2==en_3_in){
	S1=1;
	break;
}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
}
	if(r_e2==en_3_in){
	S2=3;
	break;
}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
}
	if(r_e2==en_2_in){
	S3=1;
	break;
}
}

switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}



switch (r_e3){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
}
	if(r_e2==en_3_in){
	S1=1;
	break;
}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
}
	if(r_e2==en_3_in){
	S2=3;
	break;
}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
}
	if(r_e2==en_2_in){
	S3=1;
	break;
}

}

SemMaster(S1,S2,S3,S4);	
}

//---------------------------------------------------------------


void sesta21(r_e1, r_e2, r_e3){
unsigned S1=2, S2=2, S3=2, S4=2;



switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]==0){
		S1=4;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]!=0){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:

	if(Share_Mem[r_e3+10]==0){
		S2=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]!=0){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]!=0){
		S2=3;
		break;
	}


	case en_3_in:
	
	if(Share_Mem[r_e3+10]==0){
		S3=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]!=0){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]!=0){
		S3=1;
		break;
	}
}



switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

}


switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;


}
SemMaster(S1,S2,S3,S4);	
}
//--------------------

//void puerta(void){

//Share_Mem[puert]=1;
//Get5050(0,13,ABit,&ESTA);
//Share_Mem[puert]=ESTA

//}