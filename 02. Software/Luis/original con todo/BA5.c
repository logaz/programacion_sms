//-------------------------------------------------------------------------------------
//	Program: INTERCAT.c 
	//
	//	Author: A.Quezada, Mining Tag SA.
	//			L.Ogaz, Mining Tag SA.
	//
	//	Description: This program is designed for Adam-5510/TCP with ADAM-5050
	//				 This program is single-
	//	             threaded but multi-connections capability program.
	//				 				
	//
	//	History:
	//			Version 1.0  10/03/2017 created by Luis Ogaz
//------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------

#include "mod.h"
#include "5510drv.h"

#define DATASIZE 250
#define sizeofShareMem	1000

//Registros

	#define modo 				100
	#define automatico 			0x1
	#define seguro 	 			0x2
	#define manual 				0x3
	#define manualH 			0x4
	#define emergencia 			0x5
	#define cambiocontingencia  0xa
	#define bloqueo 			0xb  	//11
	#define test 				0xc  	//12
	#define off 				0xd		//13
	#define setear 				0xe		//14
 
	#define en_1_in  	101  //nombre SEM 1
	#define en_1_out 	111
	#define tra_1_in	121
	#define tra_1_out	131
	#define mod_tra_1 	141

	#define en_2_in  	102  //nombre SEM 2
	#define en_2_out 	112
	#define tra_2_in	122
	#define tra_2_out	132
	#define mod_tra_2 	142

	#define en_3_in  	103  //nombre SEM 3
	#define en_3_out 	113
	#define tra_3_in	123
	#define tra_3_out	133
	#define mod_tra_3 	143

	#define en_4_in  	104  //nombre SEM 4
	#define en_4_out 	114
	#define tra_4_in	124
	#define tra_4_out	134
	#define mod_tra_4 	144

	#define s1m			190
	#define s2m 		191
	#define s3m 		192
	#define s4m 		193

	// MEMORIA NO VOLATIL:

	#define semnumber	145
	#define ROJO1		146
	#define ROJO2		147
	#define ROJO3		148
	#define ROJO4		149
	#define VERDTOTAL	150
	#define offline		151
	#define RST         152
	#define ROJODE		153

	#define BLOQH		160
	#define REVH   		161
	#define C1H	 		162


	#define timebloq 	194
	#define waitnp 		195
	#define waitzero	196
	#define tnp      	197
	#define vacio      	198

	
	#define RELOJ 		199
	#define year 		200
	#define month 		201
	#define day 		202
	#define hour 		203
	#define minute 		204
	#define second 		205


//VARIABLES //

	unsigned int Share_Mem[sizeofShareMem];

	//variables Modo Emergencia //
	int count2=0;


	//Variables Modo Test //
	int count3=0;

	//Variables servidor //
	int desconexion;

	//Variables Modo seguro
	int segtrojo;
	int segestado=0;
	int segtdefault;
	int segtverde;

	//Variables Semmaster y Semaforo
	int sem1;
	int s1;
	int s2;
	int s3;
	int s4;


	//Variables modo ModbusReset
	int modbusreset=1;

	// Variables modo Automatico
	int a;
	int b;
	int c;
	int d;

	// Variables Modo Bidireccional
	int est=0;
	int cbloq=0;
	int dpri=0;
	int cesp=0;
	int vernp=0;

	// Variables Modo AutomaticoXXX
	int en1in;
	int en1out;
	int tra1in;
	int tra1out;
	int en2in;
	int en2out;
	int tra2in;
	int tra2out;
	int en3in;
	int en3out;
	int tra3in;
	int tra3out;
	int en4in;
	int en4out;
	int tra4in;
	int tra4out;
	int multi;
	int suma;

	// Variables Modo WDT_Control
	int wdt 		= 10;
	int wdt_off		= 2;
	int wdt_rst		= 9;

	int contador=0;
	int tverde=0;
	int tverde2=0;
	int tverde3=0;
	int cuenta=0;
	int E1=0;
	int E2=0;
	int E3=0;
	int E4=0;
	int cont=0;
	int conta=0;
	int cp1=0;
	int cp2=0;
	int cp3=0;
	int cu=0;
	int contad=0;
	int c1=0;
	int c2=0;
	int c3=0;
	int c4=0;
	int cn1=0;
	int cn2=0;
	int cn3=0;
	int cn4=0;
	int ESTA;
	unsigned char sector;
	//unsigned long addr;
	unsigned char data;
	unsigned char VERD;
	unsigned char ROJ1;
	unsigned char ROJ2;
	unsigned char ROJ3;
	unsigned char ROJ4;
	unsigned char ROJDE;
	unsigned char desconectado;
	unsigned char MDBRST;
	char* probando;
	int transformacion;
	unsigned char YEAR;
	unsigned char MONTH;
	unsigned char DAY;
	unsigned char HOUR;
	unsigned char MINUTE;
	unsigned char SECOND;
	int countgab=0;
	int posicion=0;
	int ES1=0;
	int ES2=0;


//Funciones
	void BiBaseFs(int r_e1, int r_e2, int r_e3);
	void sesta6(int r_e1, int r_e2, int r_e3);
	void sesta7(int r_e1, int r_e2, int r_e3);
	void sesta8(int r_e1, int r_e2, int r_e3);
	void sesta9(int r_e1, int r_e2, int r_e3);
	void sesta10(int r_e1, int r_e2, int r_e3);
	void sesta11(int r_e1, int r_e2, int r_e3);
	void sesta12(int r_e1, int r_e2, int r_e3);
	void sesta13(int r_e1, int r_e2, int r_e3);
	void sesta14(int r_e1, int r_e2, int r_e3);
	void sesta15(int r_e1, int r_e2, int r_e3);
	void sesta16(int r_e1, int r_e2, int r_e3);
	void sesta17(int r_e1, int r_e2, int r_e3);
	void sesta18(int r_e1, int r_e2, int r_e3);
	void sesta19(int r_e1, int r_e2, int r_e3);
	void sesta20(int r_e1, int r_e2, int r_e3);
	void sesta21(int r_e1, int r_e2, int r_e3);
	unsigned int LocalDI(void);  //retorna un int sin argumento
	void Emergencia(void);
	void Cambio(void);
	void Off(void);
	void Test(void);
	void PinOut(void);
	void Door(void);
	void Reloj(void);
	void Modo(void);
	void III(void);
	void FeFsI(void);
	void BeFeI(void);
	void BsFeI(void);
	void BeFsI(void);
	void FeFeFs(void);
	void BsFsI(void);
	void BeFeFe(void);
	void FeFsFs(void);
	void BsFeFe(void);
	void BeBsI(void);
	void BeFsFs(void);
	void BsFsFs(void);
	void Manual(void); 				//**** crear modo manual
	void ManualH(void); 
	void SecureMode(void);
	void WDT_control(void);
	void Bloqueo (void);
	void AutomaticoXXX(void);
	void AutomIII(void);
	void AutomIFF(void);
	void AutomBFF(void);
	void AutomBBI(void);
	void AutomBBB(void);
	void AutomBBBB(void);
	void Seguro(void);
	void ModbusReset(void);
	void SemMaster(int S1, int S2, int S3, int s4);
	void SemMasterH(int S1, int S2, int S3, int s4);
	void Semaforo(int sem1,int sem2,int sem3, int sem4);
	void BiBase(int r_e1, int r_e2);
	void seminter(int reg1, int reg2);
	void BFeFs(void);
	void AutomBBFs(void);
	void AutomBBFe(void);
	void BiBaseFe(int r_e1,int r_e2, int r_e3);
	void sesta1(int r_e1, int r_e2,int r_e3);
	void sesta2(int r_e1,int r_e2,int r_e3);
	void AutomBeBsBs(void);
	void sesta3(int r_e1, int r_e2,int r_e3);
	void sesta4(int r_e1,int r_e2,int r_e3);
	void sesta5(int r_e1, int r_e2, int r_e3);
	void AutomBeBB(int r_e1,int r_e2,int r_e3);
	void AutomFeBsBs(void);
	void AutomFeBB(int r_e1,int r_e2,int r_e3);
	void AutomBeFeFs(void);
	void AutomBeFF(int r_e1,int r_e2,int r_e3);
	void AutomBeBeBs(void);
	void AutomBeBeB(int r_e1,int r_e2,int r_e3);
	void AutomFsBeBe(void);
	void AutomFsBB(int r_e1,int r_e2,int r_e3);
	void AutomBsFeFs(void);
	void AutomBsFF(int r_e1,int r_e2,int r_e3);
	void Hr(void);
	void H(int r_e1,int r_e2,int r_e3, int r_4);
	void Setear (void);
	void Gabinete (void);

//CORE 

int main(void){

	SOCKET Sock_5510;
	int err_code;
	int serv_code;
	int tmpidx;
	int pinout;

	//int CC;



	memset(Share_Mem, 0, sizeof(Share_Mem));
	if((err_code=ADAMTCP_ModServer_Create(502, 5000, 7,
										  (unsigned char *)Share_Mem, sizeof(Share_Mem)))!=0)	//first step
	{
		adv_printf("error code is %d\n", err_code);
	}
	
	Share_Mem[modo]=seguro;   //ADAM start in secure mode
	Share_Mem[wdt]=0x0000;

	//SetDebugMessageState(1);

	Timer_Init();
	tmpidx = Timer_Set(1000);
	adv_printf("Server started, wait for connect...\n");

	WDT_enable();

	while(1)
	{
		serv_code=ADAMTCP_ModServer_Update();	//second step: return 0 NO packet, return 1 has packet
		if(serv_code!=0){
			adv_printf("packet N is %d\n", serv_code);
			if(serv_code==1)
				desconexion=0;
		}
		if(tmArriveCnt[tmpidx])
		{

			Timer_Reset(tmpidx);
			disable();


			WDT_control();
			SecureMode();
			PinOut();

			Modo();

			enable();
		}
	}
	ADAMTCP_ModServer_Release();

	WDT_disable();
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void WDT_control(void){

	if (Share_Mem[wdt]==wdt_off){
		WDT_disable();
		adv_printf("WDT disable \n");
	}

	else if (Share_Mem[wdt]==wdt_rst){
		WDT_clear();
		ADAMdelay(3000);
	}

	else
		WDT_clear();
	}

//////////////////////////////////////////////////////////////////////////////

void SecureMode(void){

	if(desconexion>=(7+read_backup_ram(16)) && desconexion<=(8+read_backup_ram(17))){			////// se debe poder configurar estos tiempos de desconexi? ***
		Share_Mem[modo]=seguro;
			ModbusReset();
	}

	else if(desconexion>=(8+read_backup_ram(17))){
		desconexion=0;
		modbusreset=1;

	}

	desconexion++;
	}

//////////////////////////////////////////////////////////////////////////////

void ModbusReset(void){

	switch(modbusreset){

		case 1:
		ADAMTCP_ModServer_Release();
		modbusreset=2;
		adv_printf("MDB release \n");
		break;

		case 2:
		ADAMTCP_ModServer_Create(502, 5000, 7,(unsigned char *)Share_Mem, sizeof(Share_Mem));
		modbusreset=0;
		adv_printf("MDB create\n");
		break;
	}
	}

//////////////////////////////////////////////////////////////////////////////

unsigned int LocalDI(void){
	unsigned DI;
	Get5050(0,0,AWord,&DI);
	return (unsigned int) DI;
	}

//////////////////////////////////////////////////////////////////////////////

void Door(void){
	unsigned puerta;
	Get5050(0,13,ABit,&puerta);
	Share_Mem[2]=puerta;
	}

//////////////////////////////////////////////////////////////////////////////

void PinOut(void){
	Share_Mem[1] = LocalDI();
	Share_Mem[8] = modbusreset;
	Share_Mem[9] = desconexion;

	adv_printf("modbus reset %d\n",modbusreset);
	adv_printf("desconexion %d\n",desconexion);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void Reloj(void){
	YEAR=GetRTCtime(RTC_year);
	MONTH=GetRTCtime(RTC_month);
	DAY=GetRTCtime(RTC_week);
	HOUR=GetRTCtime(RTC_hour);
	MINUTE=GetRTCtime(RTC_min);
	SECOND=GetRTCtime(RTC_sec);

	Share_Mem[206]=YEAR;
	Share_Mem[207]=MONTH;
	Share_Mem[208]=DAY;
	Share_Mem[209]=HOUR;
	Share_Mem[210]=MINUTE;
	Share_Mem[211]=SECOND;
	}

//////////////////////////////////////////////////////////////////////////////

void Modo(void){ 					// este es el menu principal de funcionamiento del PAC

	//int *p;

	Reloj();
	Door();
	Gabinete();

	if(Share_Mem[2]==1){
		adv_printf("Puerta gabinete abierta\n");
	}

	adv_printf("RELOJ = %02d:%02d:%02d\n",GetRTCtime(RTC_hour), GetRTCtime(RTC_min), GetRTCtime(RTC_sec));

	switch (Share_Mem[modo]){

		case automatico:
		AutomaticoXXX();					// entra a la funcion intersecci?, definida segun MOD de cada subtramo
		adv_printf("AutomaticoXXX\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case seguro:
		Seguro();		// Set Secure mode on ADAM 5510
		adv_printf("Seguro\n");
		break;

		case manual:
		Manual();		// Set Manual mode on ADAM 5510 				/// se debe configurar el modo manual u override ***
		adv_printf("Manual\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case manualH:
		ManualH();		// Set Manual mode on ADAM 5510 				/// se debe configurar el modo manual u override ***
		adv_printf("Manual\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case emergencia:
		Emergencia();		// Set Emergency mode on ADAM 5510
		adv_printf("Emergencia\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case cambiocontingencia: //10
		adv_printf("Cambio de contingencia, restaurando valores\n");
		Cambio();
		break;

	
		case bloqueo:	// 11
		Bloqueo();
		segestado=0;					//reinicia el modo seguro
		break;

		case test:	// 12
		Test();		// Set Off mode on ADAM 5510
		adv_printf("Test\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case off:	// 13
		Off();		// Set Off mode on ADAM 5510
		adv_printf("Off\n");
		segestado=0;					//reinicia el modo seguro
		break;

		case setear:	// 14
		Setear();		// Set setear mode on ADAM 5510
		adv_printf("Setear\n");
		segestado=0;					//reinicia el modo seguro
		break;

	}	
	}

//////////////////////////////////////////////////////////////////////////////

void Seguro(void){


	//primero semaforo 1 verde, demas en rojo por 5 segundos(ahora dtV)
		//luego todos en rojo x delta tiempo segdt1 (ahora dtRt1)
	//segundo S2 verde demas en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt2		(dtRt2)
	//tercero S3 verde demas en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt3		(dtRt3)
	//cuarto S4 verde dem? en rojo x 5 seg.	(dtV)
		//luego todos en rojo x dt segdt3		(dtRt4)
	adv_printf("TIME %02d:%02d:%02d\n",GetRTCtime(RTC_hour), GetRTCtime(RTC_min), GetRTCtime(RTC_sec));

	if (read_backup_ram(10)>=5){
	Bloqueo();
	adv_printf("Ingrese un numero valido de semaforos\n"); //comprimir en 1 solo
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));
	}
	if (read_backup_ram(10)==0 || read_backup_ram(10)==1 ){
	Bloqueo();
	adv_printf("Ingrese un numero valido de semaforos\n");
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));
	}

	if(read_backup_ram(10)==4){
	adv_printf("HAY CONECTADOS 4 SEMAFOROS\n");
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));
	switch(segestado){
		case 1:							//verde sem 1
			SemMaster(4,2,2,2);
			segtverde++;
			adv_printf("SEM 1 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=2;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 2:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 1\n");
			if(segtrojo>=read_backup_ram(12)){			// x 10s
				segestado=3;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 3:							//verde sem 2
			SemMaster(2,4,2,2);
			segtverde++;
			adv_printf("SEM 2 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=4;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 4:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 2\n");
			if(segtrojo>=read_backup_ram(13)){			// x 10s
				segestado=5;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 5:							//verde sem 3
			SemMaster(2,2,4,2);
			segtverde++;
			adv_printf("SEM 3 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=6;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 6:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 3\n");
			if(segtrojo>=read_backup_ram(14)){			// x 10s
				segestado=7;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 7:							//verde sem 3
			SemMaster(2,2,2,4);
			segtverde++;
			adv_printf("SEM 4 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=8;
				segtverde=0;
				segtrojo=0;
			}
		break;		

		case 8:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 4\n");
			if(segtrojo>=read_backup_ram(15)){			// x 10s
				segestado=1;
				segtverde=0;
				segtrojo=0;
			}
		break;


		default:
			SemMaster(2,2,2,2);
			segtdefault++;
			adv_printf("SEMAFOROS EN ROJO DEFAULT\n");
			if(segtdefault>=read_backup_ram(18)){
				segestado=1;
				segtdefault=0;
			}

	}
	}

	if(read_backup_ram(10)==3){
		adv_printf("HAY CONECTADOS 3 SEMAFOROS\n");
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));
		switch(segestado){
		case 1:							//verde sem 1
			SemMaster(4,2,2,2);
			segtverde++;
			adv_printf("SEM 1 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=2;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 2:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 1\n");
			if(segtrojo>=read_backup_ram(12)){			// x 10s
				segestado=3;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 3:							//verde sem 2
			SemMaster(2,4,2,2);
			segtverde++;
			adv_printf("SEM 2 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=4;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 4:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 2\n");
			if(segtrojo>=read_backup_ram(13)){			// x 10s
				segestado=5;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 5:							//verde sem 3
			SemMaster(2,2,4,2);
			segtverde++;
			adv_printf("SEM 3 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=6;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 6:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 3\n");
			if(segtrojo>=read_backup_ram(14)){			// x 10s
				segestado=7;
				segtverde=0;
				segtrojo=0;
			}
		break;
		default:
			SemMaster(2,2,2,2);
			segtdefault++;
			adv_printf("SEMAFOROS EN ROJO DEFAULT\n");
			if(segtdefault>=read_backup_ram(18)){
				segestado=1;
				segtdefault=0;
			}

	}
	}


	if(read_backup_ram(10)==2){
	adv_printf("HAY CONECTADOS 2 SEMAFOROS\n");
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));

		switch(segestado){
		case 1:							//verde sem 1
			SemMaster(4,2,2,2);
			segtverde++;
			adv_printf("SEM 1 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=2;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 2:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 1\n");
			if(segtrojo>=read_backup_ram(12)){			// x 10s
				segestado=3;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 3:							//verde sem 2
			SemMaster(2,4,2,2);
			segtverde++;
			adv_printf("SEM 2 VERDE\n");
			if(segtverde>=read_backup_ram(11)){
				segestado=4;
				segtverde=0;
				segtrojo=0;
			}
		break;

		case 4:							//todos rojo
			SemMaster(2,2,2,2);
			segtrojo++;
			adv_printf("SEMAFOROS EN ROJO CIRCULAN AUTOS 2\n");
			if(segtrojo>=read_backup_ram(13)){			// x 10s
				segestado=5;
				segtverde=0;
				segtrojo=0;
			}
		break;

		default:
		SemMaster(2,2,2,2);
		segtdefault++;
		adv_printf("SEMAFOROS EN ROJO DEFAULT\n");
		if(segtdefault>=read_backup_ram(18)){
			segestado=1;
			segtdefault=0;
		}


		}
		}
		}

//////////////////////////////////////////////////////////////////////////////

void Off(void){
	unsigned doff = 0x0000;
	Set5050(&doff,0,0,AWord);
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void Manual(void){

	s1=Share_Mem[s1m];

	s2=Share_Mem[s2m];

	s3=Share_Mem[s3m];

	SemMaster(s1,s2,s3,s4);
	}

//////////////////////////////////////////////////////////////////////////////

void ManualH(void){

	s1=Share_Mem[s1m];

	s2=Share_Mem[s2m];

	s3=Share_Mem[s3m];

	s4=Share_Mem[s4m];

	SemMasterH(s1,s2,s3,s4);
	}

//////////////////////////////////////////////////////////////////////////////

void Emergencia(void){     //set Adam-5056&5068 and return Adam-5051 Status

    unsigned doe;
    if(count2%2==0){
        doe = 0x1492;// en binario es: ?1010010010010?   (V12,V10,V7,V4,V1)
    }
    else{
        doe = 0x0000;  
 
    }
    count2++;
    if(count2>100)
        count2 = 1;
    Set5050(&doe,0,0,AWord);       	//slot 0, pin V0-V16
    return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void Cambio(void){

	adv_printf("Volviendo a 0\n");
	est=0;
	cbloq=0;
	dpri=0;
	cesp=0;
	vernp=0;
	contador=0;
	tverde=0;
	tverde2=0;
	tverde3=0;
	cuenta=0;
	E1=0;
	E2=0;
	E3=0;
	E4=0;
	cont=0;
	conta=0;
	cp1=0;
	cp2=0;
	cp3=0;
	cu=0;
	contad=0;
	c1=0;
	c2=0;
	c3=0;
	c4=0;
	cn1=0;
	cn2=0;
	cn3=0;
	cn4=0;
	Bloqueo();
	adv_printf("TODOS LOS VALORES EN ESTADO INICIAL\n");
	}

//////////////////////////////////////////////////////////////////////////////

void Test(void){     //set Adam-5056&5068 and return Adam-5051 Status
    unsigned dtest;

    switch(count3){
    	case 0:
    	dtest = 0x249;
    	break;

    	case 1:
    	dtest = 5266;
    	break;

    	case 2:
    	dtest = 0x924;
    	break;

    	default:
    	dtest = 0x0000;
    	}
      
    	count3++;
    	if(count3>3)
        count3 = 0;
    	Set5050(&dtest,0,0,AWord);       	//slot 0, pin V0-V16
    	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void Bloqueo (void){
	unsigned Bq=0x1492;
		Set5050(&Bq,0,0,AWord);
		adv_printf("Bloqueo\n");
	}

//////////////////////////////////////////////////////////////////////////////

void Gabinete (void){

	unsigned puerta;
	Get5050(0,13,ABit,&puerta);
	Share_Mem[2]=puerta;

	//adv_printf("ES1=%d\n",ES1);
	//adv_printf("ES2=%d\n",ES2);
	//adv_printf("countgab=%02d\n",countgab);
	//adv_printf("posicion=%02d\n",read_backup_ram(49));
	//adv_printf("Año apertura gabinete%x\n",read_backup_ram(50+posicion));
	//adv_printf("Mes apertura gabinete=%x\n",read_backup_ram(51+posicion));
	//adv_printf("Dia apertura gabinete=%x\n",read_backup_ram(52+posicion));
	//adv_printf("Hora apertura gabinete=%x\n",read_backup_ram(53+posicion));
	//adv_printf("Minuto apertura gabinete=%x\n",read_backup_ram(54+posicion));
	//adv_printf("Segundo apertura gabinete=%x\n",read_backup_ram(55+posicion));

	Share_Mem[230]=read_backup_ram(50);
	Share_Mem[231]=read_backup_ram(51);
	Share_Mem[232]=read_backup_ram(52);
	Share_Mem[233]=read_backup_ram(53);
	Share_Mem[234]=read_backup_ram(54);
	Share_Mem[235]=read_backup_ram(55);

	Share_Mem[236]=read_backup_ram(60);
	Share_Mem[237]=read_backup_ram(61);
	Share_Mem[238]=read_backup_ram(62);
	Share_Mem[239]=read_backup_ram(63);
	Share_Mem[240]=read_backup_ram(64);
	Share_Mem[241]=read_backup_ram(65);

	Share_Mem[242]=read_backup_ram(70);
	Share_Mem[243]=read_backup_ram(71);
	Share_Mem[244]=read_backup_ram(72);
	Share_Mem[245]=read_backup_ram(73);
	Share_Mem[246]=read_backup_ram(74);
	Share_Mem[247]=read_backup_ram(75);

	Share_Mem[248]=read_backup_ram(80);
	Share_Mem[249]=read_backup_ram(81);
	Share_Mem[250]=read_backup_ram(82);
	Share_Mem[251]=read_backup_ram(83);
	Share_Mem[252]=read_backup_ram(84);
	Share_Mem[253]=read_backup_ram(85);

	Share_Mem[254]=read_backup_ram(90);
	Share_Mem[255]=read_backup_ram(91);
	Share_Mem[256]=read_backup_ram(92);
	Share_Mem[257]=read_backup_ram(93);
	Share_Mem[258]=read_backup_ram(94);
	Share_Mem[259]=read_backup_ram(95);


	if(ES1==1){
		if(puerta==1){
			ES2=1;
			ES1=0;
			}
		if(puerta==0){
			if(countgab>=4){
			countgab=0;
			write_backup_ram(49,read_backup_ram(49)+10);
			//posicion=posicion+10;
				if(read_backup_ram(49)>40){
					write_backup_ram(49,0);
					}
			}
		}
			if(countgab<4){
			countgab=0;
			}
	}
	
	if(ES2==1){
		if(puerta==1){
		countgab++;
		if(countgab==4){
			write_backup_ram(50+read_backup_ram(49),GetRTCtime(RTC_year));
			write_backup_ram(51+read_backup_ram(49),GetRTCtime(RTC_month));
			write_backup_ram(52+read_backup_ram(49),GetRTCtime(RTC_week));
			write_backup_ram(53+read_backup_ram(49),GetRTCtime(RTC_hour));
			write_backup_ram(54+read_backup_ram(49),GetRTCtime(RTC_min));
			write_backup_ram(55+read_backup_ram(49),GetRTCtime(RTC_sec));
		}
		}
		
		if(puerta==0){
		ES1=1;
		ES2=0;
		}
		}

	if((ES1!=0 && ES2!=0) || (ES1!=1 && ES2!=1)){
		ES1=1;
		ES2=0;
	}

	if(ES1!=0 && ES1!=1 && ES2!=0 && ES2!=1){
		ES1=1;
		ES2=0;
	}

	}

//////////////////////////////////////////////////////////////////////////////

void Setear (void){
	sector = Get_NVRAM_Size();

	/*Set Bacup ram 40Kbyte*/
	Set_NVRAM_Size(10);

	data=Share_Mem[semnumber];
	write_backup_ram(10,data);

	VERD=Share_Mem[VERDTOTAL];
	write_backup_ram(11,VERD);

	ROJ1=Share_Mem[ROJO1];
	write_backup_ram(12,ROJ1);

	ROJ2=Share_Mem[ROJO2];
	write_backup_ram(13,ROJ2);

	ROJ3=Share_Mem[ROJO3];
	write_backup_ram(14,ROJ3);

	ROJ4=Share_Mem[ROJO4];
	write_backup_ram(15,ROJ4);

	desconectado=Share_Mem[offline];
	write_backup_ram(16,desconectado);

	MDBRST=Share_Mem[RST];
	write_backup_ram(17,MDBRST);

	ROJDE=Share_Mem[ROJODE];
	write_backup_ram(18,ROJDE);
	
	if(Share_Mem[RELOJ]==1){
	SetRTCtime(RTC_year,Share_Mem[year]);
	SetRTCtime(RTC_month,Share_Mem[month]);
	SetRTCtime(RTC_week,Share_Mem[day]);
	SetRTCtime(RTC_hour,Share_Mem[hour]);
	SetRTCtime(RTC_min,Share_Mem[minute]);
	SetRTCtime(RTC_sec,Share_Mem[second]);
	}

	if(Share_Mem[228]==1){
	write_backup_ram(49,Share_Mem[229]);
	}

	YEAR=GetRTCtime(RTC_year);
	MONTH=GetRTCtime(RTC_month);
	DAY=GetRTCtime(RTC_week);
	HOUR=GetRTCtime(RTC_hour);
	MINUTE=GetRTCtime(RTC_min);
	SECOND=GetRTCtime(RTC_sec);
	
	
	adv_printf("Año=%02d\n",GetRTCtime(RTC_year));
	adv_printf("Mes=%02d\n",GetRTCtime(RTC_month));
	adv_printf("Dia=%02d\n",GetRTCtime(RTC_week));
	adv_printf("Hora=%02d\n",GetRTCtime(RTC_hour));
	adv_printf("Minuto=%02d\n",GetRTCtime(RTC_min));
	adv_printf("Segundo=%02d\n",GetRTCtime(RTC_sec));
	adv_printf("RELOJ %02d:%02d:%02d\n",GetRTCtime(RTC_hour), GetRTCtime(RTC_min), GetRTCtime(RTC_sec));
	adv_printf("Backup ram=%dKbyte\n",sector*4);
	adv_printf("Numero de semáforos=%x\n",read_backup_ram(10));
	adv_printf("TIEMPO EN VERDE=%x\n",read_backup_ram(11));
	adv_printf("SEMAFORO 1 ROJO=%x\n",read_backup_ram(12));
	adv_printf("SEMAFORO 2 ROJO=%x\n",read_backup_ram(13));
	adv_printf("SEMAFORO 3 ROJO=%x\n",read_backup_ram(14));
	adv_printf("SEMAFORO 4 ROJO=%x\n",read_backup_ram(15));
	adv_printf("SEMAFOROS EN ROJO POR DEFAULT=%x\n",read_backup_ram(18));
	adv_printf("TIEMPO ANTE UNA DESCONEXION=%x\n",read_backup_ram(16));
	adv_printf("TIEMPO RESETEAR SERVIDOR MDBS=%x\n",read_backup_ram(17));
	Bloqueo();
	}

//////////////////////////////////////////////////////////////////////////////

void AutomaticoXXX(void){

	a=Share_Mem[mod_tra_1]; // ACA YA SE INGRESA LA PRIORIDAD
	b=Share_Mem[mod_tra_2];
	c=Share_Mem[mod_tra_3];
	d=Share_Mem[mod_tra_4];

	en1in=Share_Mem[en_1_in];
	en1out=Share_Mem[en_1_out];
	tra1in=Share_Mem[tra_1_in];
	tra1out=Share_Mem[tra_1_out];
	en2in=Share_Mem[en_2_in];
	en2out=Share_Mem[en_2_out];
	tra2in=Share_Mem[tra_2_in];
	tra2out=Share_Mem[tra_2_out];
	en3in=Share_Mem[en_3_in];
	en3out=Share_Mem[en_3_out];
	tra3in=Share_Mem[tra_3_in];
	tra3out=Share_Mem[tra_3_out];
	en4in=Share_Mem[en_4_in];
	en4out=Share_Mem[en_4_out];
	tra4in=Share_Mem[tra_4_in];
	tra4out=Share_Mem[tra_4_out];

	multi=a*b*c*d;
	suma=a+b+c+d;

	if(multi==0){
	adv_printf("Ingrese el modo de cada tramo\n");
	Bloqueo();
	}

	if (multi==1){
	adv_printf("Los tramos estan inhabilitados\n");
	III();
	}

	if(multi==6){
	adv_printf("Modo Fe-Fs-I\n");
	FeFsI();
	}


	if(multi==12 && a!=b && a!=c && b!=c){
	adv_printf("El modo Be-Fs-I no esta permitido\n");
	Bloqueo();
	}

	if((a==b || a==c || b==c) && multi==12){
	FeFeFs();
	}


	if(multi==18){
	FeFsFs();
	}

	if(multi==20 && suma==10){
	adv_printf("El modo BS-Fe-Fe no esta permitido\n");
	Bloqueo();
	}

	if(multi==20 && a!=b && a!=c && b!=c){
	BeBsI();
	}

	if(multi==24){
	adv_printf("Modo Be-Fe-Fs\n");	
	AutomBeFeFs();
	}

	if(multi==30){
	AutomBsFeFs();	
	}

	if(multi==40){
	adv_printf("Modo Be-Bs-Fe\n");
	AutomBBFe();
	}

	if(multi==48){
	adv_printf("Modo Fs Be Be\n");
	AutomFsBeBe();
	}

	if(multi==50){
	adv_printf("Modo Fe Bs Bs\n");
	AutomFeBsBs();
	}

	if(multi==60){
	AutomBBFs();
	}

	if(multi==80){
	adv_printf("Modo Be Be Bs\n");
	AutomBeBeBs();
	}

	if(multi==100){
	AutomBeBsBs();
	adv_printf("Modo Be-Bs-Bs\n");
	}

	if(multi==256){
	adv_printf("Modo H\n");
	Hr();	
	}

	if (multi==2 || multi==3 || multi==4 || multi==5 || multi==8 || 
	multi==9 || multi==10 || multi==15 || multi==16 || multi==25 || 
	multi==27 || multi==32 || multi==36 || multi==45 || 
	multi==75 || multi==125){
	adv_printf("Ingrese un modo valido\n");
	Bloqueo();
	} 

	}

//////////////////////////////////////////////////////////////////////////////

//Inhabilitado-Inhabilitado-Inhabilitado (III) (1)

void III(void){
	if(multi==1){
	Bloqueo();
	adv_printf("Todos los tramos estan inhabilitados\n");	
	}
	}

//////////////////////////////////////////////////////////////////////////////

//Fijo entrando-Fijo Saliendo-Ihnabilitado(FeFsI) (6)

void FeFsI(void){
	if(multi==6){
	s4=2;
	switch(a){
	case 1:
	s1=2; //inhabilitado semaforo en rojo
	break;
	case 2:
	if(b!=1 && tra2in==0 && tra1out==0){
	s1=3;
	break;	
	}
	
	if(b!=1 && tra2in!=0 && tra1out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}


    if(c!=1 && tra3in==0 && tra1out==0){
	s1=1;
	break;
	}
	if(c!=1 && tra3in!=0 && tra1out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}

	case 3:
	s1=2; //semaforo en rojo es fijo saliendo
	break;
	}

	switch(b){
	case 1:
	s2=2; //inhabilitado semaforo en rojo
	break;
	case 2:

	if(a!=1 && tra1in==0 && tra2out==0){
	s2=1;
	break;
	}

	if(a!=1 && tra1in!=0 && tra2out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	if(c!=1 && tra3in==0 && tra2out==0){
	s2=3;
	break;
	}

	if(c!=1 && tra3in!=0 && tra2out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	case 3:
	s2=2; //semaforo en rojo es fijo saliendo
	break;
	}

	switch(c){
	case 1:
	s3=2; //inhabilitado semaforo en rojo
	break;

	case 2:
	if(a!=1 && tra1in==0 && tra3out==0){
	s3=3;
	break;
	}

	if(a!=1 && tra1in!=0 && tra3out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}

	if(b!=1 && tra2in==0 && tra3out==0){
	s3=1;
	break;
	}

	if(b!=1 && tra2in!=0 && tra3out!=0){
	Bloqueo();
	adv_printf("Vehiculos en sentido contrario, tramo bloqueado\n");
	break;
	}	

	case 3:
	s3=2; //semaforo en rojo es fijo saliendo
	break;
	}


	SemMaster(s1,s2,s3,s4);

	}
	}

//////////////////////////////////////////////////////////////////////////////

//Fijo entrando - Fijo entrando - Fijo saliendo (FeFeFs) (12)

void FeFeFs(void){
	if((a==b || a==c || b==c) && multi==12){

	switch (a){
	case 2:
	if (a==b && tra1out==0 && tra3in==0){
		s1=1;
		break;
	}
	if (a==c && tra1out==0 && tra2in==0){
		s1=3;
		break;
	}
	case 3:
		s1=2; //apagado fijo saliendo
		break;
		}

	switch (b){
	case 2:
	if(b==a && tra2out==0 && tra3in==0){
		s2=3;
		break;
	}
	if(b==c && tra2out==0 && tra1in==0){
		s2=1;
		break;
	}
	case 3:
		s2=2;
		break;

	}

	switch(c){
	case 2:
	if(c==a && tra3out==0 && tra2in==0){
		s3=1;
		break;
	}
	if(c==b && tra3out==0 && tra1in==0){
		s3=3;
		break;
	}	
	case 3:
		s3=2;
		break;
	}

	SemMaster(s1,s2,s3,s4);
	}
	}

//////////////////////////////////////////////////////////////////////////////

//Fijo entrando - Fijo saliendo - Fijo saliendo (FeFsFs) (18)

void FeFsFs(void){
	if(multi==18){
	s4=2;
	switch (a){
		case 2:
		if (tra1out==0 && tra2in==0 && tra3in==0){
		s1=4;
		break;}

		if(tra1out!=0){
			Bloqueo();
		}

		case 3:
		s1=2;
		break;
	}

	switch (b){
		case 2:
		if (tra2out==0 && tra1in==0 && tra3in==0){
		s2=4;
		break;}

		if(tra2out!=0){
			Bloqueo();
		}
		
		case 3:
		s2=2;
		break;
	}

	switch (c){
		case 2:
		if (tra3out==0 && tra1in==0 && tra2in==0){
		s3=4;
		break;
		}

		if(tra3out!=0){
			Bloqueo();
		}
		case 3:
		s3=2;
		break;
		}
	SemMaster(s1,s2,s3,s4);
	}
	}

//////////////////////////////////////////////////////////////////////////////

//Bidireccional entrando - Bidireccional saliendo - Inhabilitado (BeBsI) (20)

void BeBsI(void){
	if(multi==20 && a!=b && a!=c && b!=c){
	AutomBBI();
	}
	}

//////////////////////////////////////////////////////////////////////////////
// sirve para el multi=20 a=!b,a=!c
void AutomBBI(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2; // registro ensanche 1 y registro ensache 2, de este modo se le llamara al tramo comprometido.
	// siempre r_e1 es el tramo bidireccional que tiene prioridad entrando y r_e2 es el 2do bidireccional con prioridad saliendo.

	//En esta parte del codigo, se detecta que tramo es bidireccional y en que sentido.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		


	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}		
			
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}		



		
	BiBase(r_e1,r_e2);  //prioridad de ensanche 1 al ensanche 2
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void BiBase(r_e1,r_e2){
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e1+30]==0){
				est=1;
				adv_printf("ESTADO ES 1\n");
			}
			}
		}

	if (est==1){
		if(Share_Mem[r_e2]!=0){
			adv_printf("CAMION EN ENSANCHE 2 ESPERANDO\n");
		dpri++;
		}
		
		//if(Share_Mem[r_e2+10]>=2){
		//	adv_printf("ENSANCHE DE SALIDA CON 2 AUTOS\n");
		//	Bloqueo();
		//}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e1+20]==0){
			cont++;
		}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && cont>Share_Mem[vacio]){
			adv_printf("No hay camiones en el ensanche 1, dar paso a camiones del ensanche 2\n");
			est=2;
			cont=0;
		}

		if (dpri>Share_Mem[waitnp] && Share_Mem[r_e2]!=0){
			dpri=0;
			adv_printf("AUTOS DEL ENS2 YA ESPERARON CAMBIA A ESTADO 2\n");
			if(Share_Mem[r_e1+10]==0){
			est=2;
			}
	
		}
		if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0){
			adv_printf("VERDE DE ENSANCHE 1 A ENSANCHE 2\n");

			seminter(r_e1,r_e2);
			}	
		if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0){
			adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
			Bloqueo();
		}
	}

	if(est==2){
		adv_printf("ESPERANDO TRAMO VACIO PARA DAR VERDE A LOS NP\n");
		Bloqueo();
		cesp++;
		if(cesp>Share_Mem[waitzero]){
			adv_printf("TIEMPO CUMPLIDO, REVISAR TRAMO\n");
			cesp=0;
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0){
				adv_printf("TRAMO LIBRE PASAR A ESTADO 3\n");
				est=3;
			}
		}
	}	

	if(est==3){
		vernp++;
		if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0){
		adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
		Bloqueo();
		}
	//	if(Share_Mem[r_e2]==0){
	//		adv_printf("NO HAY AUTOS EN EL ENSANCHE 2, SE VUELVE A ESTADO 0\n");
	//		est=0;
	//	}
		if(Share_Mem[r_e1+10]!=0){
			adv_printf("NO HAY ESPACIO EN EL ENSANCHE 1 VUELVE A 0\n");
			est=0;
		}

		if(vernp>Share_Mem[tnp]){
			adv_printf("TIEMPO EN VERDE LISTO, VUELVE A 0\n");
			vernp=0;
			est=0;
		}
		if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e1+10]==0){
		seminter(r_e2,r_e1);
		adv_printf("ENCENDER NP\n");
	}
	}
	}

//////////////////////////////////////

void seminter(int reg1, int reg2){  /////  *** falta actualizar para 4 semaforos

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (reg2){		//como la direcci? es de reg1 a reg2, el semaforo asociado a reg2 tiene que estar en rojo.
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}

	switch (reg1){		//como existen 2 verdes, se debe indical cual de los 2 debe encender(para el caso B-B)
		case en_1_in:
			if(reg2==en_2_in)  //si la salida es por el semaforo de la derecha (en_2_in>en_1_in), entonces el S1 es verde a la derecha ( 3 = rojo derecha)
				S1=3;
			else
				S1=1;
			break;

		case en_2_in:
			if(reg2==en_3_in)
				S2=3;
			else
				S2=1;
			break;

		case en_3_in:
			if(reg2==en_1_in) // aca en_1_in es el que esta a la derecha del en_3_in, porque da la vuelta. para la versi? de 4 semaforos, hay que resolver ***
				S3=3;
			else
				S3=1;
			break;
	}

	SemMaster(S1,S2,S3,S4);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void SemMaster (int s1, int s2, int s3, int s4){

	unsigned SB, sem1, sem2, sem3, sem4;

	if(s1==3 && s2==1||s2==3 && s3==1 || s3==3 && s4==1 || s4==3 && s1==1 ||  //flechas opuestas

			s1==1 && s2==1 || s1==3 && s2==3 ||      //felchas semi opuestas // esto se puede optimizar cuando se sepa la posici? del cami? cuando dobla ***
			s2==1 && s3==1 || s2==3 && s3==3 ||
			s3==1 && s4==1 || s3==3 && s4==3 ||
			s4==1 && s1==1 || s4==3 && s1==3// ||

		//	(s1==1 || s1==3) && (s3==1 || s3==3) ||		//felchas cruzadas (solo para 4 piernas) // revisar, creo que no es as?***. 
		//	(s2==1 || s2==3) && (s4==1 || s4==3)		// una opci? de soluci? es contar con 3 flechas para indicar el paso de cada tramo.
		)

	{		//esta serie de if, aseguran que no existan errores en la declaraci? de los sem?oros.
		adv_printf("Error-flechas verdes inconsecuentes\n");		
		Bloqueo();
		return 0;
	}

	else if (s1!=2 && s2!=2 && s3!=2 && s4!=2) {
		adv_printf("Error-todos con verde hacia nodo\n");
		Bloqueo();
		return 0;
	}
	else if((s1==4 && (s2!=2 || s3!=2 || s4!=2)) || (s2==4 && (s3!=2 || s1!=2 || s4!=2)) ||  // esto se puede optimizar cuando se sepa la posici? del cami? cuando dobla **
		    (s3==4 && (s1!=2 || s2!=2 || s4!=2)) || (s4==4 && (s1!=2 || s2!=2 || s3!=2))) {

		adv_printf("Error-semaforo con doble flecha y demas con verde\n");
		Bloqueo();
		return 0;
	}

	switch (s1){
		case 1:			//semaforo 1 verde izquierda
			sem1=1;			
			break;
		case 2:			//semaforo 1 verde rojo
			sem1=2;			
			break;
		case 3:			//semaforo 1 verde derecha
			sem1=4;			
			break;
		case 4:			//semaforo 1 verde izquierda y verde derecha
			sem1=5;			
			break;
		default:
			sem1=2;
			break;
	}

	switch (s2){
		case 1:
			sem2=8;
			break;
		case 2:
			sem2=16;
			break;
		case 3:
			sem2=32;
			break;
		case 4:
			sem2=40;
			break;
		default:
			sem2=16;
			break;
	}

	switch (s3){
		case 1:
			sem3=64;
			break;
		case 2:
			sem3=128;
			break;
		case 3:
			sem3=256;
			break;
		case 4:
			sem3=320;
			break;
		default:
			sem3=128;
			break;
	}

	switch (s4){
		case 1:
			sem4=512;
			break;
		case 2:
			sem4=1024;
			break;
		case 3:
			sem4=2048;
			break;
		case 4:
			sem4=2560;
			break;
		default:
			sem4=1024;
			break;
	}

	Semaforo(sem1,sem2,sem3,sem4);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void SemMasterH (int s1, int s2, int s3, int s4){

	unsigned SB, sem1, sem2, sem3, sem4;

	switch (s1){
		case 1:			//semaforo 1 verde izquierda
			sem1=1;			
			break;
		case 2:			//semaforo 1 verde rojo
			sem1=2;			
			break;
		case 3:			//semaforo 1 verde derecha
			sem1=4;			
			break;
		case 4:			//semaforo 1 verde izquierda y verde derecha
			sem1=5;			
			break;
		default:
			sem1=2;
			break;
	}

	switch (s2){
		case 1:
			sem2=8;
			break;
		case 2:
			sem2=16;
			break;
		case 3:
			sem2=32;
			break;
		case 4:
			sem2=40;
			break;
		default:
			sem2=16;
			break;
	}

	switch (s3){
		case 1:
			sem3=64;
			break;
		case 2:
			sem3=128;
			break;
		case 3:
			sem3=256;
			break;
		case 4:
			sem3=320;
			break;
		default:
			sem3=128;
			break;
	}

	switch (s4){
		case 1:
			sem4=512;
			break;
		case 2:
			sem4=1024;
			break;
		case 3:
			sem4=2048;
			break;
		case 4:
			sem4=2560;
			break;
		default:
			sem4=1024;
			break;
	}

	Semaforo(sem1,sem2,sem3,sem4);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void Semaforo(int sem1,int sem2,int sem3, int sem4){

	unsigned SB;

	SB=sem1+sem2+sem3+sem4;
	adv_printf("SB1 %u \n", SB);
	Set5050(&SB,0,0,AWord);
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void AutomBBFs(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2, r_e3; // registro ensanche 1 y registro ensache 2, de este modo se le llamara al tramo comprometido.
	// siempre r_e1 es el tramo bidireccional que tiene prioridad entrando y r_e2 es el 2do bidireccional con prioridad saliendo.

	//En esta parte del codigo, se detecta que tramo es bidireccional y en que sentido.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		
	if(Share_Mem[mod_tra_1]==3){ 	
		r_e3= en_1_in;}	

	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}		
	if(Share_Mem[mod_tra_2]==3){ 	
		r_e3= en_2_in;}

	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}		
	if(Share_Mem[mod_tra_3]==3){ 	
		r_e3= en_3_in;}	

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
		
	BiBaseFs(r_e1,r_e2,r_e3);  //prioridad de ensanche 1 al ensanche 2
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void BiBaseFs(r_e1,r_e2,r_e3){
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 ){
				est=1;
				adv_printf("ESTADO ES 1\n");
			}
			}
		}

	if (est==1){
		
		if(Share_Mem[r_e2]!=0){
		adv_printf("CAMION EN ENSANCHE 2 ESPERANDO\n");
		dpri++;
		}
		if (dpri>Share_Mem[waitnp] && Share_Mem[r_e2]!=0){
		adv_printf("AUTOS DEL ENS2 YA ESPERARON CAMBIA A ESTADO 2\n");
		est=2;
		dpri=0;
		}



		if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
		adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
		Bloqueo();
		}	

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+30]==0){
			cont++;
		}

		if(Share_Mem[r_e2]!=0 && Share_Mem[r_e1+10]==0 && cont>Share_Mem[vacio]){
			adv_printf("No hay camiones en el ensanche 1, dar paso a camiones del ensanche 2\n");
			est=2;
			cont=0;
		}


		if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
			adv_printf("VERDE DE ENSANCHE 1 A ENSANCHE 2\n");
			sesta6(r_e1,r_e2, r_e3);
			}	


	}

	if(est==2){
		adv_printf("ESPERANDO TRAMO VACIO PARA DAR VERDE A LOS NP\n");
		Bloqueo();
		cesp++;
		if(cesp>Share_Mem[waitzero]){
			adv_printf("TIEMPO CUMPLIDO, REVISAR TRAMO\n");
			cesp=0;
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 &&Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
				adv_printf("TRAMO LIBRE PASAR A ESTADO 3\n");
				est=3;
			}
		}
	}	

	if(est==3){
		vernp++;

		if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
			adv_printf("Vehiculos en contra del tramo, se procede a bloquear\n");
			Bloqueo();
		}

		//if(Share_Mem[r_e2]==0){
		//	adv_printf("NO HAY AUTOS EN EL ENSANCHE 2, SE VUELVE A ESTADO 0\n");
		//	est=0;
		//}
		//if(Share_Mem[r_e1+10]!=0){
		//	adv_printf("NO HAY ESPACIO EN EL ENSANCHE 1 VUELVE A 0\n");
		//	est=0;
		//}

		if(vernp>Share_Mem[tnp]){
			adv_printf("TIEMPO EN VERDE LISTO, VUELVE A 0\n");
			vernp=0;
			est=0;
		}
		if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
		sesta7(r_e1,r_e2,r_e3);
		adv_printf("ENCENDER NP\n");
	}
	}
	}

//////////////////////////////////////////////////////////////////////////////

void sesta6(int r_e1, int r_e2, int r_e3){  

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){		

	case en_1_in:
		if(Share_Mem[r_e2+10]<=10){
		S1=4;
		break;
	}
		if(r_e2==en_2_in && Share_Mem[r_e2+10]>10){
		S1=1;
		break;
	}
		if(r_e2==en_3_in && Share_Mem[r_e2+10]>10){
		S1=3;
		break;
	}

		case en_2_in:
		if(Share_Mem[r_e2+10]<=10 ){
		S2=4;
		break;
	}
		if(r_e2==en_1_in && Share_Mem[r_e2+10]>10){
		S2=3;
		break;
	}
		if(r_e2==en_3_in && Share_Mem[r_e2+10]>10){
		S2=1;
		break;
	}

		case en_3_in:
		if(Share_Mem[r_e2+10]<=10 ){
		S3=4;
		break;
	}
		if(r_e2==en_1_in && Share_Mem[r_e2+10]>10){
		S3=1;
		break;
	}
		if(r_e2==en_2_in && Share_Mem[r_e2+10]>10){
		S3=3;
		break;
	}
	}

	switch (r_e2){		//como la direcci? es de reg1 a reg2, el semaforo asociado a reg2 tiene que estar en rojo.
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}



	SemMaster(S1,S2,S3,S4);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void sesta7(int r_e1, int r_e2, int r_e3){  /////  *** falta actualizar para 4 semaforos

	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){		
		case en_1_in:
			S1=2;
			break;
		case en_2_in:
			S2=2;
			break;
		case en_3_in:
			S3=2;
			break;
	}

	switch (r_e2){		
	case en_1_in:
		if(Share_Mem[r_e1+10]==0){
		S1=4;
		break;
	}
		if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0){
		S1=1;
		break;
	}
		if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0){
		S1=3;
		break;
	}

		case en_2_in:
		if(Share_Mem[r_e1+10]==0){
		S2=4;
		break;
	}
		if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0){
		S2=3;
		break;
	}
		if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0){
		S2=1;
		break;
	}
		case en_3_in:
		if(Share_Mem[r_e1+10]==0){
		S3=4;
		break;
	}
		if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0){
		S3=1;
		break;
	}
		if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0){
		S3=3;
		break;
	}
	}
	SemMaster(S1,S2,S3,S4);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void AutomBBFe(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2 , r_e3; // r_e1 es el bidireccional entrando, r_e2 es el bidireccional saliendo y r_e3 es el fijo entrando.

	//En esta parte del codigo, se detecta que tramo es bidireccional y fijo.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;}			
	if(Share_Mem[mod_tra_1]==5){ 	
		r_e2= en_1_in;}		
	if(Share_Mem[mod_tra_1]==2){ 	
		r_e3= en_1_in;}	


	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;}			
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e2= en_2_in;}	
	if(Share_Mem[mod_tra_2]==2){ 	
		r_e3= en_2_in;}		
			
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;}			
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e2= en_3_in;}	
	if(Share_Mem[mod_tra_3]==2){ 	
		r_e3= en_3_in;}		



		
	BiBaseFe(r_e1,r_e2,r_e3);  //prioridad de ensanche 1 al ensanche 2
	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void BiBaseFe(r_e1,r_e2,r_e3){

	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta1( r_e1, r_e2, r_e3);

	if(Share_Mem[r_e2]!=0 || Share_Mem[r_e3]!=0){
	contador++;
	adv_printf("Camion esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[waitnp]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	if(Share_Mem[r_e1+10]==0){
	est=2;
	}
	contador=0;

	}

	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && (Share_Mem[r_e2]!=0 || Share_Mem[r_e3]!=0)){
		cu++;
	}

	if(cu>Share_Mem[vacio] && Share_Mem[r_e1]==0 && Share_Mem[r_e1+10]==0){
		cu=0;
		est=2;

	}


	if(Share_Mem[r_e2+10]>=10){
	adv_printf(" Ensanche de salida lleno, pasar a estado 0\n");
	Bloqueo();
	}

	if(Share_Mem[r_e2+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}
	}

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(Share_Mem[r_e1+10]!=0){
		est=0;
		adv_printf("Ensanche sin espacio\n");
	}
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e1+10]==0 ){
		est=3;
		adv_printf("Tramo libre, verde contra la prioridad \n");
	}
	}
	}


	if(est==3){
	adv_printf(" Verde contra la prioridad\n");
	sesta2(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	}

//////////////////////////////////////////////////////////////////////////////

void sesta1(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){
	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S1=1;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
		S1=2;
		break;
	}


	case en_2_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S2=3;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
	S2=2;
	break;
	}

	case en_3_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e1+30]==0){
		S3=1;
		break;
	}

	if(Share_Mem[r_e1+30]!=0){
	S3=2;
	break;
	}
	}

	switch (r_e2){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch(r_e3){
	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S1=3;
	break;
    }
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S1=1;
	break;
	}
	if(Share_Mem[r_e3+30]!=0){
	S1=2;
	break;	
	}

	case en_2_in:
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S2=1;
	break;
    }
	if(r_e2==en_3_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S2=3;
	break;
	}
	if(Share_Mem[r_e3+30]!=0){
	S2=2;
	break;	
	}

	case en_3_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S3=1;
	break;
    }
	if(r_e2==en_1_in && Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+30]==0){
	S3=3;
	break;
	}

	if(Share_Mem[r_e3+30]!=0){
	S3=2;
	break;	
	}
	}
	SemMaster(S1,S2,S3,S4);

	}	

//////////////////////////////////////////////////////////////////////////////

void sesta2(r_e1,r_e2,r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;
	switch (r_e1){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){
	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S1=1;
		break;
	}
	if(Share_Mem[r_e2+30]!=0){
		S1=2;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S2=3;
		break;
	}

	if(Share_Mem[r_e2+30]!=0){
		S2=2;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S3=3;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+30]==0){
		S3=1;
		break;
	}
	if(Share_Mem[r_e2+30]!=0){
		S3=2;
	}
	}

	switch (r_e3){
	case en_1_in:
	//if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S1=2;
		break;
	//}
	//if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S1=1;
		break;
	//}
	
	//if(Share_Mem[r_e3+30]!=0){
	//	S1=2;
	//}

	case en_2_in:
	//if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S2=2;
		break;
	//}
	//if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S2=3;
	//	break;
	//}
	//if(Share_Mem[r_e3+30]!=0){
	//	S2=2;
	//}

	case en_3_in:
	//if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
		S3=2;
		break;
	//}
	//if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+30]==0){
	//	S3=1;
	//	break;
	//}
	//if(Share_Mem[r_e3+30]!=0){
	//	S3=2;
	//}
	}
	SemMaster(S1,S2,S3,S4);
	}

//////////////////////////////////////////////////////////////////////////////

//Modo BeBsBs

void AutomBeBsBs(void){

	//101->en_1_in          102-> en_2_in           103-> en_3_in // esto para la versi? de Septimbre del 2016.

	int r_e1, r_e2 , r_e3; // r_e1 es el bidireccional entrando, r_e2 es el bidireccional saliendo y r_e3 es el fijo entrando.

	//En esta parte del codigo, se detecta que tramo es bidireccional y fijo.
	if(Share_Mem[mod_tra_1]==4){ 	
		r_e1= en_1_in;			
		r_e2= en_2_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_2]==4){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==4){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}



	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeBB(r_e1,r_e2,r_e3);

	return 0;
	}

//////////////////////////////////////////////////////////////////////////////

void AutomBeBB(r_e1,r_e2,r_e3){

	//----------------------------ESTADO 0-------------------------//
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//----------------------------ESTADO 1-------------------------//
	if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta3( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0 && Share_Mem[r_e2+30]==0){
	cp1++;
	adv_printf("ENSANCHE Y TRAMO CON PRIORIDAD EN 0\n");
	}

	if(cp1>Share_Mem[vacio] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e3+10]<=10)){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3]!=0 && Share_Mem[r_e3+30]==0){
	cp2++;
	adv_printf("ENSANCHE Y TRAMO CON PRIORIDAD EN 0\n");
	}

	if(cp2>(Share_Mem[vacio]+2) && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]<=10)){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo\n");
	    est=2;
	    E2=1;
	    E1=0;
	}


	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contador++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[waitnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e3+10]<=10)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	contador=0;
	conta=2;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]<=10)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contador=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0){
		est=3;
		adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e1 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0){
		est=4;
		adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e1 \n");
	}

	}
	}

	//------------------------ESTADO 3----------------------------------//

	if(est==3){
	adv_printf(" Verde a r_e2\n");
	sesta4(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}


	if(est==4){
	adv_printf(" Verde a r_e3\n");
	sesta5(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	}

//////////////////////////////////////////////////////////////////////////////

void sesta3(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){
	case en_1_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}


	case en_2_in:
	
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}

	case en_3_in:
	
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}	
	}

	switch (r_e2){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e3){
	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	SemMaster(S1,S2,S3,S4);



	}

//////////////////////////////////////////////////////////////////////////////

void sesta4(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}


	switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10 ){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10 ){
	S1=1;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10 ){
	S1=3;
	break;
	}

	/////////////////

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10 ){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10 ){
	S1=3;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}



	case en_2_in:
	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}	

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}	

	case en_3_in:
	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}


	}
	switch (r_e3){

	case en_1_in:
	S1=2;

	case en_2_in:
	S2=2;

	case en_3_in:
	S3=2;

	}



	SemMaster(S1,S2,S3,S4);

	}

//////////////////////////////////////////////////////////////////////////////

void sesta5(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

	}

	switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S1=3;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S2=3;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S2=1;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S2=1;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S2=3;
	break;
	}


	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S3=1;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]<=10){
	S3=3;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]>10){
	S3=1;
	break;
	}

	}
	SemMaster(S1,S2,S3,S4);
	}

//////////////////////////////////////////////////////////////////////////////

void Hr(void){

	int r_e1, r_e2 , r_e3, r_e4;

		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;
		r_e4= en_4_in;			

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
	adv_printf("el valor r_e4 %i \n", r_e4);	
		
	H(r_e1,r_e2,r_e3,r_e4);

	}

//////////////////////////////////////////////////////////////////////////////

void H(r_e1,r_e2,r_e3,r_e4){
	unsigned S1, S2, S3, S4;
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[BLOQH]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e4+20]==0 && Share_Mem[r_e4+30]==0){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	if (est==1){

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0 && Share_Mem[mod_tra_1]!=1){
	c1++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(c1>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=1;
	E2=0;
	E3=0;
	E4=0;
	c1=0;
	c2=3;
	c3=2;
	c4=1;
	}

	if(Share_Mem[r_e2]!=0 && Share_Mem[mod_tra_2]!=1 ){
	c2++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(c2>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=1;
	E3=0;
	E4=0;
	c1=1;
	c2=0;
	c3=3;
	c4=2;
	}

	if(Share_Mem[r_e3]!=0 && Share_Mem[mod_tra_3]!=1){
	c3++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(c3>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=0;
	E3=1;
	E4=0;
	c1=2;
	c2=1;
	c3=0;
	c4=3;
	}


	if(Share_Mem[r_e4]!=0 && Share_Mem[mod_tra_4]!=1){
	c4++;
	adv_printf("Camion de r_e4 esperando contra la prioridad \n");
	}

	if(c4>Share_Mem[C1H]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	est=2;
	E1=0;
	E2=0;
	E3=0;
	E4=1;
	c1=3;
	c2=2;
	c3=1;
	c4=0;
	}

	if(Share_Mem[r_e1+20]!=0 && Share_Mem[r_e1+30]!=0 && Share_Mem[r_e2+20]!=0 && Share_Mem[r_e2+30]!=0 && Share_Mem[r_e3+20]!=0 && Share_Mem[r_e3+30]!=0 && Share_Mem[r_e4+20]!=0 && Share_Mem[r_e4+30]!=0){
	est=0;
	adv_printf(" Camiones en el tramo\n");			
	}
	}

	if (est==2){
	S1=2;
	S2=2;
	S3=2;
	S4=2;
	if(E1==1){
	cn1++;
	if(cn1>Share_Mem[REVH] &&(Share_Mem[r_e2]!=0 ||Share_Mem[r_e3]!=0 ||Share_Mem[r_e4]!=0)){
	cn1=0;
	est=0;}	
	if(Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+20]==0){
	S1=4;
	}
	}

	if(E2==1){	
	cn2++;
	if(cn2>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e3]!=0 ||Share_Mem[r_e4]!=0)){
	cn2=0;
	est=0;}	
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+20]==0){
	S2=4;
	}
	}
	if(E3==1){
	cn3++;	
	if(cn3>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e2]!=0 ||Share_Mem[r_e4]!=0)){
	cn3=0;
	est=0;}	
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e4+20]==0){
	S3=4;
	}
	}

	if(E4==1){	
	cn4++;
	if(cn4>Share_Mem[REVH] && (Share_Mem[r_e1]!=0 ||Share_Mem[r_e2]!=0 ||Share_Mem[r_e3]!=0)){
	cn4=0;
	est=0;}		
	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e4+30]==0){
	S4=4;
	}
	}
	SemMaster(S1,S2,S3,S4);
	}
	}

//////////////////////////////////////////////////////////////////////////////

void AutomBeBeBs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==5){ 	
		r_e1= en_2_in;
		r_e2= en_3_in;		
		r_e3= en_1_in;			

	}
	if(Share_Mem[mod_tra_2]==5){ 	
		r_e1= en_1_in;		
		r_e2= en_3_in;
		r_e3= en_2_in;
	}
	if(Share_Mem[mod_tra_3]==5){ 	
		r_e1= en_1_in;		
		r_e2= en_2_in;
		r_e3= en_3_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeBeB(r_e1,r_e2,r_e3);

	}

//////////////////////////////////////////////////////////////////////////////

void AutomBeBeB(r_e1,r_e2,r_e3){

	//----------------------------ESTADO 0-------------------------//
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//----------------------------ESTADO 1-------------------------//
	if (est==1){
	adv_printf(" verde a la Prioridad\n");
	sesta8( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e2]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e1]!=0){
	cp1++;
	adv_printf("ENSANCHE 1 ESPERANDO POR R_E2 Y ENSANCHE 2 Y TRAMO EN 0\n");
	}

	if(cp1>(Share_Mem[vacio]+2) && Share_Mem[r_e2+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	    E2=0;
	    E3=0;
	}
	
	////////////////
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0){
	cp2++;
	adv_printf("ENSANCHE 2 ESPERANDO POR R_E1 Y ENSANCHE 1 Y TRAMO EN 0\n");
	}

	if(cp2>Share_Mem[vacio] && Share_Mem[r_e1+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	    E3=0;
	}

	////////////////

	if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3]!=0 && (Share_Mem[r_e1]==0 || Share_Mem[r_e2]==0)){
	cp3++;
	adv_printf("ENSANCHE 3 ESPERANDO POR R_E1 Y ENSANCHE 1 Y TRAMO EN 0\n");
	}

	if(cp3>(Share_Mem[vacio]+4) && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]==0)){
		cp3=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e3\n");
	    est=2;
	    E2=0;
	    E1=0;
	    E3=1;
	}


	//Si en el ensanche de r_e1 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0){
	contador++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(contador>Share_Mem[tnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	E3=0;
	est=2;
	contador=0;
	conta=2;
	contad=4;
	}


	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[tnp] && Share_Mem[r_e1+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	E3=0;
	est=2;
	contador=2;
	conta=4;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[tnp] && (Share_Mem[r_e1+10]==0 || Share_Mem[r_e2+10]==1)){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=0;
	E3=1;
	est=2;
	conta=0;
	contador=4;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e1 a r_e2 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e1 \n");
	}
	if(E3==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=5;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a (r_e1 y r_e2) \n");
	}

	}
	}

	//----------------------------ESTADO 3-------------------------//

	if(est==3){
	adv_printf(" Verde hacia r_e2\n");
	sesta9(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	//----------------------------ESTADO 4-------------------------//

	if(est==4){
	adv_printf(" Verde hacia r_e1\n");
	sesta10(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}	

	//----------------------------ESTADO 5-------------------------//

	if(est==5){
	adv_printf(" Verde hacia r_e1 y r_e2\n");
	sesta11(r_e1, r_e2, r_e3);
	tverde3++;
	if(tverde3>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde3=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
	}

//////////////////////////////////////////////////////////////////////////////

void sesta8(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:

	if(r_e2==en_2_in && Share_Mem[r_e3+10]<=10){
	
		S1=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]<=10){

		S1=3;
		break;
	}

	case en_2_in:

	if(r_e2==en_1_in && Share_Mem[r_e3+10]<=10){

		S2=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]<=10){

		S2=1;
		break;
	}	

	case en_3_in:
	
	if(r_e2==en_1_in && Share_Mem[r_e3+10]<=10){
		S3=1;

		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]<=10){

		S3=3;
		break;
	}
	}

	switch (r_e2){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){

		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){

		S1=3;
		break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
	
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){

		S2=1;
		break;
	}

	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){

		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){

		S3=3;
		break;
	}
	}

	switch (r_e3){

	case en_1_in:

	S1=2;
	break;


	case en_2_in:

	S2=2;
	break;

	case en_3_in:

	S3=2;
	break;
	}
	SemMaster(S1,S2,S3,S4);
	}
	
//////////////////////////////////////////////////////////////////////////////

void sesta9(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=1;
	break;
	}	
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=3;
	break;
	}


	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=3;
	break;
	}	
	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}


	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e2==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}	

	case en_3_in:

	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}	
	if(r_e2==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}


	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}	
	if(r_e2==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}	
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

	}

	switch (r_e3){
		case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;

	}
	SemMaster(S1,S2,S3,S4);
	}

//////////////////////////////////////////////////////////////////////////////

void sesta10(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=3;
	break;
	}


	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=1;
	break;
	}


	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S2=3;
	break;
	}


	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=1;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=3;
	break;
	}


	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]<=10){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0 && Share_Mem[r_e3+10]<=10){
	S3=3;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e2+10]==0 && Share_Mem[r_e3+10]>10){
	S3=1;
	break;
	}
	}

	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}
	SemMaster(S1,S2,S3,S4);
	}

//////////////////////////////////////////////////////////////////////////////

void sesta11(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;

	case en_3_in:
	S3=2;
	break;
	}


	switch (r_e3){

	case en_1_in:

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S1=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S1=1;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S1=3;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S1=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S1=3;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S1=1;
	break;
	}

	case en_2_in:

	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S2=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S2=3;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S2=1;
	break;
	}

	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S2=4;
	break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S2=1;
	break;
	}	
	if(r_e1==en_3_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S2=3;
	break;
	}


	case en_3_in:

	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S3=4;
	break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S3=1;
	break;
	}	
	if(r_e1==en_1_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S3=3;
	break;
	}

	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]==0){
	S3=4;
	break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e1+10]!=0 && Share_Mem[r_e2+10]==0){
	S3=3;
	break;
	}	
	if(r_e1==en_2_in && Share_Mem[r_e1+10]==0 && Share_Mem[r_e2+10]!=0){
	S3=1;
	break;
	}
	}
	SemMaster(S1,S2,S3,S4);	
	}

//////////////////////////////////////////////////////////////////////////////

void AutomFeBsBs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 	
		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;			

	}
	if(Share_Mem[mod_tra_2]==2){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==2){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomFeBB(r_e1,r_e2,r_e3);

	}

//////////////////////////////////////////////////////////////////////////////

void AutomFeBB(r_e1,r_e2,r_e3){


	//---------------------------------------------E0--------------------
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//---------------------------------------------E1--------------------

	if (est==1){

	adv_printf(" verde a la Prioridad\n");
	sesta12( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2]!=0 && Share_Mem[r_e2+30]==0){
	cp1++;
	adv_printf("ENSANCHE 2 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	////////////////
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3]!=0 && Share_Mem[r_e3+30]==0){
	cp2++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp2>(Share_Mem[vacio]+2) && Share_Mem[r_e2+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	}

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	conta=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e3 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}

	}
	}

	//----------------------------ESTADO 3-------------------------//

	if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta13(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	//----------------------------ESTADO 4-------------------------//

	if(est==4){
	adv_printf(" Verde hacia r_e2\n");
	sesta14(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
	}	

//////////////////////////////////////////////////////////////////////////////

void sesta12(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(Share_Mem[r_e2+10]<=10 && Share_Mem[r_e3+10]>10){
		S3=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10 && Share_Mem[r_e3+10]<=10){
		S3=3;
		break;
	}
	}


	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}

	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}
	SemMaster(S1,S2,S3,S4);	
	}
	
//////////////////////////////////////////////////////////////////////////////

void sesta13(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e3+10]<=10){
		S3=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
	}


	switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){
		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e3+10]<=10){
		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e3+10]>10){
		S3=2;
		break;
	}

	}

	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}
	SemMaster(S1,S2,S3,S4);	
	}

//////////////////////////////////////////////////////////////////////////////

void sesta14(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e2+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S3=1;
		break;
	}
	}



	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}


	switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in && Share_Mem[r_e2+10]<=10){
		S1=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]<=10){
		S1=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S1=2;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in && Share_Mem[r_e2+10]<=10){
		S2=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]<=10){
		S2=1;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S2=2;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in && Share_Mem[r_e2+10]<=10){
		S3=1;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]<=10){
		S3=3;
		break;
	}
	if(Share_Mem[r_e2+10]>10){
		S3=2;
		break;
	}

	}
	SemMaster(S1,S2,S3,S4);	

	}

//////////////////////////////////////////////////////////////////////////////

void AutomFsBeBe(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==3){ 	
		r_e1= en_1_in;
		r_e2= en_2_in;		
		r_e3= en_3_in;			

	}
	if(Share_Mem[mod_tra_2]==3){ 	
		r_e1= en_2_in;		
		r_e2= en_1_in;
		r_e3= en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 	
		r_e1= en_3_in;		
		r_e2= en_1_in;
		r_e3= en_2_in;
	}

	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomFsBB(r_e1,r_e2,r_e3);

	}

//////////////////////////////////////////////////////////////////////////////

void AutomFsBB(r_e1,r_e2,r_e3){


	//---------------------------------------------E0--------------------
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//---------------------------------------------E1--------------------

	if (est==1){

	adv_printf(" verde a la Prioridad\n");
	sesta15( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e3]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e2]!=0){
	cp1++;
	adv_printf("ENSANCHE 2 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E1=1;
	    E2=0;
	}
	
	////////////////
	if(Share_Mem[r_e2]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3]!=0){
	cp2++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp2>Share_Mem[vacio] && Share_Mem[r_e2+10]==0){
		cp2=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e2\n");
	    est=2;
	    E2=1;
	    E1=0;
	}

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e2]!=0){
	contad++;
	adv_printf("Camion de r_e2 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	E2=0;
	est=2;
	conta=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	if(Share_Mem[r_e3]!=0){
	conta++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(conta>Share_Mem[waitnp] && Share_Mem[r_e2+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=0;
	E2=1;
	est=2;
	conta=0;
	contad=2;
	}

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e2 a r_e3 \n");
	}
	if(E2==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=4;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}

	}
	}

	//----------------------------ESTADO 3-------------------------//

	if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta16(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+30]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}

	//----------------------------ESTADO 4-------------------------//

	if(est==4){
	adv_printf(" Verde hacia r_e2\n");
	sesta17(r_e1, r_e2, r_e3);
	tverde2++;
	if(tverde2>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde2=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
	}	

//////////////////////////////////////////////////////////////////////////////

void sesta15(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}


	switch (r_e2){

	case en_1_in:
	if(r_e1==en_2_in){
		S1=3;
		break;
	}
	if(r_e1==en_3_in){
		S1=1;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in){
		S2=1;
		break;
	}
	if(r_e1==en_3_in){
		S2=3;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in){
		S3=3;
		break;
	}
	if(r_e1==en_2_in){
		S3=1;
		break;
	}
	}


	switch (r_e3){

	case en_1_in:
	if(r_e1==en_2_in){
		S1=3;
		break;
	}
	if(r_e1==en_3_in){
		S1=1;
		break;
	}

	case en_2_in:
	if(r_e1==en_1_in){
		S2=1;
		break;
	}
	if(r_e1==en_3_in){
		S2=3;
		break;
	}

	case en_3_in:
	if(r_e1==en_1_in){
		S3=3;
		break;
	}
	if(r_e1==en_2_in){
		S3=1;
		break;
	}


	}	
	SemMaster(S1,S2,S3,S4);	
	}
	
//////////////////////////////////////////////////////////////////////////////

void sesta16(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){

	case en_1_in:
	if(Share_Mem[r_e3+10]==0){
		S1=4;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]!=0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e3+10]==0){
		S2=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e3+10]!=0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e3+10]!=0){
		S2=3;
		break;
	}
	case en_3_in:
	if(Share_Mem[r_e3+10]==0){
		S3=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e3+10]!=0){
		S3=3;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e3+10]!=0){
		S3=1;
		break;
	}
	}



	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}
	SemMaster(S1,S2,S3,S4);	
	}

//////////////////////////////////////////////////////////////////////////////

void sesta17(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;
	}


	switch (r_e3){

	case en_1_in:
	if(Share_Mem[r_e2+10]==0){
		S1=4;
		break;
	}
	if(r_e1==en_2_in && Share_Mem[r_e2+10]!=0){
		S1=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:
	if(Share_Mem[r_e2+10]==0){
		S2=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0){
		S2=1;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S2=3;
		break;
	}

	case en_3_in:
	if(Share_Mem[r_e2+10]==0){
		S3=4;
		break;
	}
	if(r_e1==en_1_in && Share_Mem[r_e2+10]!=0){
		S3=3;
		break;
	}
	if(r_e1==en_3_in && Share_Mem[r_e2+10]!=0){
		S3=1;
		break;
	}
	}

	SemMaster(S1,S2,S3,S4);	

	}

//////////////////////////////////////////////////////////////////////////////

void AutomBsFeFs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 			
		r_e1=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==3){ 			
		r_e2=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==5){ 			
		r_e3=en_1_in;
	}


	if(Share_Mem[mod_tra_2]==2){ 			
		r_e1=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==3){ 			
		r_e2=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==5){ 			
		r_e3=en_2_in;
	}

	if(Share_Mem[mod_tra_3]==2){ 			
		r_e1=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 			
		r_e2=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==5){ 			
		r_e3=en_3_in;
	}


	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBsFF(r_e1,r_e2,r_e3);

	}

//////////////////////////////////////////////////////////////////////////////

void AutomBsFF(r_e1,r_e2,r_e3){


	//---------------------------------------------E0--------------------
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//---------------------------------------------E1--------------------

	if (est==1){

	adv_printf(" verde a la Prioridad\n");
	sesta18( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e1]==0 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e3+30]==0 && Share_Mem[r_e3]!=0){
	cp1++;
	adv_printf("ENSANCHE 3 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]<=10){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e3\n");
	    est=2;
	    E1=1;
	}
	

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e3]!=0){
	contad++;
	adv_printf("Camion de r_e3 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp]){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	est=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>5){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+20]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+30]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e3 a r_e2 \n");
	}
	}
	}

	//----------------------------ESTADO 3-------------------------//

	if(est==3){
	adv_printf(" Verde hacia r_e2\n");
	sesta19(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
	}

//////////////////////////////////////////////////////////////////////////////

void sesta18(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]<=10){
		S1=4;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]>10){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]>10){
		S1=1;
		break;
	}

	case en_2_in:

	if(Share_Mem[r_e3+10]<=10){
		S2=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]>10){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]>10){
		S2=3;
		break;
	}


	case en_3_in:
	
	if(Share_Mem[r_e3+10]<=10){
		S3=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]>10){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]>10){
		S3=1;
		break;
	}
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}

	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;


	}
	SemMaster(S1,S2,S3,S4);	
	}
	
//////////////////////////////////////////////////////////////////////////////

void sesta19(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
	}
	if(r_e2==en_3_in){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
	}
	if(r_e2==en_3_in){
	S2=3;
	break;
	}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
	}
	if(r_e2==en_2_in){
	S3=1;
	break;
	}
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}



	switch (r_e3){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
	}
	if(r_e2==en_3_in){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
	}
	if(r_e2==en_3_in){
	S2=3;
	break;
	}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
	}
	if(r_e2==en_2_in){
	S3=1;
	break;
	}
	}

	SemMaster(S1,S2,S3,S4);	
	}

//////////////////////////////////////////////////////////////////////////////

void AutomBeFeFs(void){

	int r_e1, r_e2 , r_e3;

	if(Share_Mem[mod_tra_1]==2){ 			
		r_e1=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==3){ 			
		r_e2=en_1_in;
	}
	if(Share_Mem[mod_tra_1]==4){ 			
		r_e3=en_1_in;
	}


	if(Share_Mem[mod_tra_2]==2){ 			
		r_e1=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==3){ 			
		r_e2=en_2_in;
	}
	if(Share_Mem[mod_tra_2]==4){ 			
		r_e3=en_2_in;
	}

	if(Share_Mem[mod_tra_3]==2){ 			
		r_e1=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==3){ 			
		r_e2=en_3_in;
	}
	if(Share_Mem[mod_tra_3]==4){ 			
		r_e3=en_3_in;
	}


	adv_printf("el valor r_e1 %i \n", r_e1);
	adv_printf("el valor r_e2 %i \n", r_e2);
	adv_printf("el valor r_e3 %i \n", r_e3);
		
	AutomBeFF(r_e1,r_e2,r_e3);

	}

//////////////////////////////////////////////////////////////////////////////

void AutomBeFF(r_e1,r_e2,r_e3){

	//---------------------------------------------E0--------------------
	if (est==0){
		cbloq++;
		Bloqueo();
		adv_printf("MODO BLOQUEO EN EST=0\n");
		if (cbloq>Share_Mem[timebloq]){
			cbloq=0;
			adv_printf("CBLOQ ES 0 EST = 0\n");
			
			if(Share_Mem[r_e1+20]==0 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e2+30]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e3+30]==0 ){
				est=1;
				adv_printf(" Pasar a ESTADO ES 1\n");
			}
			}
		}

	//---------------------------------------------E1--------------------

	if (est==1){

	adv_printf(" verde a la Prioridad\n");
	sesta20( r_e1, r_e2, r_e3);

	//Si no hay nada en el tramo ni en el ensanche, pasa al estado siguiente
	if(Share_Mem[r_e3]==0 && Share_Mem[r_e3+20]==0 && Share_Mem[r_e1]!=0){
	cp1++;
	adv_printf("reg 1 ESPERANDO\n");
	}

	if(cp1>Share_Mem[vacio] && Share_Mem[r_e3+10]==0){
		cp1=0;
		adv_printf(" Tiempo cumplido, revisar tramo para dar verde a r_e1\n");
	    est=2;
	    E1=1;
	}
	

	////////////////

	//Si en el ensanche de r_e2 hay camion esperando comienza cuenta
	if(Share_Mem[r_e1]!=0){
	contad++;
	adv_printf("Camion de r_e1 esperando contra la prioridad \n");
	}

	if(contad>Share_Mem[waitnp] && Share_Mem[r_e3+10]==0){
	adv_printf(" Tiempo cumplido, revisar tramo\n");
	E1=1;
	est=2;
	contad=0;
	}

	//Si en el ensanche r_e3 hay camion esperando comienza cuenta

	//seguridad para vehiculos que estan contra el tr?ico 
	if(Share_Mem[r_e1+30]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+30]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;
	}

	}

	//----------------------------ESTADO 2-------------------------//

	if(est==2){
	adv_printf("Revisando tramo\n");
	cuenta++;
	Bloqueo();
	if(cuenta>Share_Mem[waitzero]){
		cuenta=0;
	if(E1==1 && Share_Mem[r_e1+30]==0 && Share_Mem[r_e2+20]==0 && Share_Mem[r_e3+20]==0){
	est=3;
	adv_printf("Tramo libre, verde contra la prioridad de r_e1 a r_e3 \n");
	}
	}
	}

	//----------------------------ESTADO 3-------------------------//

	if(est==3){
	adv_printf(" Verde hacia r_e3\n");
	sesta21(r_e1, r_e2, r_e3);
	tverde++;
	if(tverde>Share_Mem[tnp]){
		adv_printf(" Tiempo cumplido\n");
		est=0;
		tverde=0;
	}

	if(Share_Mem[r_e1+20]!=0 || Share_Mem[r_e2+20]!=0 || Share_Mem[r_e3+20]!=0){
	adv_printf(" Vehiculos en contra el sentido\n");
	Bloqueo();
	est=0;	
	}

	}
	}

//////////////////////////////////////////////////////////////////////////////

void sesta20(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;

	switch (r_e1){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
	}
	if(r_e2==en_3_in){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
	}
	if(r_e2==en_3_in){
	S2=3;
	break;
	}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
	}
	if(r_e2==en_2_in){
	S3=1;
	break;
	}
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}

	switch (r_e3){

	case en_1_in:
	if(r_e2==en_2_in){
	S1=3;
	break;
	}
	if(r_e2==en_3_in){
	S1=1;
	break;
	}

	case en_2_in:
	if(r_e2==en_1_in){
	S2=1;
	break;
	}
	if(r_e2==en_3_in){
	S2=3;
	break;
	}
	
	case en_3_in:
	if(r_e2==en_1_in){
	S3=3;
	break;
	}
	if(r_e2==en_2_in){
	S3=1;
	break;
	}

	}

	SemMaster(S1,S2,S3,S4);	
	}

//////////////////////////////////////////////////////////////////////////////

void sesta21(r_e1, r_e2, r_e3){
	unsigned S1=2, S2=2, S3=2, S4=2;
	switch (r_e1){

	case en_1_in:
	if(Share_Mem[r_e3+10]==0){
		S1=4;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]!=0){
		S1=3;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]!=0){
		S1=1;
		break;
	}

	case en_2_in:

	if(Share_Mem[r_e3+10]==0){
		S2=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]!=0){
		S2=1;
		break;
	}
	if(r_e2==en_3_in && Share_Mem[r_e3+10]!=0){
		S2=3;
		break;
	}


	case en_3_in:
	
	if(Share_Mem[r_e3+10]==0){
		S3=4;
		break;
	}
	if(r_e2==en_1_in && Share_Mem[r_e3+10]!=0){
		S3=3;
		break;
	}
	if(r_e2==en_2_in && Share_Mem[r_e3+10]!=0){
		S3=1;
		break;
	}
	}

	switch (r_e2){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;

	}
	
	switch (r_e3){

	case en_1_in:
	S1=2;
	break;

	case en_2_in:
	S2=2;
	break;
	
	case en_3_in:
	S3=2;
	break;


	}
	SemMaster(S1,S2,S3,S4);	
	}
//////////////////////////////////////////////////////////////////////////////
