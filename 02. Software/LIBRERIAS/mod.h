//-----------------------------------------------------------------------------------------
//Revised History:
//	A1.20 Revise ADAMTCP_ModServer_Update() function behavior.		2004-04-05		Arthur
//	A1.21 Revise client timeout function							2004-07-15		Arthur
//		  Modify ADAMTCP_ModServer_Create()
//		  Add ADAMTCP_ModServer_Release()
//	A1.22 Extend maximum connections to 64
//	A1.23 Modify void ADAMTCP_ModServer_Release(void) and modify
//		  modbus tcp function calls timeout function according to
//		  socket lib select() time tick								2005-06-05		Arthur
//Current Library Ver. A1.23
//------------------------------------------------------------------------------------------

#include "socket.h"
#define Errno errno


typedef int bool;
#define FALSE   0
#define TRUE    !FALSE
#define false   0
#define true    !false
//-----Error
#define GetHostName_Err     (-1)
#define SocketInit_Err      (-2)
#define Connect_Err         (-3)
#define TCPSend_Err         (-1)
#define TCPRecv_Err         (-2)
#define TCPTimeOut_Err      0

//-----Error Code
#define ILLEGAL_FUNC        01
#define ILLEGAL_DATA_ADDR   02
#define ILLEGAL_DATA_VALUE  03
#define SLAVE_DE_FAIL       04
#define ACKNOWLEDGE         05
#define SLAVE_DE_BUSY       06
#define NEG_ACKNOWLEDGE     07
#define MEM_PARITY_ERR      08

//-----Server Error code
#define ModSer_Socket_Err	91
#define ModSer_bind_Err		92
#define ModSer_ioctl_Err	93
#define ModSer_listen_Err	94
#define ModSer_accept_Err	95
#define Parse_Cmd_Err		96

void Ver_TCP_Mod(char *vstr);   //library version

//Modbus TCP Client Functions
int ReturnErr_code(void);		//not exception error: return error code==NULL
                                //is  exception error: return error code== error code
int ADAMTCP_Connect(SOCKET * SO, char * Target_IP, int Target_Port);
bool ADAMTCP_Disconnect(SOCKET * SO);
int ADAMTCP_ForceMultiCoils(SOCKET * SO,
                            int WaitMilliSec,
                            int Slave_Addr,
                            int CoilIndex,
                            int TotalPoint,
                            int TotalByte,
                            unsigned char szData[]);
int ADAMTCP_ForceSingleCoil(SOCKET * SO,
                            int WaitMilliSec,
                            int Slave_Addr,
                            int CoilIndex,
                            int Data);
int ADAMTCP_PresetMultiRegs(SOCKET * SO,
                            int WaitMilliSec,
                            int Slave_Addr,
                            int StartReg,
                            int TotalReg,
                            int TotalByte,
                            unsigned char Data[]);
int ADAMTCP_PresetSingleReg(SOCKET * SO,
                            int WaitMilliSec,
                            int Slave_Addr,
                            int RegIndex,
                            int Data);
int ADAMTCP_ReadCoilStatus(SOCKET * SO,
                           int WaitMilliSec,
                           int Slave_Addr,
                           int StartIndex,
                           int TotalPoint,
                           int * ByteCount,
                           char * wData);
int ADAMTCP_ReadHoldingRegs(SOCKET * SO,
                           int WaitMilliSec,
                           int Slave_Addr,
                           int StartIndex,
                           int TotalPoint,
                           int * ByteCount,
                           char * wData);
int ADAMTCP_ReadInputRegs(SOCKET * SO,
                          int WaitMilliSec,
                          int Slave_Addr,
                          int StartIndex,
                          int TotalPoint,
                          int * ByteCount,
                          char * wData);
int ADAMTCP_ReadInputStatus(SOCKET * SO,
                            int WaitMilliSec,
                            int Slave_Addr,
                            int StartIndex,
                            int TotalPoint,
                            int * ByteCount,
                            char * wData);

//Modbus TCP Server Functions

int ADAMTCP_ModServer_Create(int Host_Port,
							 unsigned long waittimeout,
							 unsigned int numberConns,
							 unsigned char * ptr_mem,
							 int size_mem);
							 
int ADAMTCP_ModServer_Update(void);

void ADAMTCP_ModServer_Release(void);